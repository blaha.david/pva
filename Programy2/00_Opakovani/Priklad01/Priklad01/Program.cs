﻿using System;

namespace Priklad01 {
    class Program {

        static void Main (string[] args) {
            int N = ReadIntFromConsole("N: ");
            for (int i = 0; i < N; i++) {
                PrintRow(i, N);
            }
            Console.ReadKey();
        }

        static void PrintRow (int offset, int N) {
            for (int i = 0; i < N; i++) {
                Console.Write("{0:00} ", 1 + i + offset);
            }
            Console.WriteLine();
        }

        static void Print (string text) {
            Console.Write(text);
        }

        static void PrintLn (string text) {
            Console.WriteLine(text);
        }

        static string ReadStringFromConsole () {
            return Console.ReadLine();
        }

        static string ReadStringFromConsole (string prompt) {
            Print(prompt);
            return ReadStringFromConsole();
        }

        static int ConvertStringToInt (string s) {
            int i;
            if (!int.TryParse(s, out i)) {
                throw new ArgumentException("String s is not an Integer");
            } else {
                return i;
            }
        }

        static int ReadIntFromConsole (string prompt = "") {
            while (true) {
                try {
                    return ConvertStringToInt(ReadStringFromConsole(prompt));
                } catch (ArgumentException e) {
                    PrintLn("Invalid argument");
                }
            }
        }
    }
}
