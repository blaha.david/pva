﻿using System;

namespace Priklad02 {
    class Program {
        static void Main (string[] args) {
            Console.WriteLine(KorigovanyPrumer(new double[] { 2, 1, 10, 4, 11, 5, 2, 57, 12, 45, 27, 9, 4, 11, 34, 7, 3, 2, 4, 6, 56, 62, 342, 6, 3, 57, 6, 52, 47, 8, 65, 2, 1, 10, 4, 11, 5, 2, 57, 12, 45, 27, 9, 4, 11, 34, 7, 3, 2, 4, 6, 56, 62, 342, 6, 3, 57, 6, 52, 47, 8, 65 }));

            Console.ReadKey();
        }

        static double KorigovanyPrumer (double[] input) {
            int minIndex = -1, maxIndex = -1;
            double min = double.MaxValue, max = -double.MaxValue;
            for (int i = 0; i < input.Length; i++) {
                if (input[i] < min) {
                    min = input[i];
                    minIndex = i;
                }
                if (input[i] > max) {
                    max = input[i];
                    maxIndex = i;
                }
            }

            double sum = 0;
            for (int i = 0; i < input.Length; i++) {
                if (i != minIndex && i != maxIndex) {
                    sum += input[i];
                }
            }
            return sum / (input.Length - 2);
        }

        static double KorigovanyPrumer (int[] input) {
            int minIndex = -1, maxIndex = -1;
            int min = int.MaxValue, max = -int.MaxValue;
            for (int i = 0; i < input.Length; i++) {
                if (input[i] < min) {
                    min = input[i];
                    minIndex = i;
                }
                if (input[i] > max) {
                    max = input[i];
                    maxIndex = i;
                }
            }

            double sum = 0;
            for (int i = 0; i < input.Length; i++) {
                if (i != minIndex && i != maxIndex) {
                    sum += input[i];
                }
            }
            return sum / (input.Length - 2);
        }
    }
}