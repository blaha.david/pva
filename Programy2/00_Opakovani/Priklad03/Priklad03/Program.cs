﻿using System;
using System.Collections.Generic;

namespace Priklad03 {
    class Program {
        static void Main (string[] args) {
            string[] jmena = new string[50];
            Random r = new Random();
            for (int i = 0; i < jmena.Length; i++) {
                jmena[i] = "";
                int len = r.Next(3,10);
                for (int j = 0; j < len; j++) {
                    jmena[i] += (char)r.Next('a', 'z');
                }
            }
            Osoba[] osoby = Convert(jmena);
            foreach (Osoba o in osoby) {
                Console.WriteLine("{0,-10} id: {1:0000000000}", o.jmeno, o.identifikator);
            }

            Console.ReadKey();
        }

        static Osoba[] Convert (string[] jmena) {
            Random r = new Random();

            Osoba[] osoby = new Osoba[jmena.Length];

            for (int i = 0; i < osoby.Length; i++) {
                int id = r.Next();
                if (ContainsID(osoby, id)) {
                    throw new IndexOutOfRangeException();
                }
                osoby[i] = new Osoba(jmena[i], id);
            }
            return osoby;
        }

        static bool ContainsID (Osoba[] osoby, int id) {
            foreach (Osoba o in osoby) {
                if (o != null) {
                    if (o.identifikator == id) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    class Osoba {
        public string jmeno;
        public int identifikator;

        public Osoba (string jmeno, int identifikator) {
            this.jmeno = jmeno;
            this.identifikator = identifikator;
        }
    }
}
