﻿using System;

namespace Priklad04 {
    class Program {
        static void Main (string[] args) {
            int[] cisla = new int[] { 5, 2, 0, 1, 4, 125, 12, -20, -10 };
            Graf(cisla);
            Console.ReadKey();
        }

        static void Graf (int[] cisla) {
            Array.Sort(cisla);
            int minValue = cisla[0];
            int maxValue = cisla[cisla.Length - 1];
            int medianIndex = cisla.Length / 2;

            int meritko = (maxValue / 100) + 1;
            //int meritko = 2;

            string spacePadding = new string(' ', (minValue < 0 ? -minValue : minValue)/meritko);
            for (int i = 0; i < cisla.Length; i++) {
                string median = (i == medianIndex ? " --- MEDIAN" : "");

                if (cisla[i] == 0) {
                    Console.WriteLine("{0}* (0)" + median, spacePadding);
                } else

                //if (cisla[i] > 100) {
                //    Console.WriteLine("{0}|{1} ... {1} ({2})" + median, spacePadding, new string('*', 30), cisla[i]);
                //} else

                if (cisla[i] < 0) {
                    int paddingLength = (minValue < 0 ? -minValue : minValue)/meritko;
                    Console.WriteLine("{0," + paddingLength + "}| ({1})" + median, new string('*', -cisla[i] / meritko), cisla[i]);
                } else
                if (cisla[i] > 0) {
                    Console.WriteLine("{0}|{1} ({2})" + median, spacePadding, new string('*', cisla[i] / meritko), cisla[i]);
                }
            }
        }
    }
}
