﻿using System;

namespace Priklad02 {
    class Program {
        static void Main (string[] args) {
            Osoba o = new Osoba("Han", "Havel", new DateTime(1950, 1, 6));
            Osoba p = new Osoba("Han", "Havel", new DateTime(1950, 11, 6));
            Zamestnanec z = new Zamestnanec("jan", "Havel", new DateTime(1950, 11, 6), 32500.0m, new DateTime(2020, 1, 20));
            Console.WriteLine(o.Vek());
            Console.WriteLine("Rozdil veku je {0}", o.RozdilVeku(z));
            Student s = new Student("Jan", "Havel", new DateTime(1950, 11, 6), "3K", 2.5);
            Student t = new Student("Jan", "Havel", new DateTime(1950, 11, 6), "3K", 2.5);
            Console.WriteLine("Jsou ze stejne tridy:{0}", s.StejnaTrida(t));
            Student sZKonzole = Student.Nacti();
            Osoba[] po = { o, p, z, s, t, sZKonzole };
            foreach (var v in po) {
                Console.WriteLine(v);
            }
            Console.ReadKey();
        }
    }

    class Osoba {
        public string jmeno;
        public string prijmeni;
        public DateTime narozen;

        public Osoba (string jmeno, string prijmeni, DateTime narozen) {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.narozen = narozen;
        }

        public int Vek () {
            return (DateTime.MinValue + (DateTime.Now - narozen)).Year - 1;
        }

        public int RozdilVeku (Osoba o) {
            return Vek() - o.Vek();
        }

        public static Osoba Nacti () {
            Console.WriteLine("Nova Osoba:");
            Console.Write("Jmeno: "); string jmeno = Console.ReadLine();
            Console.Write("Prijmeni: "); string prijmeni = Console.ReadLine();
            Console.Write("Datum narozeni: "); string datumNarozeni = Console.ReadLine();
            return new Osoba(jmeno, prijmeni, DateTime.Parse(datumNarozeni));
        }

        public override string ToString () {
            return string.Format("Osoba:\n\t{0} {1} ({2})", jmeno, prijmeni, narozen.ToString("d"));
        }
    }

    class Student : Osoba {
        public string trida;
        public double prumer;

        public Student (string jmeno, string prijmeni, DateTime narozen, string trida, double prumer) : base(jmeno, prijmeni, narozen) {
            this.trida = trida;
            this.prumer = prumer;
        }

        public bool StejnaTrida (Student s) {
            return trida == s.trida;
        }

        new public static Student Nacti () {
            Console.WriteLine("Novy Student:");
            Console.Write("Jmeno: "); string jmeno = Console.ReadLine();
            Console.Write("Prijmeni: "); string prijmeni = Console.ReadLine();
            Console.Write("Datum narozeni: "); string datumNarozeni = Console.ReadLine();
            Console.Write("Trida: "); string trida = Console.ReadLine();
            Console.Write("Studijni prumer: "); string prumer = Console.ReadLine();
            return new Student(jmeno, prijmeni, DateTime.Parse(datumNarozeni), trida, double.Parse(prumer));
        }

        public override string ToString () {
            return string.Format("Student:\n\t{0} {1} {3} ({2}), prumer={4:0.00 }", jmeno, prijmeni, narozen.ToString("d"), trida, prumer);
        }
    }

    class Zamestnanec : Osoba {
        public decimal plat;
        public DateTime nastoupil;

        public Zamestnanec (string jmeno, string prijmeni, DateTime narozen, decimal plat, DateTime nastoupil) : base(jmeno, prijmeni, narozen) {
            this.plat = plat;
            this.nastoupil = nastoupil;
        }

        public Zamestnanec DeleZamestnany (Zamestnanec z) {
            return nastoupil > z.nastoupil ? z : this;
        }

        new public static Zamestnanec Nacti () {
            Console.WriteLine("Novy Zamestnanec:");
            Console.Write("Jmeno: "); string jmeno = Console.ReadLine();
            Console.Write("Prijmeni: "); string prijmeni = Console.ReadLine();
            Console.Write("Datum narozeni: "); string datumNarozeni = Console.ReadLine();
            Console.Write("Plat: "); string plat = Console.ReadLine();
            Console.Write("Datum nastoupeni: "); string datumNastoupeni = Console.ReadLine();
            return new Zamestnanec(jmeno, prijmeni, DateTime.Parse(datumNarozeni), decimal.Parse(plat), DateTime.Parse(datumNarozeni));
        }

        public override string ToString () {
            return string.Format("Zamestnanec:\n\t{0} {1} ({2}), plat={3:0.00}Kc/mesic, datum nastupu={4}", jmeno, prijmeni, narozen.ToString("d"), plat, nastoupil.ToString("d"));
        }
    }
}
