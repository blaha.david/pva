﻿using System;
namespace Priklad01 {
    class Program {
        static void Main (string[] args) {
            Teleso a = new Teleso(0, 0.5, 2);
            Teleso b = new Teleso(0, 0.5, 5, 15);
            Teleso c = new Teleso(0, 0.5, 2, 7);

            //for (int i = 0; i < a.arrX.Length; i++) {
            //    if (i % 10 == 0) Console.Write("{0}s \t", i);
            //}
            //Console.WriteLine();
            //
            //a.PrintTableRow();
            //b.PrintTableRow();
            //c.PrintTableRow();

            Console.WriteLine("A - B: {0}", a.Vzdalenost(b));
            Console.WriteLine("B - C: {0}", b.Vzdalenost(c));
            Console.WriteLine("A - C: {0}", a.Vzdalenost(c));
            Console.ReadKey();
        }
    }

    class Teleso {
        private double x;
        private double m;
        private double f;
        private int startF;
        private int time = 0;

        public double[] arrX;

        public Teleso (double x, double m, double f, int startF = 0) {
            this.x = x;
            this.m = m;
            this.f = f;
            this.startF = startF;
            arrX = new double[91];


            for (int i = 0; i < arrX.Length; i++) {
                arrX[time++] = x;
                if (time >= startF) {
                    x = 0.5 * (f / m) * (time - startF) * (time - startF);
                }
            }

        }

        public void PrintTableRow () {
            for (int i = 0; i < arrX.Length; i++) {
                if (i % 10 == 0) Console.Write("{0} \t", arrX[i]);
            }
            Console.WriteLine();
        }

        public double Vzdalenost (Teleso t) {
            return Math.Abs(t.arrX[time - 1] - arrX[time - 1]);
        }
        public double Vzdalenost (Teleso t, int time) {
            return Math.Abs(t.arrX[time] - arrX[time]);
        }
    }
}
