﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ZaverecnyProjekt_Hra {
    public static class Popup {

        //popup window with a text input (enter player name)
        public static string StringInput (string text, string title) {
            Form prompt = new Form() {
                Width = 500,
                Height = 150,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = title,
                StartPosition = FormStartPosition.CenterScreen
            };
            Label textLabel = new Label() { Left = 50, Top = 20, Text = text, Width = 400 };
            TextBox textBox = new TextBox() { Left = 50, Top = 50, Width = 400, Text = "" };
            Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 70, DialogResult = DialogResult.OK };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.AcceptButton = confirmation;
            DialogResult result = prompt.ShowDialog();
            return result == DialogResult.OK ? textBox.Text : null;
        }

        //popup window for selection options from an enum (select difficulty)
        public static T EnumInput<T> (string text, string title) {
            Form prompt = new Form() {
                Width = 500,
                Height = 150,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = title,
                StartPosition = FormStartPosition.CenterScreen
            };
            Label textLabel = new Label() { Left = 50, Top = 20, Text = text, Width = 400 };
            ComboBox comboBox = new ComboBox() { Left = 50, Top = 50, Width = 400, DataSource = Enum.GetValues(typeof(T)), DropDownStyle = ComboBoxStyle.DropDownList };
            Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 70, DialogResult = DialogResult.OK };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(comboBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.AcceptButton = confirmation;
            prompt.ShowDialog();
            return (T)comboBox.SelectedItem;
        }

        //display scoreboard in a new popup window
        public static void DisplayScoreboard (List<HighScore> scores, string extra) {
            Form scoreboard = new Form() {
                Width = 500,
                Height = 600,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = "Scoreboard",
                StartPosition = FormStartPosition.CenterScreen
            };

            //build the scoreboard string
            Label labelScores = new Label() { Left = 50, Top = 20, Width = 400, Height = 300, Text = "", Font = new Font("Arial", 18) };
            for (int i = 0; i < Math.Min(scores.Count, 10); i++) {
                labelScores.Text += string.Format("{0}. {1}\n", i + 1, scores[i]);
            }

            Label labelPosition = new Label() { Left = 50, Top = 400, Width = 400, Height = 100, Text = extra, ForeColor = Color.Red, Font = new Font("Arial", 24) };
            Button btnClose = new Button() { Text = "Close", Left = 350, Width = 100, Top = 500, DialogResult = DialogResult.OK };

            btnClose.Click += (sender, e) => { scoreboard.Close(); };
            scoreboard.Controls.Add(labelScores);
            scoreboard.Controls.Add(labelPosition);
            scoreboard.Controls.Add(btnClose);
            scoreboard.AcceptButton = btnClose;
            scoreboard.ShowDialog();
        }
    }
}
