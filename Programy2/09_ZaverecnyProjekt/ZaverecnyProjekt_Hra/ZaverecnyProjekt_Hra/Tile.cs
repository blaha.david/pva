﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace ZaverecnyProjekt_Hra {
    public class Tile {

        public static Color[] colors = { Color.FromArgb(0, 128, 255), Color.FromArgb(50, 200, 50), Color.FromArgb(255, 50, 50), Color.FromArgb(50, 0, 150), Color.FromArgb(150, 0, 10), Color.FromArgb(0, 200, 200), Color.FromArgb(10, 10, 10), Color.FromArgb(100, 100, 100) };

        private int i; //grid index
        private int j; //grid index
        private float x; //on screen coordinate (in pixels)
        private float y; //on screen coordinate (in pixels)
        private float w; //pixel width
        private float h; //pixel height

        private bool revealed = false;
        private bool mine = false;
        private int neighbourMines = 0;
        private bool marked = false;
        private bool exploded = false;

        //autobot
        private List<Tile> neighbours = new List<Tile>(8); //TODO: remove???
        private int unrevealedNeighbours; //TODO: remove???

        private Pen blackPen = new Pen(Color.FromArgb(0, 0, 0));
        private Pen redPen = new Pen(Color.FromArgb(255, 0, 0));
        private Brush black = new SolidBrush(Color.FromArgb(0, 0, 0));
        private Brush green = new SolidBrush(Color.FromArgb(0, 255, 0));
        private Brush red = new SolidBrush(Color.FromArgb(255, 0, 0));
        private Brush gray = new SolidBrush(Color.FromArgb(200, 200, 200));
        private Brush white = new SolidBrush(Color.FromArgb(255, 255, 255));

        public Tile (int i, int j, float w, float h) {
            this.i = i;
            this.j = j;
            this.w = w;
            this.h = h;
            this.x = i * w;
            this.y = j * h;
        }

        //draw the tile on the screen canvas
        public void Paint (Graphics g, bool playing) {

            if (revealed) {
                if (mine) {
                    g.FillRectangle(gray, x, y, w, h);

                    if (marked) {
                        g.FillEllipse(green, x + (w / 4), y + (h / 4), w / 2, h / 2);
                    } else {
                        if (exploded) {
                            g.FillEllipse(red, x + (w / 4), y + (h / 4), w / 2, h / 2);
                        } else {
                            g.FillEllipse(black, x + (w / 4), y + (h / 4), w / 2, h / 2);
                        }
                    }

                } else {
                    g.FillRectangle(gray, x, y, w, h);
                }
            } else {
                g.FillRectangle(white, x, y, w, h);

                if (marked) {
                    if (!playing && !mine) {
                        g.FillEllipse(black, x + (w / 4), y + (h / 4), w / 2, h / 2);
                        g.DrawLine(redPen, x, y, x + w, y + h);
                        g.DrawLine(redPen, x + w, y, x, y + h);
                    } else {
                        g.FillEllipse(green, x + (w / 4), y + (h / 4), w / 2, h / 2);
                    }
                }

            }

            g.DrawRectangle(blackPen, x, y, w, h);

            if (neighbourMines > 0 && revealed && !mine) {
                Color c = colors[neighbourMines - 1]; //TODO: replace color with pen
                                                      //g.setFont(new Font("Arial", 0, MineSweeper.TILE_SIZE));
                                                      //TODO: premade font, premade brush
                g.DrawString(neighbourMines.ToString(), new Font("Arial", w * 0.8f), new SolidBrush(c), x, y - (h / 6));
            }
        }

        public void Reveal () {
            revealed = true;
        }

        public bool IsRevealed () {
            return revealed;
        }

        public bool IsMine () {
            return mine;
        }

        public void SetMine () {
            mine = true;
        }

        public void SetNeighbourMines (int neighbourMines) {
            this.neighbourMines = neighbourMines;
        }

        public int GetNeighbourMines () {
            return neighbourMines;
        }

        public void SetNeighbours (List<Tile> neighbours) {
            this.neighbours = neighbours;
        }

        public List<Tile> GetNeighbours () {
            return neighbours;
        }

        public void Mark (bool state) {
            marked = state;//TODO: Graphics Label, print mines left
        }

        public bool IsMarked () {
            return marked;
        }

        public void Explode () {
            exploded = true;
        }

        public int GetUnrevealedNeighbours () {
            return unrevealedNeighbours;
        }

        public void SetUnrevealedNeighbours (int unrevealedNeighbours) {
            this.unrevealedNeighbours = unrevealedNeighbours;
        }

        public int GetI () {
            return i;
        }

        public int GetJ () {
            return j;
        }


    }
}
