﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;


namespace ZaverecnyProjekt_Hra {
    public partial class Form1 : Form {

        private bool playing = false;
        private int playingTime = 0;
        private string playerName;

        private Game game;
        private Game.Difficulty difficulty;

        //setup custom double buffered rendering
        BufferedGraphicsContext currentGraphicsContext;
        BufferedGraphics gBuffer;

        //setup graphic enviroment
        public Form1 () {

            //enable double buffering to stop blinking
            DoubleBuffered = true;
            SetStyle(
                System.Windows.Forms.ControlStyles.UserPaint |
                System.Windows.Forms.ControlStyles.AllPaintingInWmPaint |
                System.Windows.Forms.ControlStyles.OptimizedDoubleBuffer |
                System.Windows.Forms.ControlStyles.ResizeRedraw, true);
            UpdateStyles();

            InitializeComponent();

            //setup custom double buffered rendering
            currentGraphicsContext = BufferedGraphicsManager.Current;
            gBuffer = currentGraphicsContext.Allocate(canvas.CreateGraphics(), canvas.DisplayRectangle);

        }

        //display popups to enter player name and difficulty and setup and start the game
        private void Form1_Load (object sender, EventArgs e) {

            playerName = "";
            while (playerName == "") {
                playerName = Popup.StringInput("Please enter your name:", "Enter your name");
                if (playerName == null) { //window was closed
                    Application.Exit();
                    return;
                }
            }

            difficulty = Popup.EnumInput<Game.Difficulty>("Please select difficulty:", "Select difficulty");

            game = new Game(difficulty, playerName, canvas.ClientRectangle.Width, GameWon, GameLost);

            game.Restart(); //generate world and start playing
            labelDifficulty.Text = "Difficulty:\n" + difficulty.ToString();

            int minesLeft = game.MinesLeft;
            labelMinesLeft.Text = "Mines left:\n" + minesLeft;

            labelTime.Text = "Time:\n" + playingTime;

            labelGlobalHighscore.Text = "Top Highscore:\n" + game.GlobalHighScore();
            labelHighscore.Text = "Your Highscore:\n" + game.PlayerHighScore();

            timer.Start();
            playing = true;
            Invalidate(invalidateChildren: true);

        }

        private void canvas_Paint (object sender, PaintEventArgs e) {
            game.Paint(gBuffer.Graphics, playing);

            gBuffer.Render(); //swap buffers
        }

        
        private void canvas_MouseClick (object sender, MouseEventArgs e) {
            if (playing) {
                int mouseX = e.X;
                int mouseY = e.Y;

                game.TileClicked(mouseX, mouseY, e);

                //update mine count
                labelMinesLeft.Text = "Mines left:\n" + game.MinesLeft;
                Invalidate(invalidateChildren: true);
            }
        }

        //stop game, add new highscore and display scoreboard
        public void GameWon () {
            playing = false;

            game.NewScore(playingTime);
            //update high scores
            labelGlobalHighscore.Text = "Top Highscore:\n" + game.GlobalHighScore();
            labelHighscore.Text = "Your Highscore:\n" + game.PlayerHighScore();
            Invalidate(invalidateChildren: true);

            List<HighScore> scores = HighScore.Filter(difficulty);

            //calculate in which place the player is
            int place = 0;
            for (place = 0; place < scores.Count; place++) {
                if (scores[place].PlayerName == playerName) {
                    break;
                }
            }


            Popup.DisplayScoreboard(scores, string.Format("{0}. place in\ndifficulty {1}", place + 1, difficulty));
        }

        //stop game and display scoreboard
        public void GameLost () {
            playing = false;
            Invalidate(invalidateChildren: true);

            List<HighScore> scores = HighScore.Filter(difficulty);
            Popup.DisplayScoreboard(scores, "You hit a bomb\nYou can try again");

        }

        //add one to the playtime counter every one second
        private void timer_Tick (object sender, EventArgs e) {
            if (playing) {
                playingTime += 1;
                labelTime.Text = "Time: \n" + playingTime;
            }
        }

        //restart the game
        private void btnRestart_Click (object sender, EventArgs e) {
            game.Restart();

            labelMinesLeft.Text = "Mines left:\n" + game.MinesLeft;

            playing = true;
            playingTime = 0;
            Invalidate(invalidateChildren: true);

        }

        //change difficulty setting and restart the game
        private void btnSettings_Click (object sender, EventArgs e) {
            difficulty = Popup.EnumInput<Game.Difficulty>("Please select difficulty:", "Select difficulty");
            labelDifficulty.Text = "Difficulty:\n" + difficulty.ToString();
            game.ChangeDifficulty(difficulty);

            //update high scores
            labelGlobalHighscore.Text = "Top Highscore:\n" + game.GlobalHighScore();
            labelHighscore.Text = "Your Highscore:\n" + game.PlayerHighScore();


            game.Restart();

            labelMinesLeft.Text = "Mines left:\n" + game.MinesLeft;

            playing = true;
            playingTime = 0;
            Invalidate(invalidateChildren: true);
        }

        //save data to file when the player closes the window
        private void Form1_FormClosing (object sender, FormClosingEventArgs e) {
            HighScore.SaveScores();
        }


    }


}
