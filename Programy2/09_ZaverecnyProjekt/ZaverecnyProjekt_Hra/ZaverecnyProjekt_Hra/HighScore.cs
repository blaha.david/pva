﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace ZaverecnyProjekt_Hra {

    public class HighScore {

        private static List<HighScore> allScores;


        public int Time { get; private set; }
        public string PlayerName { get; private set; }
        public Game.Difficulty Difficulty { get; private set; }

        public HighScore (int time, string playerName, Game.Difficulty difficulty) {
            Time = time;
            PlayerName = playerName;
            Difficulty = difficulty;
        }

        public override string ToString () {
            return string.Format("{0} - {1}:{2}", PlayerName, Time / 60, Time % 60);
        }

        public string ToFileString () {
            return string.Format("{0},{1},{2}", PlayerName, Time, Difficulty);
        }

        public static void LoadScores () {
            allScores = new List<HighScore>();
            if (!File.Exists("game.dat")) {
                return;
            }
            using (var sr = new StreamReader("game.dat")) {
                while (sr.Peek() != -1) {
                    string[] line = sr.ReadLine().Split(',');
                    try {
                        string playerName = line[0];
                        int time = int.Parse(line[1]);
                        Game.Difficulty difficulty = (Game.Difficulty)Enum.Parse(typeof(Game.Difficulty), line[2]);
                        allScores.Add(new HighScore(time, playerName, difficulty));
                    } catch (FormatException) {
                        //TODO: save file corrupted
                    }
                }
            }
        }

        public static void SaveScores () {
            if (allScores == null) {
                return;
            }
            using (var sw = new StreamWriter("game.dat")) {
                foreach (var score in allScores) {
                    sw.WriteLine(score.ToFileString());
                }
            }
        }


        //return all scores of specified difficulty and playerName(if specified)
        public static List<HighScore> Filter (Game.Difficulty difficulty) {
            List<HighScore> filtered = new List<HighScore>();
            foreach (var score in allScores) {
                if (score.Difficulty == difficulty) { //if the player name is set filter by name as well
                    filtered.Add(score);
                }
            }

            filtered.Sort((a, b) => a.Time - b.Time);

            return filtered;
        }

        //get the score of a specified player in the specified difficulty. Return null if the player hasn't played in that dfficulty yet
        public static HighScore Get (Game.Difficulty difficulty, string playerName) {
            foreach (var score in allScores) {
                if (score.Difficulty == difficulty && score.PlayerName == playerName) { //if the player name is set filter by name as well
                    return score;
                }
            }
            return null;
        }

        //if a player already has a score and score is better, update it, if not, don't do anything. If the player does not have a score yet, create a new one
        public static void NewScore (Game.Difficulty difficulty, string playerName, int time) {
            foreach (var score in allScores) {
                if (score.Difficulty == difficulty && score.PlayerName == playerName) {
                    if (time < score.Time) {
                        score.Time = time;
                    }
                    return;
                }
            }

            //player does not exist, create a new score
            allScores.Add(new HighScore(time, playerName, difficulty));
        }
    }
}