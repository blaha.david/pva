﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZaverecnyProjekt_Hra {
    public class Game {

        private int SIZE; //grid size
        private int MINES;
        private float TILE_SIZE;
        private int canvasSize;

        private const bool DEBUG = true;//TODO: remove

        private Difficulty difficulty;
        private string playerName;

        public World World { get; private set; }

        public Game (Difficulty difficulty, string playerName, int canvasSize, GameEvent gameWon, GameEvent gameLost) {

            HighScore.LoadScores(); //load high scores from file

            this.canvasSize = canvasSize;
            this.playerName = playerName;
            this.difficulty = difficulty;

            World = new World(gameWon, gameLost);

            ChangeDifficulty(difficulty);//set difficulty, generate world and start playing


        }

        //update difficulty, world settings, generate a new world and restart the game
        public void ChangeDifficulty (Difficulty difficulty) {
            this.difficulty = difficulty;
            switch (difficulty) {
                case Difficulty.Easy:
                    SIZE = 10;
                    MINES = 8;
                    break;
                case Difficulty.Normal:
                    SIZE = 15;
                    MINES = 25;
                    break;
                case Difficulty.Hard:
                    SIZE = 25;
                    MINES = 100;
                    break;
                case Difficulty.Impossible:
                    SIZE = 30;
                    MINES = 200;
                    break;
                default:
                    break;
            }

            TILE_SIZE = canvasSize / (float)SIZE;

            Restart(); //regenerate world
        }

        //calculate the remaining mines to mark
        public int MinesLeft {
            get { return World.CountMinesLeft(); }
        }

        //get the high score of the best player
        public HighScore GlobalHighScore () {
            List<HighScore> scores = HighScore.Filter(difficulty);
            if (scores.Count > 0) {
                return scores[0];
            } else {
                return null;
            }
        }

        //get the high score of the current player
        public HighScore PlayerHighScore () {
            return HighScore.Get(difficulty, playerName);
        }

        //mark or reveal the tile that the player clicked on
        public void TileClicked (int mouseX, int mouseY, MouseEventArgs e) {
            if (mouseX < 0 || mouseY < 0 || mouseX > SIZE * TILE_SIZE || mouseY > SIZE * TILE_SIZE) return;

            int i = (int)(mouseX / TILE_SIZE);
            int j = (int)(mouseY / TILE_SIZE);
            if (e.Button == MouseButtons.Left) {//left - reveal the clicked tile
                World.Reveal(i, j);
            } else if (e.Button == MouseButtons.Right) {//right - mark tile as mine
                World.Mark(i, j);
            } else if (e.Button == MouseButtons.Middle) { //middle - reveal all surrounding tiles
                World.Reveal(i, j);
                if (i > 0) {
                    World.Reveal(i - 1, j);
                }
                if (i < SIZE - 1) {
                    World.Reveal(i + 1, j);
                }
                if (j > 0) {
                    World.Reveal(i, j - 1);
                }
                if (j < SIZE - 1) {
                    World.Reveal(i, j + 1);
                }

                if (i > 0 && j > 0) {
                    World.Reveal(i - 1, j - 1);
                }
                if (i < SIZE - 1 && j > 0) {
                    World.Reveal(i + 1, j - 1);
                }
                if (i > 0 && j < SIZE - 1) {
                    World.Reveal(i - 1, j + 1);
                }
                if (i < SIZE - 1 && j < SIZE - 1) {
                    World.Reveal(i + 1, j + 1);
                }

            }
        }

        //generate a new world
        public void Restart () {
            World.Generate(SIZE, TILE_SIZE, MINES);
        }

        //render the world
        public void Paint (Graphics g, bool playing) {
            World.Paint(g, playing);
        }

        //add a new highscore
        public void NewScore (int time) {
            HighScore.NewScore(difficulty, playerName, time);
        }


        
        public enum Difficulty {
            Easy,
            Normal,
            Hard,
            Impossible
        }
    }
}
