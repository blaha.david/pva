﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace ZaverecnyProjekt_Hra {

    public delegate void GameEvent ();
    public class World {
        private int size; //size of the grid
        private int mines = 0;

        private Tile[][] grid;

        private Random random;

        private event GameEvent gameWon;
        private event GameEvent gameLost;

        public World (GameEvent gameWon, GameEvent gameLost) {
            this.gameWon = gameWon;
            this.gameLost = gameLost;
        }

        //render all tiles
        public void Paint (Graphics g, bool playing) {
            for (int j = 0; j < grid.Length; j++) {
                for (int i = 0; i < grid[j].Length; i++) {
                    grid[j][i].Paint(g, playing);
                }
            }
        }

        //reveal the tile on specified coordinates
        public void Reveal (int i, int j) {
            if (grid[j][i].IsMarked()) {
                return;
            }
            grid[j][i].Reveal();
            if (grid[j][i].IsMine()) {
                grid[j][i].Explode();
                RevealMines();
                gameLost();
                return;
            }

            //chain reveal
            if (grid[j][i].GetNeighbourMines() == 0) {
                if (j > 0) {
                    if (!grid[j - 1][i].IsRevealed()) {
                        Reveal(i, j - 1);
                    }
                }
                if (j < size - 1) {
                    if (!grid[j + 1][i].IsRevealed()) {
                        Reveal(i, j + 1);
                    }
                }
                if (i > 0) {
                    if (!grid[j][i - 1].IsRevealed()) {
                        Reveal(i - 1, j);
                    }
                }
                if (i < size - 1) {
                    if (!grid[j][i + 1].IsRevealed()) {
                        Reveal(i + 1, j);
                    }
                }

                if (j > 0 && i > 0) {
                    if (!grid[j - 1][i - 1].IsRevealed()) {
                        Reveal(i - 1, j - 1);
                    }
                }
                if (j < size - 1 && i > 0) {
                    if (!grid[j + 1][i - 1].IsRevealed()) {
                        Reveal(i - 1, j + 1);
                    }
                }
                if (j > 0 && i < size - 1) {
                    if (!grid[j - 1][i + 1].IsRevealed()) {
                        Reveal(i + 1, j - 1);
                    }
                }
                if (j < size - 1 && i < size - 1) {
                    if (!grid[j + 1][i + 1].IsRevealed()) {
                        Reveal(i + 1, j + 1);
                    }
                }
            }

            CheckWin();
        }


        //generate a new empty world and generate mines
        public void Generate (int size, float tile_size, int mineCount) {
            random = new Random();
            this.size = size;

            grid = new Tile[size][];
            for (int j = 0; j < grid.Length; j++) {
                grid[j] = new Tile[size];
                for (int i = 0; i < grid[j].Length; i++) {
                    grid[j][i] = new Tile(i, j, tile_size, tile_size);
                }
            }

            GenerateMines(mineCount);
            CalcNeighbours();
        }

        //toggle mark mine
        public void Mark (int i, int j) {
            if (!grid[j][i].IsRevealed()) {
                grid[j][i].Mark(!grid[j][i].IsMarked());
            }
            CheckWin();
        }

        //set state of marked mine (not toggle)
        public void Mark (int i, int j, bool state) {
            if (!grid[j][i].IsRevealed()) {
                grid[j][i].Mark(state);
            }
            CheckWin();
        }

        //randomly put mines in the world
        private void GenerateMines (int count) {
            mines = count;
            for (int a = 0; a < count; a++) {
                int i = random.Next(0, size);
                int j = random.Next(0, size);
                if (!grid[j][i].IsMine()) {
                    grid[j][i].SetMine();
                } else {
                    a--;
                }
            }
        }

        //calculate how many neighbouring mines each tile has(the number that is displayed on that tile)
        private void CalcNeighbours () {
            for (int j = 0; j < grid.Length; j++) {
                for (int i = 0; i < grid[j].Length; i++) {
                    List<Tile> neighbours = new List<Tile>();
                    int neighbourMines = 0;
                    if (j > 0) {
                        if (grid[j - 1][i].IsMine()) {
                            neighbourMines++;
                        }
                        neighbours.Add(grid[j - 1][i]);
                    }
                    if (j < size - 1) {
                        if (grid[j + 1][i].IsMine()) {
                            neighbourMines++;
                        }
                        neighbours.Add(grid[j + 1][i]);
                    }
                    if (i > 0) {
                        if (grid[j][i - 1].IsMine()) {
                            neighbourMines++;
                        }
                        neighbours.Add(grid[j][i - 1]);
                    }
                    if (i < size - 1) {
                        if (grid[j][i + 1].IsMine()) {
                            neighbourMines++;
                        }
                        neighbours.Add(grid[j][i + 1]);
                    }

                    if (j > 0 && i > 0) {
                        if (grid[j - 1][i - 1].IsMine()) {
                            neighbourMines++;
                        }
                        neighbours.Add(grid[j - 1][i - 1]);
                    }
                    if (j < size - 1 && i > 0) {
                        if (grid[j + 1][i - 1].IsMine()) {
                            neighbourMines++;
                        }
                        neighbours.Add(grid[j + 1][i - 1]);
                    }
                    if (j > 0 && i < size - 1) {
                        if (grid[j - 1][i + 1].IsMine()) {
                            neighbourMines++;
                        }
                        neighbours.Add(grid[j - 1][i + 1]);
                    }
                    if (j < size - 1 && i < size - 1) {
                        if (grid[j + 1][i + 1].IsMine()) {
                            neighbourMines++;
                        }
                        neighbours.Add(grid[j + 1][i + 1]);
                    }
                    grid[j][i].SetNeighbours(neighbours);
                    grid[j][i].SetNeighbourMines(neighbourMines);
                }
            }
        }

        //check the user completed the game - count non revealed(or marked) tiles and compare them with the amount of mines
        public void CheckWin () {
            int notRevealed = 0;

            for (int j = 0; j < grid.Length; j++) {
                for (int i = 0; i < grid[j].Length; i++) {
                    if (!grid[j][i].IsRevealed()) {
                        notRevealed++;
                    }
                }
            }

            if (notRevealed == mines) {
                //WON
                MarkAll(); //mark all remaining mines
                gameWon();
            }
        }

        //go through all tiles and count how many mines are left to mark
        public int CountMinesLeft () {
            int minesLeft = mines;

            for (int j = 0; j < grid.Length; j++) {
                for (int i = 0; i < grid[j].Length; i++) {
                    if (grid[j][i].IsMarked()) {
                        minesLeft--;
                    }
                }
            }
            return minesLeft;
        }

        // if the player revealed all necessary tiles (GameWon) - mark the rest of the mines
        public void MarkAll () {
            for (int j = 0; j < grid.Length; j++) {
                for (int i = 0; i < grid[j].Length; i++) {
                    if (grid[j][i].IsMine()) {
                        grid[j][i].Mark(true);
                    }
                }
            }
        }

        // if the player clicked on a mine (GameLost) - reveal all the mines
        public void RevealMines () {
            for (int j = 0; j < grid.Length; j++) {
                for (int i = 0; i < grid[j].Length; i++) {
                    if (grid[j][i].IsMine()) {
                        grid[j][i].Reveal();
                    }
                }
            }
        }

    }
}
