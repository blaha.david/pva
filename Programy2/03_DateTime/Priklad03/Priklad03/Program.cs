﻿using System;

namespace Priklad03 {
    class Program {

        struct Rezervace {
            public DateTime start;
            public DateTime end;

            public Rezervace (DateTime start, DateTime end) {
                this.start = start;
                this.end = end;
            }

            public bool Intercepts (DateTime day) {
                return start < day && day < end;
            }

        }

        static void Main (string[] args) {
            Rezervace[] rezervace = {
                new Rezervace(new DateTime(2019, 11, 15), new DateTime(2019, 11, 19)),
                new Rezervace(new DateTime(2019, 11, 24), new DateTime(2019, 11, 29)),
                new Rezervace(new DateTime(2019, 12,  6), new DateTime(2019, 12, 12)),
                new Rezervace(new DateTime(2019, 12, 26), new DateTime(2019, 12, 31))
            };

            DateTime day = new DateTime(2019, 11, 11);
            while (day.Year == 2019) {
                bool valid = true;
                foreach (var r in rezervace) {
                    if (r.Intercepts(day) || r.Intercepts(day.AddDays(1)) || r.Intercepts(day.AddDays(2))) {
                        valid = false;
                        break;
                    }
                }
                if (valid) {
                    Console.WriteLine("{0} - {1]", day.ToString("d"), day.AddDays(2).ToString("d"));
                }
            }

        }
    }
}
