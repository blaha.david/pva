﻿using System;

namespace Priklad01 {
    class Program {
        static void Main (string[] args) {
            DateTime now = DateTime.Now;
            Console.WriteLine("Dnes je {0} {1}.{2}.{3}", now.DayOfWeek, now.Day, now.Month, now.Year);
            Console.WriteLine("Do konce roku zbývá {0} dní", (new DateTime(2020, 1, 1, 0, 0, 0) - now).TotalDays);
            Console.WriteLine("Silvestr je {0}", new DateTime(2019, 12, 31).DayOfWeek);

            Console.ReadKey();
        }
    }
}
