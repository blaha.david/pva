﻿using System;

namespace Priklad04 {
    class Program {
        static void Main (string[] args) {
            int perFyz = 23; //perioda
            int perEmo = 28;
            int perInt = 33;

            Console.Write("Narozeni: "); DateTime narozeni = DateTime.Parse(Console.ReadLine());
            Console.Write("Od: "); DateTime od = DateTime.Parse(Console.ReadLine());
            Console.Write("Do: "); DateTime @do = DateTime.Parse(Console.ReadLine());

            //DateTime narozeni = new DateTime(2000, 1, 1);
            //DateTime od = new DateTime(2000, 1, 1);
            //DateTime @do = new DateTime(2100, 1, 1);

            for (DateTime day = od; day <= @do; day = day.AddDays(1)) {
                int modFyz = (day - narozeni).Days % perFyz; //modulo day
                int modEmo = (day - narozeni).Days % perEmo;
                int modInt = (day - narozeni).Days % perInt;

                char symFyz = GetSymbol(modFyz, perFyz);//symbol
                char symEmo = GetSymbol(modEmo, perEmo);
                char symInt = GetSymbol(modInt, perInt);

                if (symFyz == '*' && symEmo == '*' && symInt == '*' || true) {
                    Console.WriteLine("{0} {1}{2}{3}", day.ToString("dd.MM.yyyy"), symFyz, symEmo, symInt, (day - narozeni).Days);
                }
            }

            Console.ReadKey();
        }

        static char GetSymbol (int day, int period) {
            if (day == 0 || day == period / 2) {
                return '*';
            }
            if (day < period / 2) {
                return '+';
            }
            return '-';
        }
    }
}
