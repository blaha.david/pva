﻿using System;

namespace Priklad02 {
    class Program {
        static void Main (string[] args) {

            DateTime now = new DateTime(1,1,13);
            DateTime current = new DateTime(now.Year, now.Month, 13);
            int i = 0;
            while (current <= new DateTime(9999, 11, 13)) {
                if (current.DayOfWeek == DayOfWeek.Friday) {
                    Console.WriteLine(current.ToString("d"));
                    i++;
                }
                current = current.AddMonths(1);
            }

            Console.WriteLine(i / (double)(current.Year - now.Year));
            Console.ReadKey();
        }
    }
}
