﻿using System;

namespace Priklad05 {
    class Program {
        static void Main (string[] args) {
            TimeSpan[,] mezicasy = {
                {new TimeSpan(0,1,51), new TimeSpan(0,3,10), new TimeSpan(0,5,28), new TimeSpan(0,7,31), new TimeSpan(0,09,36), new TimeSpan(0,11,07)},
                {new TimeSpan(0,1,58), new TimeSpan(0,3,08), new TimeSpan(0,5,21), new TimeSpan(0,7,42), new TimeSpan(0,09,38), new TimeSpan(0,11,21)},
                {new TimeSpan(0,1,55), new TimeSpan(0,3,15), new TimeSpan(0,5,45), new TimeSpan(0,7,45), new TimeSpan(0,10,00), new TimeSpan(0,11,25)},
                {new TimeSpan(0,1,59), new TimeSpan(0,3,09), new TimeSpan(0,5,22), new TimeSpan(0,7,22), new TimeSpan(0,09,39), new TimeSpan(0,11,28)},
                {new TimeSpan(0,1,55), new TimeSpan(0,3,11), new TimeSpan(0,5,19), new TimeSpan(0,7,19), new TimeSpan(0,09,29), new TimeSpan(0,11,15)},
            };
            TimeSpan[] casy = new TimeSpan[mezicasy.GetLength(0)];
            for (int i = 0; i < casy.Length; i++) {
                casy[i] = mezicasy[i, mezicasy.GetLength(1) - 1];
            }

            for (int kolo = 0; kolo < mezicasy.GetLength(1); kolo++) {
                int nejlepsi = 0;
                for (int zavodnik = 1; zavodnik < mezicasy.GetLength(0); zavodnik++) {
                    if (mezicasy[zavodnik, kolo] < mezicasy[nejlepsi, kolo]) {
                        nejlepsi = zavodnik;
                    }
                }
                casy[nejlepsi] -= new TimeSpan(0, 0, 30);
            }

            int[] poradi = new int[mezicasy.GetLength(0)];

            TimeSpan predchoziCas = new TimeSpan(0, 0, 0);
            for (int i = 0; i < poradi.Length; i++) {
                int nejlepsi = 0;
                for (int zavodnik = 1; zavodnik < casy.Length; zavodnik++) {
                    if (casy[zavodnik] < casy[nejlepsi] && casy[zavodnik] > predchoziCas) {
                        nejlepsi = zavodnik;
                    }
                }
                poradi[i] = nejlepsi;
                predchoziCas = casy[nejlepsi];
            }

            for (int i = 0; i < poradi.Length; i++) {
                Console.WriteLine("{0}. zavodnik č.{1} - {2}", i + 1, poradi[i], casy[poradi[i]].ToString("mm\\:ss"));
            }

            Console.ReadKey();
        }
    }
}
