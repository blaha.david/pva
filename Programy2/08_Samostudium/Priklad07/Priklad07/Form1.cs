﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Priklad07 {

    public delegate double Function(double x);

    public partial class Form1 : Form {

        const double step = 0.1;
        double od, @do;
        Function fun;

        string[] s_Functions = { "2*sin(x)", "|cos(x)|", "1/x", "|x+|2x|-2|" };
        Function[] functions = { Fun_2xSin, Fun_AbsCos, Fun_1divX, Fun_xAbs2x };

        public Form1() {
            InitializeComponent();
            inputOd.Tag = errorOd;
            inputDo.Tag = errorDo;

            inputFunkce.DataSource = s_Functions;
            od = GetValue(inputOd);
            @do = GetValue(inputDo);

            fun = Fun_2xSin;
        }

        static private double Fun_2xSin(double x) {
            return 2 * x * Math.Sin(x);
        }

        static private double Fun_AbsCos(double x) {
            return 3 * Math.Abs(Math.Cos(x));
        }

        static private double Fun_1divX(double x) {
            return 1 / x;
        }

        static private double Fun_xAbs2x(double x) {
            return Math.Abs(x + Math.Abs(2 * x) - 2);
        }

        private void btnVykreslit_Click(object sender, EventArgs e) {
            double new_od = GetValue(inputOd);
            double new_do = GetValue(inputDo);

            if (double.IsNaN(new_od) || double.IsNaN(new_do)) return;
            if (new_od >= new_do) {
                errorDoMensi.Visible = true;
                return;
            }
            errorDoMensi.Visible = false;

            od = new_od;
            @do = new_do;

            Invalidate(invalidateChildren: true);
        }

        private void canvas_Paint(object sender, PaintEventArgs e) {
            Graphics g = e.Graphics;
            Pen p = new Pen(Color.Black);
            Pen pGray = new Pen(Color.LightGray);

            PointF[] curve = new PointF[(int)((@do - od) / step)];

            g.TranslateTransform(40, canvas.Height / 2);

            g.DrawLine(pGray, -20, 0, canvas.Width - 60, 0);
            g.DrawLine(pGray, 0, -canvas.Height / 2 + 20, 0, canvas.Height / 2 - 20);

            g.DrawString("x", new Font("Arial", 15), new SolidBrush(Color.Black), canvas.Width - 60, 0);
            g.DrawString("y", new Font("Arial", 15), new SolidBrush(Color.Black), 0, -canvas.Height / 2 + 5);

            int i = 0;
            double maxY = double.MinValue, minY = double.MaxValue;
            for (double x = od; x < @do; x += step) {
                x = Math.Round(x, 5); //float rounding error
                double y = fun(x);
                if (double.IsNaN(y) || double.IsInfinity(y)) y = 0;
                if (y < minY) minY = y;
                if (y > maxY) maxY = y;


                curve[i++] = new PointF((float)Map(x, od, @do, 0, canvas.Width - 60), -(float)y);
            }

            g.ScaleTransform(1, (canvas.Height / 2) / (float)Math.Max(Math.Abs(maxY), Math.Abs(minY)));

            g.DrawCurve(p, curve);
        }




        private double GetValue(TextBox input) {
            string text = input.Text;
            if (!IsNumber(text)) {
                (input.Tag as Label).Visible = true;
                return double.NaN;
            }
            (input.Tag as Label).Visible = false;
            return double.Parse(text);
        }



        private bool IsNumber(string input) {
            return double.TryParse(input, out double num);
        }

        private void inputFunkce_SelectedIndexChanged(object sender, EventArgs e) {
            fun = functions[inputFunkce.SelectedIndex];
        }

        private double Map(double X, double A, double B, double C, double D) {
            return (X - A) / (B - A) * (D - C) + C;
        }
    }
}
