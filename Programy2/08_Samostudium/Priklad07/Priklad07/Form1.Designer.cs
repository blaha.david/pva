﻿namespace Priklad07 {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.canvas = new System.Windows.Forms.Panel();
            this.labelOd = new System.Windows.Forms.Label();
            this.labelDo = new System.Windows.Forms.Label();
            this.inputOd = new System.Windows.Forms.TextBox();
            this.errorOd = new System.Windows.Forms.Label();
            this.errorDo = new System.Windows.Forms.Label();
            this.inputDo = new System.Windows.Forms.TextBox();
            this.btnVykreslit = new System.Windows.Forms.Button();
            this.inputFunkce = new System.Windows.Forms.ComboBox();
            this.errorDoMensi = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // canvas
            // 
            this.canvas.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.canvas.Location = new System.Drawing.Point(346, 12);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(432, 426);
            this.canvas.TabIndex = 0;
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            // 
            // labelOd
            // 
            this.labelOd.AutoSize = true;
            this.labelOd.Location = new System.Drawing.Point(63, 134);
            this.labelOd.Name = "labelOd";
            this.labelOd.Size = new System.Drawing.Size(21, 13);
            this.labelOd.TabIndex = 2;
            this.labelOd.Text = "Od";
            // 
            // labelDo
            // 
            this.labelDo.AutoSize = true;
            this.labelDo.Location = new System.Drawing.Point(63, 264);
            this.labelDo.Name = "labelDo";
            this.labelDo.Size = new System.Drawing.Size(21, 13);
            this.labelDo.TabIndex = 3;
            this.labelDo.Text = "Do";
            // 
            // inputOd
            // 
            this.inputOd.Location = new System.Drawing.Point(90, 131);
            this.inputOd.Name = "inputOd";
            this.inputOd.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.inputOd.Size = new System.Drawing.Size(123, 20);
            this.inputOd.TabIndex = 4;
            this.inputOd.Text = "0.0";
            // 
            // errorOd
            // 
            this.errorOd.AutoSize = true;
            this.errorOd.ForeColor = System.Drawing.Color.Red;
            this.errorOd.Location = new System.Drawing.Point(103, 167);
            this.errorOd.Name = "errorOd";
            this.errorOd.Size = new System.Drawing.Size(137, 13);
            this.errorOd.TabIndex = 5;
            this.errorOd.Text = "Zadaná hodnota není číslo";
            this.errorOd.Visible = false;
            // 
            // errorDo
            // 
            this.errorDo.AutoSize = true;
            this.errorDo.ForeColor = System.Drawing.Color.Red;
            this.errorDo.Location = new System.Drawing.Point(103, 298);
            this.errorDo.Name = "errorDo";
            this.errorDo.Size = new System.Drawing.Size(137, 13);
            this.errorDo.TabIndex = 6;
            this.errorDo.Text = "Zadaná hodnota není číslo";
            this.errorDo.Visible = false;
            // 
            // inputDo
            // 
            this.inputDo.Location = new System.Drawing.Point(90, 261);
            this.inputDo.Name = "inputDo";
            this.inputDo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.inputDo.Size = new System.Drawing.Size(116, 20);
            this.inputDo.TabIndex = 7;
            this.inputDo.Text = "5.0";
            // 
            // btnVykreslit
            // 
            this.btnVykreslit.Location = new System.Drawing.Point(92, 379);
            this.btnVykreslit.Name = "btnVykreslit";
            this.btnVykreslit.Size = new System.Drawing.Size(114, 39);
            this.btnVykreslit.TabIndex = 8;
            this.btnVykreslit.Text = "Vykreslit";
            this.btnVykreslit.UseVisualStyleBackColor = true;
            this.btnVykreslit.Click += new System.EventHandler(this.btnVykreslit_Click);
            // 
            // inputFunkce
            // 
            this.inputFunkce.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.inputFunkce.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputFunkce.FormattingEnabled = true;
            this.inputFunkce.Location = new System.Drawing.Point(59, 27);
            this.inputFunkce.Name = "inputFunkce";
            this.inputFunkce.Size = new System.Drawing.Size(217, 33);
            this.inputFunkce.TabIndex = 9;
            this.inputFunkce.SelectedIndexChanged += new System.EventHandler(this.inputFunkce_SelectedIndexChanged);
            // 
            // errorDoMensi
            // 
            this.errorDoMensi.AutoSize = true;
            this.errorDoMensi.ForeColor = System.Drawing.Color.Red;
            this.errorDoMensi.Location = new System.Drawing.Point(38, 311);
            this.errorDoMensi.Name = "errorDoMensi";
            this.errorDoMensi.Size = new System.Drawing.Size(273, 13);
            this.errorDoMensi.TabIndex = 10;
            this.errorDoMensi.Text = "Zadaná hodnota musí být větší než zadaná hodnota Od";
            this.errorDoMensi.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.errorDoMensi);
            this.Controls.Add(this.inputFunkce);
            this.Controls.Add(this.btnVykreslit);
            this.Controls.Add(this.inputDo);
            this.Controls.Add(this.errorDo);
            this.Controls.Add(this.errorOd);
            this.Controls.Add(this.inputOd);
            this.Controls.Add(this.labelDo);
            this.Controls.Add(this.labelOd);
            this.Controls.Add(this.canvas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel canvas;
        private System.Windows.Forms.Label labelOd;
        private System.Windows.Forms.Label labelDo;
        private System.Windows.Forms.TextBox inputOd;
        private System.Windows.Forms.Label errorOd;
        private System.Windows.Forms.Label errorDo;
        private System.Windows.Forms.TextBox inputDo;
        private System.Windows.Forms.Button btnVykreslit;
        private System.Windows.Forms.ComboBox inputFunkce;
        private System.Windows.Forms.Label errorDoMensi;
    }
}

