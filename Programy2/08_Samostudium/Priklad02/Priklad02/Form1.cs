﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Priklad02 {
    public partial class Form1 : Form {
        public Form1 () {
            InitializeComponent();
        }

        private void DrawRectangle (Graphics g, Pen p, int x, int y) {
            g.DrawLines(p, new Point[] { new Point(x, y), new Point(x + 200, y), new Point(x + 200, y + 100), new Point(x, y + 100), new Point(x, y), });
        }

        private void DrawCircle (Graphics g, Pen p, int x, int y, int r) {
            g.DrawEllipse(p, x-r, y-r, 2 * r, 2 * r);
        }

        private void Form1_Paint (object sender, PaintEventArgs e) {
            Graphics g = e.Graphics;
            Pen p = new Pen(Color.Black);
            Pen pr = new Pen(Color.Red);
            Pen pb = new Pen(Color.Blue);

            DrawRectangle(g, p, 0, 0);
            DrawRectangle(g, pr, 250, 0);

            DrawCircle(g, pb, 100, 300, 75);

            System.Drawing.Drawing2D.Matrix m = g.Transform;
            g.TranslateTransform(250, 200);
            DrawRectangle(g, p, 0, 0);
            DrawRectangle(g, pr, 250, 0);
            g.Transform = m;


            g.RotateTransform(20);
            DrawRectangle(g, p, 0, 0);
            DrawRectangle(g, pr, 250, 0);

            g.ScaleTransform(0.25f, 0.75f);
            DrawRectangle(g, p, 0, 0);
            DrawRectangle(g, pr, 250, 0);
        }
    }
}
