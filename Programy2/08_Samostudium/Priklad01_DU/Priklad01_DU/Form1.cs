﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Priklad01_DU {
    public partial class Form1 : Form {
        public Form1 () {
            InitializeComponent();
        }

        private void Form1_Paint (object sender, PaintEventArgs e) {
            Pen pen = new Pen(Color.Black);
            int w = ((Form)sender).Width;
            int h = ((Form)sender).Height;

            int size = Math.Min(w, h) / 3;

            int x1 = w / 2 - size / 2;
            int y1 = h / 2 - size / 2;
            int x2 = w / 2 + size / 2;
            int y2 = h / 2 + size / 2;

            e.Graphics.DrawRectangle(pen, x1, y1, size, size);

            e.Graphics.DrawLine(pen, x1, y1, x2, y2);
            e.Graphics.DrawLine(pen, x1, y2, x2, y1);

            e.Graphics.DrawEllipse(pen, x1, y1, size, size);

            e.Graphics.DrawLine(pen, x1, y1, x1 + size / 2, y1 - size / 2);
            e.Graphics.DrawLine(pen, x2, y1, x1 + size / 2, y1 - size / 2);

        }

        private void Form1_Resize (object sender, System.EventArgs e) {
            ((Form)sender).Invalidate();
        }
    }
}
