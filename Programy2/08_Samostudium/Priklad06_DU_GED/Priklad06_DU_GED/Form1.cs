﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Priklad06_DU_GED {

    public partial class Form1 : Form {
        ObjectType drawnObjectType;
        Point start, end;
        Graphics g;
        Pen p;

        List<GraphicObject> objects;

        public Form1() {
            objects = new List<GraphicObject>();
            drawnObjectType = ObjectType.Line;
            start = new Point(0, 0);
            end = new Point(0, 0);
            p = new Pen(Color.Black);

            InitializeComponent();

            g = canvas.CreateGraphics();
        }

        private void Form1_Load(object sender, EventArgs e) {

            using (OpenFileDialog openFileDialog = new OpenFileDialog()) {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "Data|*.dat|All files (*.*)|*.*";
                openFileDialog.Title = "Open your project";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK) {
                    using (StreamReader r = new StreamReader(openFileDialog.FileName)) {
                        while (r.Peek() != -1) {
                            
                            string[] data = r.ReadLine().Split(',');
                            try {
                                switch (data[0]) {
                                    case "L":
                                        objects.Add(new Line(new Point(int.Parse(data[1]), int.Parse(data[2])), new Point(int.Parse(data[3]), int.Parse(data[4]))));
                                        break;
                                    case "E":
                                        objects.Add(new Ellipse(new Point(int.Parse(data[1]), int.Parse(data[2])), int.Parse(data[3])));
                                        break;
                                    default:
                                        break;
                                }
                            } catch (FormatException) {; }
                        }
                    }
                }
            }


            btn_line.Select();
        }

        private void btn_line_Click(object sender, EventArgs e) {
            drawnObjectType = ObjectType.Line;
        }

        private void btn_ellipse_Click(object sender, EventArgs e) {
            drawnObjectType = ObjectType.Ellipse;
        }

        private void canvas_MouseDown(object sender, MouseEventArgs e) {
            start = e.Location;
        }

        private void canvas_MouseUp(object sender, MouseEventArgs e) {
            end = e.Location;
            switch (drawnObjectType) {
                case ObjectType.Line:
                    objects.Add(new Line(start, end));
                    break;
                case ObjectType.Ellipse:
                    int r = Math.Max(Math.Abs(start.X - end.X), Math.Abs(start.Y - end.Y));
                    objects.Add(new Ellipse(start, r));
                    break;
                default:
                    break;
            }

            listBox.DataSource = null; //update
            listBox.DataSource = objects;

            Invalidate(invalidateChildren: true);

        }

        private void canvas_MouseMove(object sender, MouseEventArgs e) {
            mouseLabel.Text = e.X + ", " + e.Y;

            if (e.Button == MouseButtons.Left) {
                end = e.Location;
                Invalidate(invalidateChildren: true);
            }
        }

        private void canvas_Paint(object sender, PaintEventArgs e) {

            //draw exising objects
            foreach (var o in objects) {
                switch (o.type) {
                    case ObjectType.Line:
                        Line l = (Line)o;
                        g.DrawLine(p, l.start, l.end);
                        break;
                    case ObjectType.Ellipse:
                        Ellipse el = (Ellipse)o;
                        g.DrawEllipse(p, el.center.X - el.radius, el.center.Y - el.radius, el.radius * 2, el.radius * 2);
                        break;
                    default:
                        break;
                }
            }


            //draw preview of new object
            switch (drawnObjectType) {
                case ObjectType.Line:
                    g.DrawLine(p, start, end);
                    break;
                case ObjectType.Ellipse:
                    int r = Math.Max(Math.Abs(start.X - end.X), Math.Abs(start.Y - end.Y));
                    g.DrawEllipse(p, start.X - r, start.Y - r, r * 2, r * 2);
                    break;
                default:
                    break;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Data|*.dat";
            saveFileDialog1.Title = "Save your objects";
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != "") {
                using (var w = new StreamWriter(saveFileDialog1.FileName)) {
                    foreach (var o in objects) {
                        w.WriteLine(o.ToSaveFile());
                    }
                }
            }
        }

    }


    public enum ObjectType {
        Line = 'L',
        Ellipse = 'E',
    }

    public abstract class GraphicObject {
        public ObjectType type;

        public GraphicObject(ObjectType type) {
            this.type = type;
        }

        public abstract string ToSaveFile();
    }

    public class Ellipse : GraphicObject {
        public Point center;
        public int radius;

        public Ellipse(Point center, int radius) : base(ObjectType.Ellipse) {
            this.center = center;
            this.radius = radius;
        }

        public override string ToString() {
            return string.Format("{0}  |  [{1},{2}],  r={3}", (char)type, center.X, center.Y, radius);
        }

        public override string ToSaveFile() {
            return (char)type + "," + center.X + "," + center.Y + "," + radius;
        }
    }

    public class Line : GraphicObject {
        public Point start;
        public Point end;

        public Line(Point start, Point end) : base(ObjectType.Line) {
            this.start = start;
            this.end = end;
        }

        public override string ToString() {
            return string.Format("{0}  |  [{1},{2}],  [{3},{4}]", (char)type, start.X, start.Y, end.X, end.Y);
        }

        public override string ToSaveFile() {
            return (char)type + "," + start.X + "," + start.Y + "," + end.X + "," + end.Y;
        }
    }
}
