﻿namespace Priklad02 {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent () {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.inJmeno = new System.Windows.Forms.TextBox();
            this.inPrijmeni = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.inNarozeni = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.inVzdelani = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.inPozice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.inPlat = new System.Windows.Forms.NumericUpDown();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.outZZZ = new System.Windows.Forms.ListBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.inPlat)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jméno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Příjmení";
            // 
            // inJmeno
            // 
            this.inJmeno.Location = new System.Drawing.Point(124, 46);
            this.inJmeno.Name = "inJmeno";
            this.inJmeno.Size = new System.Drawing.Size(201, 20);
            this.inJmeno.TabIndex = 2;
            // 
            // inPrijmeni
            // 
            this.inPrijmeni.Location = new System.Drawing.Point(124, 77);
            this.inPrijmeni.Name = "inPrijmeni";
            this.inPrijmeni.Size = new System.Drawing.Size(201, 20);
            this.inPrijmeni.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Datum narození";
            // 
            // inNarozeni
            // 
            this.inNarozeni.Location = new System.Drawing.Point(125, 113);
            this.inNarozeni.Name = "inNarozeni";
            this.inNarozeni.Size = new System.Drawing.Size(200, 20);
            this.inNarozeni.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 198);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Dosažené vzdělání";
            // 
            // inVzdelani
            // 
            this.inVzdelani.FormattingEnabled = true;
            this.inVzdelani.Location = new System.Drawing.Point(128, 198);
            this.inVzdelani.Name = "inVzdelani";
            this.inVzdelani.Size = new System.Drawing.Size(197, 21);
            this.inVzdelani.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 235);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Pracovní pozice";
            // 
            // inPozice
            // 
            this.inPozice.Location = new System.Drawing.Point(128, 235);
            this.inPozice.Name = "inPozice";
            this.inPozice.Size = new System.Drawing.Size(197, 20);
            this.inPozice.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(60, 275);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Hrubý plat";
            // 
            // inPlat
            // 
            this.inPlat.DecimalPlaces = 2;
            this.inPlat.Location = new System.Drawing.Point(128, 275);
            this.inPlat.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.inPlat.Name = "inPlat";
            this.inPlat.Size = new System.Drawing.Size(197, 20);
            this.inPlat.TabIndex = 11;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.Location = new System.Drawing.Point(63, 351);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(80, 29);
            this.btnSubmit.TabIndex = 12;
            this.btnSubmit.Text = "Odeslat";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // outZZZ
            // 
            this.outZZZ.FormattingEnabled = true;
            this.outZZZ.Location = new System.Drawing.Point(430, 46);
            this.outZZZ.Name = "outZZZ";
            this.outZZZ.Size = new System.Drawing.Size(230, 368);
            this.outZZZ.TabIndex = 13;
            this.outZZZ.DoubleClick += new System.EventHandler(this.outZZZ_DoubleClick);
            // 
            // btnRemove
            // 
            this.btnRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Location = new System.Drawing.Point(238, 351);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(83, 29);
            this.btnRemove.TabIndex = 14;
            this.btnRemove.Text = "Odstranit";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(149, 351);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 29);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Zrusit";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 450);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.outZZZ);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.inPlat);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.inPozice);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.inVzdelani);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.inNarozeni);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.inPrijmeni);
            this.Controls.Add(this.inJmeno);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.inPlat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox inJmeno;
        private System.Windows.Forms.TextBox inPrijmeni;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker inNarozeni;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox inVzdelani;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox inPozice;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown inPlat;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.ListBox outZZZ;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnCancel;
    }
}

