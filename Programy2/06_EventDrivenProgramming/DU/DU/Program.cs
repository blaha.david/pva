﻿using System;

namespace DU {
    class Program {
        static void Main (string[] args) {
            Console.WriteLine(Eval("2 + 3 * 4 / 2 - 1"));
            Console.WriteLine(Eval("6*2-3*4+1"));

            //Console.WriteLine(Eval("1+5/2*5-2*1+4-20/5*12-59+123/3"));

            Console.ReadKey();
        }


        static double Eval (string input) {
            Console.WriteLine(input);

            string exp = input.Replace(" ", "");
            bool priorityOperation = true; //if it contains '*' or '/'
            while (priorityOperation) {
                priorityOperation = false;
                int opIndex = exp.IndexOfAny(new char[] { '*', '/' });
                if (opIndex != -1) {
                    int prev = exp.LastIndexOfAny(new char[] { '+', '-' }, opIndex) + 1;
                    int next = exp.IndexOfAny(new char[] { '+', '-', '*', '/' }, opIndex + 1);
                    if (prev == -1) {
                        prev = 0;
                    }
                    if (next == -1) {
                        next = exp.Length;
                    }
                    string singleExp = exp.Substring(prev, next - prev);
                    exp = exp.Replace(singleExp, EvalSingle(singleExp).ToString());
                    //Console.WriteLine(exp);
                    priorityOperation = true;
                }
            }
            bool operation = true; //if it contains '+' or '-'
            while (operation) {
                operation = false;
                int opIndex = exp.IndexOfAny(new char[] { '+', '-' }, 1);
                if (opIndex != -1) {
                    int prev = 0;
                    int next = exp.IndexOfAny(new char[] { '+', '-' }, opIndex + 1);
                    if (next == -1) {
                        next = exp.Length;
                    }
                    string singleExp = exp.Substring(prev, next - prev);
                    exp = exp.Replace(singleExp, EvalSingle(singleExp).ToString());
                    //Console.WriteLine(exp);
                    operation = true;
                }
            }
            return double.Parse(exp);
        }

        static double EvalSingle (string exp) {
            int opIndex = exp.IndexOfAny(new char[] { '+', '-', '*', '/' }, 1);
            char operation = exp[opIndex];
            double num1 = double.Parse(exp.Substring(0, opIndex));
            double num2 = double.Parse(exp.Substring(opIndex + 1, (exp.Length) - (opIndex + 1)));
            switch (operation) {
                case '+':
                    return num1 + num2;
                case '-':
                    return num1 - num2;
                case '*':
                    return num1 * num2;
                case '/':
                    return num1 / num2;
            }
            return double.MaxValue;
        }
    }
}
