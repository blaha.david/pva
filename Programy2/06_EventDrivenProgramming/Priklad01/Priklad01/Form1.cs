﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Priklad01 {
    public partial class Form1 : Form {

        private string ans = "";

        public Form1 () {
            InitializeComponent();
        }

        private void btn_Click (object sender, EventArgs e) {
            textBox.Text += ((Button)sender).Text;
        }

        private void btn_clear_Click (object sender, EventArgs e) {
            textBox.Text = "";
        }

        private void btn_back_Click (object sender, EventArgs e) {
            if (textBox.Text.Length > 0) {
                textBox.Text = textBox.Text.Substring(0, textBox.Text.Length - 1);
            }
        }

        private void btn_equal_Click (object sender, EventArgs e) {
            textBox.Text = MyMath.Eval(textBox.Text).ToString();
            ans = textBox.Text;
        }

        private void btn_ans_Click (object sender, EventArgs e) {
            textBox.Text += ans;
        }
    }



    public class MyMath {
        public static double Eval (string input) {
            Console.WriteLine(input);

            string exp = input.Replace(" ", "");
            bool priorityOperation = true; //if it contains '*' or '/'
            while (priorityOperation) {
                priorityOperation = false;
                int opIndex = exp.IndexOfAny(new char[] { '*', '/' });
                if (opIndex != -1) {
                    int prev = exp.LastIndexOfAny(new char[] { '+', '-' }, opIndex) + 1;
                    int next = exp.IndexOfAny(new char[] { '+', '-', '*', '/' }, opIndex + 1);
                    if (prev == -1) {
                        prev = 0;
                    }
                    if (next == -1) {
                        next = exp.Length;
                    }
                    string singleExp = exp.Substring(prev, next - prev);
                    exp = exp.Replace(singleExp, EvalSingle(singleExp).ToString());
                    //Console.WriteLine(exp);
                    priorityOperation = true;
                }
            }
            bool operation = true; //if it contains '+' or '-'
            while (operation) {
                operation = false;
                int opIndex = -1;
                if (exp.Length > 0) {
                    opIndex = exp.IndexOfAny(new char[] { '+', '-' }, 1);
                }
                if (opIndex != -1) {
                    int prev = 0;
                    int next = exp.IndexOfAny(new char[] { '+', '-' }, opIndex + 1);
                    if (next == -1) {
                        next = exp.Length;
                    }
                    string singleExp = exp.Substring(prev, next - prev);
                    exp = exp.Replace(singleExp, EvalSingle(singleExp).ToString());
                    //Console.WriteLine(exp);
                    operation = true;
                }
            }
            double ret;
            if (!double.TryParse(exp, out ret)) {
                return 0.0;
            }
            return ret;
        }

        static double EvalSingle (string exp) {
            int opIndex = exp.IndexOfAny(new char[] { '+', '-', '*', '/' }, 1);
            char operation = exp[opIndex];
            double num1 = double.Parse(exp.Substring(0, opIndex));
            double num2 = double.Parse(exp.Substring(opIndex + 1, (exp.Length) - (opIndex + 1)));
            switch (operation) {
                case '+':
                    return num1 + num2;
                case '-':
                    return num1 - num2;
                case '*':
                    return num1 * num2;
                case '/':
                    return num1 / num2;
            }
            return double.MaxValue;
        }
    }
}
