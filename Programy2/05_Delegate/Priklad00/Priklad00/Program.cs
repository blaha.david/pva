﻿using System;

namespace Priklad00 {

    public delegate void Tiknuto ();

    class Budik {
        public event Tiknuto OnTik;

        public Budik (Tiknuto t) {
            OnTik += t;
        }

        public void Ukaz () {
            OnTik();
        }
    }



    class Program {
        public static void Main () {
            Budik b = new Budik(Test);
            b.Ukaz();
            Console.WriteLine("------------");

            b.OnTik += Test2;
            b.Ukaz();

            Console.WriteLine("------------");
            b.OnTik -= Test;
            b.Ukaz();

            Console.ReadKey();
        }


        public static void Test () {
            Console.WriteLine("test");
        }

        public static void Test2 () {
            Console.WriteLine("test2");
        }
    }
























    public delegate void Vypis ();

    class Priklad {

        public event Vypis deleg;

        public Priklad () {

            deleg = Test;
            deleg += Test2;

            deleg();
        }

        public static void Test () {
            Console.WriteLine("test1");
        }

        public static void Test2 () {
            Console.WriteLine("test2");
        }

        public static void Mains () {
            Priklad p = new Priklad();
            Console.ReadKey();
        }
    }
}
