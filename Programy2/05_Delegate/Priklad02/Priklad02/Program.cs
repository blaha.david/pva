﻿using System;

namespace Priklad02 {

    public delegate bool Podminka (string s);

    class Program {
        static void Main (string[] args) {

            Console.Write("Zadejte string o minimalne dvou znacich:");
            string s = Nacti(Delka);

            Console.Write("Zadejte cele cislo:");
            s = Nacti(CeleCislo);

            Console.Write("Zadejte string ktery obsahuje 'a':");
            s = Nacti(ObsahujeA);

            Console.ReadKey();
        }

        public static string Nacti (Podminka podminka) {
            string s;
            while (true) {
                s = Console.ReadLine();
                if (podminka(s)) {
                    break;
                } else {
                    Console.WriteLine("Vstup nesplnuje kriteria!");
                    Console.Write("Opakujte zadani: ");
                }
            }
            return s;
        }


        public static bool Delka (string s) {
            return s.Length >= 2;
        }

        public static bool CeleCislo (string s) {
            int i;
            return int.TryParse(s, out i);
        }

        public static bool ObsahujeA (string s) {
            return s.Contains("a");
        }

    }
}
