﻿using System;

namespace Priklad01 {
    class Program {
        public static void Main () {
            Krychle[] k = { new Krychle(1, 1, 1, 1), new Krychle(2, 2, 2, 2) };

            k[0].Vypis();
            k[1].Vypis();

            Koule[] kk = { new Koule(3, 3, 3, 3), new Koule(4, 4, 4, 4) };

            kk[0].Vypis();
            kk[1].Vypis();

            Console.ReadKey();
        }
    }

    class Krychle {

        public double Hmotnost { get; set; }
        public double Objem { get; set; }
        public double Hustota { get; set; }
        public double Rozmer { get; set; }

        public Krychle (double hmotnost, double objem, double hustota, double rozmer) {
            Hmotnost = hmotnost;
            Objem = objem;
            Hustota = hustota;
            Rozmer = rozmer;
        }

        public void Vypis () {
            Console.WriteLine($"{Hmotnost}, {Objem}, {Hustota}, {Rozmer}");
        }
    }

    class Koule {
        private double _hmotnost;
        public double Hmotnost { get { return _hmotnost; } private set { if (value <= 0) throw new ArgumentOutOfRangeException(); else _hmotnost = value; } }
        private double _objem;
        public double Objem { get { return _objem; } private set { if (value <= 0) throw new ArgumentOutOfRangeException(); else _objem = value; } }
        private double _hustota;
        public double Hustota { get { return _hustota; } private set { if (value <= 0) throw new ArgumentOutOfRangeException(); else _hustota = value; } }
        private double _rozmer;
        public double Rozmer { get { return _rozmer; } private set { if (value <= 0) throw new ArgumentOutOfRangeException(); else _rozmer = value; } }

        public Koule (double hmotnost, double objem, double hustota, double rozmer) {
            Hmotnost = hmotnost;
            Objem = objem;
            Hustota = hustota;
            Rozmer = rozmer;
        }

        public void Vypis () {
            Console.WriteLine($"{Hmotnost}, {Objem}, {Hustota}, {Rozmer}");
        }
    }
}
