﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Priklad05_DU {
    public partial class Form1 : Form {

        DateTime now;

        public Form1 () {
            InitializeComponent();
        }

        private void timer1_Tick (object sender, EventArgs e) {
            now = DateTime.Now;
            Invalidate();
        }

        private void Form1_Load (object sender, EventArgs e) {
            now = DateTime.Now;
            timer1.Start();
        }

        private void Form1_Paint (object sender, PaintEventArgs e) {
            Graphics g = e.Graphics;

            //draw clock
            Pen p = new Pen(Color.Black);
            SolidBrush b = new SolidBrush(Color.Black);

            float diameter = Math.Min(ClientRectangle.Width, ClientRectangle.Height);
            float radius = diameter / 2;

            g.DrawEllipse(p, 0, 0, diameter, diameter);

            g.TranslateTransform(radius, radius);
            g.RotateTransform(-90); //0 degrees is up

            Matrix m = g.Transform;
            for (int i = 0; i < 12; i++) {
                g.RotateTransform(i * 360 / 12);
                g.TranslateTransform(radius, 0);
                if (i % 3 == 0) {
                    g.FillRectangle(b, -85, -8, 80, 16);
                } else {
                    g.FillRectangle(b, -45, -2, 40, 4);
                }
                g.Transform = m;
            }


            //draw hands
            Brush bHour = new SolidBrush(Color.Red);
            g.RotateTransform((now.Hour % 12) * 30);
            g.FillRectangle(bHour, 0, -5, radius/2, 12);
            g.Transform = m;

            Brush bMinute = new SolidBrush(Color.Blue);
            g.RotateTransform(now.Minute * 6);
            g.FillRectangle(bMinute, 0, -4, radius/5*4, 8);
            g.Transform = m;

            Brush bSecond = new SolidBrush(Color.LightGreen);
            g.RotateTransform(now.Second * 6);
            g.FillRectangle(bSecond, 0, -2, radius, 4);
        }

        private void Form1_Resize (object sender, EventArgs e) {
            Invalidate();
        }
    }
}
