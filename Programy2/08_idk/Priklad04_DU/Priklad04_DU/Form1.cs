﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Priklad04_DU {
    public partial class Form1 : Form {
        public Form1 () {
            InitializeComponent();
        }

        private void Form1_Paint (object sender, PaintEventArgs e) {
            Graphics g = e.Graphics;
            SolidBrush b1 = new SolidBrush(Color.FromArgb(255, 210, 0));
            SolidBrush b2 = new SolidBrush(Color.Orange);
            SolidBrush b3 = new SolidBrush(Color.OrangeRed);

            int w = ClientRectangle.Width;
            int h = ClientRectangle.Height;

            float size3 = Math.Min(w, h / 2);
            float size1 = size3 / 3;
            float size2 = size1 * 2;

            g.FillEllipse(b3, (w / 2) - (size3 / 2), h - size3, size3, size3);
            g.FillEllipse(b2, (w / 2) - (size2 / 2), h - size3 - size2, size2, size2);
            g.FillEllipse(b1, (w / 2) - (size1 / 2), h - size3 - size2 - size1, size1, size1);
        }

        private void Form1_Resize (object sender, EventArgs e) {
            Invalidate();
        }
    }
}
