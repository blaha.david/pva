﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Priklad03_DU {
    public partial class Form1 : Form {
        public Form1 () {
            InitializeComponent();
        }

        private void Form1_Paint (object sender, PaintEventArgs e) {
            Graphics g = e.Graphics;
            Pen p = new Pen(Color.Black);
            SolidBrush b = new SolidBrush(Color.Black);

            float diameter = 400;
            float radius = diameter / 2;

            g.DrawEllipse(p, 0, 0, diameter, diameter);

            g.TranslateTransform(radius, radius);

            for (int i = 0; i < 12; i++) {
                Matrix m = g.Transform;
                g.RotateTransform(i * 360 / 12);
                g.TranslateTransform(radius, 0);
                if (i % 3 == 0) {
                    g.FillRectangle(b, -85, -8, 80, 16);
                } else {
                    g.FillRectangle(b, -45, -2, 40, 4);
                }
                g.Transform = m;
            }
        }
    }
}
