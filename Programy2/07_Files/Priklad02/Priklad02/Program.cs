﻿using System;
using System.IO;

namespace Priklad02 {
    class Program {
        static void Main (string[] args) {

            Console.Write("Zadejte nazev vstupniho souboru:"); string fin = Console.ReadLine();
            Console.Write("Zadejte nazev ciloveho souboru:"); string fout = Console.ReadLine();

            using (var sr = new StreamReader(File.OpenRead(fin))) {
                using (var sw = new StreamWriter(File.OpenWrite(fout))) {
                    int i = 1;
                    while (sr.Peek() != -1) {
                        sw.WriteLine(i+++". "+sr.ReadLine());
                    }
                }
            }
        }
    }
}
