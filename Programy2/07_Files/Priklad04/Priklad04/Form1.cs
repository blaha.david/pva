﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Priklad02 {
    public partial class Form1 : Form {
        List<Zamestnanec> listZamestnancu = new List<Zamestnanec>();
        Zamestnanec editing = null;

        public Form1 () {
            InitializeComponent();
            inVzdelani.DataSource = Enum.GetValues(typeof(Vzdelani));
            inVzdelani.DropDownStyle = ComboBoxStyle.DropDownList;

            ClearForm();

            if (File.Exists("zamestnanci.csv")) {
                using (var f = new StreamReader("zamestnanci.csv")) {
                    while (f.Peek() != -1) {
                        string[] data = f.ReadLine().Split(',');
                        string jmeno = data[0];
                        string prijmeni = data[1];
                        if (!DateTime.TryParse(data[2], out DateTime narozeni)) continue;
                        if (!Enum.TryParse(data[3], out Vzdelani vzdelani)) continue;
                        string pozice = data[4];
                        if (!decimal.TryParse(data[5], out decimal plat)) continue;

                        listZamestnancu.Add(new Zamestnanec(jmeno, prijmeni, narozeni, vzdelani, pozice, plat));
                    }
                }
            }

            outZZZ.DataSource = null; //update
            outZZZ.DataSource = listZamestnancu;
        }

        private void btnSubmit_Click (object sender, EventArgs e) {
            string jmeno = inJmeno.Text;
            string prijmeni = inPrijmeni.Text;
            DateTime narozeni = inNarozeni.Value;
            Enum.TryParse(inVzdelani.SelectedValue.ToString(), out Vzdelani vzdelani);
            string pozice = inPozice.Text;
            decimal plat = inPlat.Value;
            if (editing != null) {
                editing.jmeno = jmeno;
                editing.prijmeni = prijmeni;
                editing.narozeni = narozeni;
                editing.vzdelani = vzdelani;
                editing.pozice = pozice;
                editing.plat = plat;
            } else {
                listZamestnancu.Add(new Zamestnanec(jmeno, prijmeni, narozeni, vzdelani, pozice, plat));
            }
            outZZZ.DataSource = null; //update
            outZZZ.DataSource = listZamestnancu;

            ClearForm();
        }

        private void outZZZ_DoubleClick (object sender, EventArgs e) {

            Form form2 = new Form();
            Label label = new Label();
            label.Text = "";

            form2.Controls.Add(label);



            if (((ListBox)sender).SelectedItem == null) {
                return;
            }
            Zamestnanec z = (Zamestnanec)((ListBox)sender).SelectedItem;
            inJmeno.Text = z.jmeno;
            inPrijmeni.Text = z.prijmeni;
            inNarozeni.Value = z.narozeni;
            inVzdelani.SelectedIndex = (int)z.vzdelani;
            inPozice.Text = z.pozice;
            inPlat.Value = z.plat;
            editing = z;
            btnRemove.Visible = true;
            btnSubmit.Text = "Ulozit";

        }

        private void btnRemove_Click (object sender, EventArgs e) {
            if (editing != null) {
                DialogResult dialogResult = MessageBox.Show("Opravdu chcete odstranit zamestnance?", "Odstranit", MessageBoxButtons.YesNoCancel);
                if (dialogResult == DialogResult.Yes) {
                    listZamestnancu.Remove(editing);
                    outZZZ.DataSource = null; //update
                    outZZZ.DataSource = listZamestnancu;
                    ClearForm();
                }
            }
        }

        private void ClearForm () {
            inJmeno.Text = "";
            inPrijmeni.Text = "";
            inNarozeni.Value = DateTime.Now;
            inVzdelani.SelectedIndex = 0;
            inPozice.Text = "";
            inPlat.Value = 0;
            editing = null;
            btnRemove.Visible = false;
            btnSubmit.Text = "Vytvorit";
        }

        private void btnCancel_Click (object sender, EventArgs e) {
            ClearForm();
        }

        private void Form1_FormClosing (object sender, FormClosingEventArgs e) {
            SaveData();
        }

        private void SaveData () {
            using (var f = new StreamWriter("zamestnanci.csv")) {
                foreach (var z in listZamestnancu) {
                    f.WriteLine("{0},{1},{2},{3},{4},{5}", z.jmeno, z.prijmeni, z.narozeni, z.vzdelani, z.pozice, z.plat);
                }
            }
        }

    }


    public class Osoba {
        public string jmeno;
        public string prijmeni;
        public DateTime narozeni;

        public Osoba (string jmeno, string prijmeni, DateTime narozeni) {
            this.jmeno = jmeno ?? throw new ArgumentNullException(nameof(jmeno));
            this.prijmeni = prijmeni ?? throw new ArgumentNullException(nameof(prijmeni));
            this.narozeni = narozeni;
        }

        public Osoba () {
        }
    }

    public class Zamestnanec : Osoba {
        public Vzdelani vzdelani;
        public string pozice;
        public decimal plat;

        public Zamestnanec (string jmeno, string prijmeni, DateTime narozeni, Vzdelani vzdelani, string pozice, decimal plat) : base(jmeno, prijmeni, narozeni) {
            this.vzdelani = vzdelani;
            this.pozice = pozice ?? throw new ArgumentNullException(nameof(pozice));
            this.plat = plat;
        }

        public Zamestnanec () {
        }

        public override string ToString () {
            return jmeno + " " + prijmeni;
        }
    }

    public enum Vzdelani {
        Zadne, Zakladni, Stredni, Vysoke
    }

}
