﻿using System;
using System.IO;

namespace Priklad03 {
    class Program {
        static void Main (string[] args) {

            int index = 0;
            DateTime? lastDt = null;
            double lastTemp = 0;
            double lastHumi = 0;

            DateTime? dtOd = null;
            DateTime? dtDo = null;

            double sumTemp = 0;
            double sumHumi = 0;

            using (var fin = new StreamReader("data.in")) {
                using (var fout = new StreamWriter("data.out", false)) {
                    while (fin.Peek() != -1) {
                        string[] data = fin.ReadLine().Split('|');
                        if (!DateTime.TryParse(data[0], out DateTime dt)) continue;
                        if (!double.TryParse(data[1], out double temp)) {
                            if (lastDt != null) {
                                temp = lastTemp;
                            } else {
                                continue;
                            }
                        }
                        if (temp < -30 || temp > 45) {
                            if (lastDt != null) {
                                temp = lastTemp;
                            } else {
                                continue;
                            }
                        }
                        if (!double.TryParse(data[2], out double humi)) {
                            if (lastDt != null) {
                                humi = lastHumi;
                            } else {
                                continue;
                            }
                        }
                        if (humi < 0 || humi > 100) {
                            if (lastDt != null) {
                                humi = lastHumi;
                            } else {
                                continue;
                            }
                        }

                        fout.WriteLine("{0}|{1}|{2:.0}|{3:.0}", index+1, dt, temp, humi);

                        if (dtOd == null) dtOd = dt;

                        sumTemp += temp;
                        sumHumi += humi;

                        lastDt = dt;
                        lastTemp = temp;
                        lastHumi = humi;
                        index++;
                    }
                    dtDo = lastDt;
                }
            }

            Console.WriteLine("Měření v době od {0} do {1}. Průměrná teplota {2:.00} C, průměrná vlhkost {3:.0}%.", dtOd, dtDo, sumTemp/index, sumHumi/index);

            Console.ReadKey();
        }
    }
}
