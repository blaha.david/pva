﻿using System;
using System.IO;

namespace Priklad01 {
    class Program {
        static void Main (string[] args) {
            
            using (var fs = new StreamReader(File.Open("soubor.txt", FileMode.Open))) {
                while (fs.Peek() != -1) {
                    Console.Write((char)fs.Read());
                }
            }
            Console.ReadKey();
            
        }
    }
}
