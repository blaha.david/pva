﻿using System;

namespace Priklad02 {
    class Program {
        static void Main (string[] args) {
            int m = 7;
            int n = 12;
            int max = 10000;
            //------------------

            int[,] matice = new int[n, m];

            int maxJ = 0;
            int maxI = 0;
            Random r = new Random();
            for (int j = 0; j < matice.GetLength(0); j++) {
                for (int i = 0; i < matice.GetLength(1); i++) {
                    matice[j, i] = r.Next(0, max);
                    if (matice[j, i] > matice[maxJ, maxI]) {
                        maxI = i;
                        maxJ = j;
                    }
                }
            }

            //print header
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            for (int i = 0; i < matice.GetLength(1); i++) {
                Console.Write("{0,4} ", i);
            }
            Console.WriteLine();
            Console.BackgroundColor = ConsoleColor.White;

            for (int j = 0; j < matice.GetLength(0); j++) {
                for (int i = 0; i < matice.GetLength(1); i++) {
                    if (i != 0) {
                        Console.Write(" ");
                    } else {
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.Write("{0,2}:", j);
                    }
                    if (j % 2 == 0) {
                        Console.BackgroundColor = ConsoleColor.DarkGray;
                    } else {
                        Console.BackgroundColor = ConsoleColor.Gray;
                    }
                    if (i == maxI && j == maxJ) {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                    } else {
                        Console.ForegroundColor = ConsoleColor.Black;
                    }
                    Console.Write("{0,4}", matice[j, i]);
                }
                Console.WriteLine();
            }

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Max: ");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.Write("{0,4}", matice[maxJ, maxI]);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" : ");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("[{0},{1}]", maxI, maxJ);

            Console.ReadKey();
        }
    }
}
