﻿using System;

namespace Priklad03 {
    class Program {
        static void Main (string[] args) {

            int n = 9;

            int[,] fancyMatice = FancyMatice(n);
            for (int j = 0; j < fancyMatice.GetLength(1); j++) {
                for (int i = 0; i < fancyMatice.GetLength(0); i++) {
                    Console.Write("{0,2} ", fancyMatice[j, i]);
                }
                Console.WriteLine();
            }


            Console.WriteLine("\n\n");

            int[,] betterFancyMatice = BetterFancyMatice(n);
            for (int j = 0; j < betterFancyMatice.GetLength(1); j++) {
                for (int i = 0; i < betterFancyMatice.GetLength(0); i++) {
                    Console.Write("{0,2} ", betterFancyMatice[j, i]);
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }

        static int[,] FancyMatice (int n) {
            int[,] matice = new int[n, n];
            int cislo = 1;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    matice[j, i] = cislo++;
                }
            }
            return matice;
        }

        static int[,] BetterFancyMatice (int n) {
            int[,] matice = new int[n, n];
            int cislo = 1;
            for (int i = 0; i < n; i++) {
                //if (i % 2 == 0) {
                //    for (int j = 0; j < n; j++) {
                //        matice[j, i] = cislo++;
                //    }
                //} else {
                //    for (int j = n - 1; j >= 0; j--) {
                //        matice[j, i] = cislo++;
                //    }
                //}

                for (int j = 0; j < n; j++) {
                    matice[(i % 2 == 0) ? j : n-j-1, i] = cislo++;
                }
            }
            return matice;
        }
    }
}
