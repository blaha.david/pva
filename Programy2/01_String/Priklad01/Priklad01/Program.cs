﻿using System;

namespace Priklad01 {
    class Program {
        static void Main (string[] args) {
            string text = Console.ReadLine();
            text = DoplnHvezdickou(text);
            Console.WriteLine(text);
            Console.ReadKey();
        }

        static string DoplnHvezdickou (string text) {
            return text.PadRight(20, '*');
            //return text.Length < 20 ? text + new String('*', 20 - text.Length) : text;
        }

    }
}
