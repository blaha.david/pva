﻿using System;

namespace Priklad08 {
    class Program {
        static string alphabet = "`1234567890-=qwertyuiop[]asdfghjkl;'\\zxcvbnm,./~!@#$%^&*()_+QWERTYUIOP{}ASDFGHJKL:\"|ZXCVBNM<>? ";


        static void Main (string[] args) {

            int[,] A =  {
                {2, 3, 5, 7, 11, 13, 17},
                {19, 23, 29, 31, 37, 39, 41},
                {43, 47, 53, 59, 61, 67, 71}
            };

            Console.WriteLine(A.GetLength(0));
            Console.WriteLine(A.GetLength(1));
            for (int y = 0; y < A.GetLength(0); y++) { // rozsah 1. dimenze
                for (int x = 0; x < A.GetLength(1); x++) { // rozsah 2. dimenze
                    Console.Write(A[y, x] + " ");
                }
            }

            string msg = "100% l3g1T S1fr4";
            string sif = Sifruj(msg, 26000);
            Console.WriteLine(sif);
            string newmsg = Desifruj(sif, 26000);
            Console.WriteLine(newmsg);

            Console.ReadKey();
        }

        static string Sifruj (string msg, int rot) {
            string sif = "";
            foreach (char c in msg) {
                int i = alphabet.IndexOf(c);
                sif += i == -1 ? c : alphabet[(i + rot) % alphabet.Length];
            }
            return sif;
        }

        static string Desifruj (string sif, int rot) {
            return Sifruj(sif, alphabet.Length - (rot % alphabet.Length));
        }


    }
}
