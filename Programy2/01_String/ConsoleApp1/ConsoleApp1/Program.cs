﻿using System;

namespace ConsoleApp1 {
    class Program {
        static void Main (string[] args) {
            MainThing();
        }

        static void MainThing () {
            doTheThing();
            ExitTheThing();
        }

        static void ExitTheThing () {
            Environment.Exit(0);
        }


        static void doTheThing () {
            int[,] thing = {
                {1,2,3,4, 5},
                {6,7,8,9,10},
                {9,8,7,6, 5},
                {4,3,2,1, 0},
                {1,2,3,4, 5},
            };
            
            printTheThings(GetTheThing(thing),GetTheThingAboveTheThing(thing),GetTheThingUnderTheThing(thing));
        }
            
        static void printTheThings (int a, int b, int c) {
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.ReadKey();
        }

        static int GetTheThing (int[,] thing) {
            int sum_diagonal = 0;
            for (int i = 0; i < thing.GetLength(0); i++) {
                sum_diagonal += thing[i, i];
            }
            return sum_diagonal;
        }

        static int GetTheThingAboveTheThing (int[,] thing) {
            int sum_above_diagonal = 0;
            for (int i = 1; i < thing.GetLength(0); i++) {
                for (int j = 0; j < i; j++) {
                    sum_above_diagonal += thing[j, i];
                }
            }
            return sum_above_diagonal;
        }

        static int GetTheThingUnderTheThing (int[,] A) {
            int sum_under_diagonal = 0;
            for (int j = 1; j < A.GetLength(1); j++) {
                for (int i = 0; i < j; i++) {
                    sum_under_diagonal += A[j, i];
                }
            }
            return sum_under_diagonal;
        }
    }
}
