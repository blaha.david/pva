﻿using System;

namespace Priklad02 {
    class Program {
        static void Main (string[] args) {
            string[] zakazanaSlova = new string[] { "" };
            string text = Console.ReadLine();
            string newText = OdstranZakazanaSlova(text, zakazanaSlova);
            Console.WriteLine(newText);
            Console.ReadKey();
        }

        static string OdstranZakazanaSlova (string text, string[] zakazanaSlova) {
            for (int i = 0; i < zakazanaSlova.Length; i++) {
                text = text.Replace(zakazanaSlova[i], "".PadRight(zakazanaSlova[i].Length, '*'));
            }
            return text;
        }
    }
}

