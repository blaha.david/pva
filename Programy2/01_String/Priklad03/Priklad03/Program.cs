﻿using System;

namespace Priklad03 {
    class Program {
        static void Main (string[] args) {
            string nadpis = "NADPIS";
            int width = Console.WindowWidth;
            Console.WriteLine(nadpis.PadLeft((width + nadpis.Length) / 2, '*').PadRight(width, '*'));
            Console.ReadKey();
        }
    }
}
