﻿using System;

namespace Priklad04 {
    class Program {
        static void Main (string[] args) {
            char[] separators = new char[] { ' ', ',', '.', ':' };
            while (true) {
                string text = Console.ReadLine();
                Console.WriteLine("Pocet slov: {0}", text.Split(separators, StringSplitOptions.RemoveEmptyEntries).Length);
            }
        }
    }
}
