﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priklad02_stack {
    class Program {

        // prvni bod
        //static void Main (string[] args) {
        //    MyStack<int> s = new MyStack<int>(2);
        //    s.Push(1);
        //    s.Push(5);
        //    s.Push(10);

        //    s.PrintStack();

        //    Console.WriteLine(s.Pop());
        //    Console.WriteLine(s.Peek());
        //    s.Push(7);
        //    Console.WriteLine(s.Pop());

        //    s.Push(11);
        //    s.Push(12);
        //    s.Push(13);
        //    s.Push(14);
        //    s.Push(15);
        //    s.Push(16);
        //    s.Push(17);
        //    s.Push(18);

        //    s.PrintStack();

        //    Console.ReadKey();
        //}


        // druhy bod
        static void Main (string[] args) {
            const string otevrene_zavorky = "([{<";
            const string zavrene_zavorky = ")]}>";

            MyStack<char> zavorky = new MyStack<char>();

            string prompt = "Zadejte vyraz: ";
            Console.Write(prompt);
            string vyraz = Console.ReadLine();

            for (int i = 0; i < vyraz.Length; i++) {
                if (otevrene_zavorky.Contains(vyraz[i])) {
                    zavorky.Push(zavrene_zavorky[otevrene_zavorky.IndexOf(vyraz[i])]);
                }
                if (zavrene_zavorky.Contains(vyraz[i])) {
                    try {
                        char ocekavana = zavorky.Pop();
                        if (ocekavana != vyraz[i]) {
                            Console.WriteLine(new string(' ', prompt.Length + i) + "^");
                            Console.WriteLine("Error. Ocekavana '{0}'", ocekavana);
                            Console.ReadKey();
                            return;
                        }
                    } catch (IndexOutOfRangeException e) {
                        Console.WriteLine(new string(' ', prompt.Length + i) + "^");
                        Console.WriteLine("Error. '{0}' predchazi '{1}'", vyraz[i], otevrene_zavorky[zavrene_zavorky.IndexOf(vyraz[i])]);
                        Console.ReadKey();
                        return;
                    }
                }
            }
            if (!zavorky.IsEmpty()) {
                Console.WriteLine(new string(' ', prompt.Length + vyraz.Length) + "^");
                Console.WriteLine("Error. Konec vyrazu. Ocekavana {0}", zavorky.Peek());
                Console.ReadKey();
                return;
            }

            Console.WriteLine("V poradku\n:)");
            Console.ReadKey();
        }
    }




    class MyStack<T> {
        private T[] _arr;
        private int _index;

        public MyStack () : this(10) { }
        public MyStack (int initialSize) {
            _arr = new T[initialSize];
            _index = -1;
        }

        public void Push (T element) {
            if (_index >= _arr.Length - 1) {

                //extend arr
                T[] newArr = new T[_arr.Length * 2];

                //copy contents into new array
                Array.Copy(_arr, newArr, _arr.Length);
                //for (int i = 0; i < arr.Length; i++) {
                //    newArr[i] = arr[i];
                //}

                _arr = newArr;
            }
            _arr[++_index] = element;
        }

        public T Pop () {
            if (IsEmpty()) {
                throw new IndexOutOfRangeException("Nothing to pop. Stack is empty.");
                //return default(T);
            } else {
                T ret = _arr[_index];
                _arr[_index] = default(T); // delete
                _index--;
                return ret;
            }
        }

        public T Peek () {
            if (IsEmpty()) {
                throw new IndexOutOfRangeException("Nothing to peek. Stack is empty.");
                //return default(T);
            } else {
                return _arr[_index];
            }
        }

        public bool IsEmpty () {
            return _index == -1;
        }

        public void PrintStack () {
            Console.Write("[");
            for (int i = 0; i <= _index; i++) {
                Console.Write(_arr[i] + ", ");
            }
            Console.WriteLine("]");
        }
    }

}
