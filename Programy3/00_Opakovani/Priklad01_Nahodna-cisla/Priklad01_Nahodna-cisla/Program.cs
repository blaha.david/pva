﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priklad01_Nahodna_cisla {
    class Program {
        static void Main (string[] args) {
            int size = 50;

            Random random = new Random();

            int[] arr = new int[size];

            for (int i = 0; i < size; i++) {
                while (true) {
                    int r = random.Next(0, 100);
                    bool duplicate = Array.IndexOf(arr, r) != -1;
                    //bool duplicate = false;
                    //for (int j = 0; j < i; j++) { //check for duplicates
                    //    if (arr[j] == r) {
                    //        duplicate = true;
                    //        break; 
                    //    }
                    //}
                    if (duplicate) {
                        continue; //try again
                    } else {
                        arr[i] = r;
                        break; //exit loop
                    }
                }
            }
            for (int i = 0; i < size; i += 2) {
                Console.WriteLine("{0}: {1}", i, arr[i]);
            }
            for (int i = 1; i < size; i += 2) {
                Console.WriteLine("{0}: {1}", i, arr[i]);
            }
            Console.ReadKey();
        }
    }
}
