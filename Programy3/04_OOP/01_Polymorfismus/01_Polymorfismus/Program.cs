﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Polymorfismus {

    class Program {
        static void Main (string[] args) {

            List<GeometrickyObjekt> seznam = new List<GeometrickyObjekt>(10);
            seznam.Add(new Kruznice());
            seznam.Add(new Kruznice(3, new Souradnice(1, 1)));
            seznam.Add(new Ctverec(4, new Souradnice(-2, 3)));
            seznam.Add(new Obdelnik(2, 6, new Souradnice(2, 0)));

            foreach (var k in seznam) {
                Console.WriteLine(k);
                Console.WriteLine("    Obsah: {0}\n", k.Obsah());
            }

            Console.ReadKey();
        }
    }

    public class GeometrickyObjekt {
        public Souradnice Souradnice { get; protected set; }
        protected int id;

        public GeometrickyObjekt (Souradnice souradnice) {
            this.Souradnice = souradnice;
            this.id = UID.Next();
        }

        public GeometrickyObjekt () : this(new Souradnice(0, 0)) { }

        public virtual double Obsah () {
            return 0;
        }

        public override string ToString () {
            return string.Format("GeometrickyObjekt #{0}: x={1}, y={2}", id, Souradnice.x, Souradnice.y);
        }

    }

    public class Ctverec : GeometrickyObjekt {
        public double A { get; private set; }

        public Ctverec (double a, Souradnice souradnice) : base(souradnice) {
            A = a;
        }

        public override double Obsah () {
            return A * A;
        }

        public override string ToString () {
            return string.Format("Ctverec #{0}: x={1}, y={2}, a={3}", id, Souradnice.x, Souradnice.y, A);
        }
    }

    public class Obdelnik : GeometrickyObjekt {
        public double A { get; private set; }
        public double B { get; private set; }

        public Obdelnik (double a, double b, Souradnice souradnice) : base(souradnice) {
            A = a;
            B = b;
        }

        public override double Obsah () {
            return A * B;
        }

        public override string ToString () {
            return string.Format("Obdelnik #{0}: x={1}, y={2}, a={3}, b={4}", id, Souradnice.x, Souradnice.y, A, B);
        }
    }

    public class Kruznice : GeometrickyObjekt {
        public double Polomer { get; private set; }

        public Kruznice (double polomer, Souradnice souradnice) : base(souradnice) {
            this.Polomer = polomer;
        }

        public Kruznice (double polomer) : base() {
            this.Polomer = polomer;
        }

        public Kruznice () : this(1) { }

        public override double Obsah () {
            return Math.PI * Polomer * Polomer;
        }

        public double Obvod () {
            return 2 * Math.PI * Polomer;
        }

        public bool Protina (Kruznice druha) {
            return VzdalenostK(druha.Souradnice) < this.Polomer + druha.Polomer;
        }

        public double VzdalenostK (Souradnice bod) {
            double dX = this.Souradnice.x - bod.x;
            double dY = this.Souradnice.y - bod.y;
            return Math.Sqrt(dX * dX + dY * dY);
        }

        public static Kruznice NactiZKonzole () {
            string rawX, rawY, rawR;
            double x, y, r;
            do {
                Console.Write("x: ");
                rawX = Console.ReadLine();
            } while (!double.TryParse(rawX, out x));
            do {
                Console.Write("y: ");
                rawY = Console.ReadLine();
            } while (!double.TryParse(rawY, out y));
            do {
                Console.Write("r: ");
                rawR = Console.ReadLine();
            } while (!double.TryParse(rawR, out r));

            return new Kruznice(r, new Souradnice(x, y));
        }

        public override string ToString () {
            return string.Format("Kruznice #{0}: x={1}, y={2}, r={3}", id, Souradnice.x, Souradnice.y, Polomer);
        }
    }

    public struct Souradnice {
        public double x;
        public double y;

        public Souradnice (double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    public class UID {
        private static int nextID = 0;

        public static int Next () {
            return nextID++;
        }
    }
}
