﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_Abstract {
    public abstract class Teleso {

        protected static Dictionary<string, double> Hustoty = new Dictionary<string, double>();

        public abstract double Objem ();
        public abstract double Povrch ();
        public abstract double Hustota { get; set; }

        public Teleso () {
            if (Hustoty.Count == 0) {
                Hustoty.Add("Zelezo", 7870.0);
                Hustoty.Add("Med", 8960.0);
                Hustoty.Add("Olovo", 11340.0);
                Hustoty.Add("Cin", 7310.0);
            }
        }

        public double Hmotnost () {
            return Objem() * Hustota;
        }
    }


    class Koule : Teleso {
        public double Polomer { get; set; }
        public override double Hustota { get; set; }

        public Koule (double r, string material) {
            if (Hustoty.ContainsKey(material)) {
                Polomer = r;
                Hustota = Hustoty[material];
            } else {
                throw new FormatException($"Neexistujici material {material}");
            }
        }

        public override double Objem () {
            return 4.0 / 3.0 * Math.PI * Polomer * Polomer * Polomer;
        }

        public override double Povrch () {
            return 4.0 * Math.PI * Polomer * Polomer;
        }
    }

    class Krychle : Teleso {
        public double DelkaStrany { get; set; }
        public override double Hustota { get; set; }

        public Krychle (double r, string material) {
            if (Hustoty.ContainsKey(material)) {
                DelkaStrany = r;
                Hustota = Hustoty[material];
            } else {
                throw new FormatException($"Neexistujici material {material}");
            }
        }

        public override double Objem () {
            return DelkaStrany * DelkaStrany * DelkaStrany;
        }

        public override double Povrch () {
            return 6.0 * DelkaStrany * DelkaStrany;
        }
    }


    class Program {
        public static void Main () {
            Teleso[] telesa = { new Koule(1.2, "Med"), new Krychle(0.4, "Zelezo"), new Krychle(0.8, "Olovo"), new Koule(2.1, "Cin") };

            foreach (Teleso teleso in telesa) {
                Console.WriteLine("objem={0:0.00} \tpovrch={1:0.00} \thmotnost={2:0.00}", teleso.Objem(), teleso.Povrch(), teleso.Hmotnost());
            }

            Console.ReadKey();
        }
    }
}
