﻿using System;

namespace _03_Test {
    class Program {
        static void Main (string[] args) {

            DateTime datumNarozeni = new DateTime(2000, 1, 1);

            for (int i = 0; i < 5; i++) {
                Console.Write(i);
            }

            //Console.WriteLine(datumNarozeni);
            Console.ReadKey();
        }
    }

    class A {
        int a;

        byte _vek;
        byte Vek {
            get { return _vek; }
            set {
                if (value < 1 || value > 120)
                    throw new ArgumentOutOfRangeException("Musi byt mezi 1 a 120.");

                _vek = value;
            }
        }

        public A (int k) {
            a = k;
        }
    }

    class B : A {
        private string str;
        public B (string str, int k) : base(k) {
            this.str = str;
        }
    }
}
