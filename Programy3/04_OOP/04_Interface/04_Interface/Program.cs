﻿using System;

namespace _04_Interface {

    class Jahoda : IComparable {

        public int velikost;

        public Jahoda (int velikost) {
            this.velikost = velikost;
        }

        public int CompareTo (object obj) {
            if (obj.GetType() == typeof(Jahoda)) {
                return velikost - ((Jahoda)obj).velikost;
            } else {
                throw new ArgumentException();
            }
        }

        public override string ToString () {
            return "Jahoda: " + velikost;
        }
    }

    class Program {
        static void Main (string[] args) {


            int[] arr = new int[] { 20, 10, 58, 12, 3, 545, 13, 546, 5, 4, 1 };

            BubbleSortInt(ref arr);

            foreach (int i in arr) {
                Console.WriteLine(i);
            }


            IComparable[] s_arr = new string[] { "a", "c", "1", "d", "z" };

            BubbleSort(ref s_arr);

            foreach (string s in s_arr) {
                Console.WriteLine(s);
            }

            IComparable[] j_arr = new Jahoda[] { new Jahoda(10), new Jahoda(2), new Jahoda(5) };

            BubbleSort(ref j_arr);

            foreach (Jahoda j in j_arr) {
                Console.WriteLine(j);
            }

            Console.ReadKey();
        }


        public static void BubbleSortInt (ref int[] array) {
            while (true) {
                bool done = true;
                for (int i = 0; i < array.Length - 1; i++) {
                    if (array[i] > array[i + 1]) {
                        int x = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = x;
                        done = false;
                    }
                }
                if (done) {
                    break;
                }
            }
        }

        public static void BubbleSort (ref IComparable[] array) {
            while (true) {
                bool done = true;
                for (int i = 0; i < array.Length - 1; i++) {
                    if (array[i].CompareTo(array[i + 1]) > 0) {
                        IComparable x = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = x;
                        done = false;
                    }
                }
                if (done) {
                    break;
                }
            }
        }
    }


}
