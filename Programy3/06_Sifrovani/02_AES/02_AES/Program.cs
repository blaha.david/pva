﻿using System;
using System.Text;
using System.Security.Cryptography;


namespace _02_AES
{
    class Program
    {
        public static byte[] ToByteArray(string hexString)
        {
            byte[] data = new byte[hexString.Length / 2];
            for (int index = 0; index < data.Length; index++)
            {
                data[index] = Convert.ToByte(hexString.Substring(index * 2, 2), 16);
            }
            return data;
        }


        static void Main(string[] args)
        {

            byte[] key = ToByteArray("05C0D7850D415FE3F0DFDA4BC8515B114C7F6910102FAABF461CA8289469911F");
            byte[] iv = ToByteArray("2B52B0894C7418B291676DBE2B1220BB");
            byte[] cipher = ToByteArray("01B25E5EF03CB24DBE47441031CBD0DFBC79EC0872885F3FD9C42F9CD29DF7299FDE77A6C0B34AEF300E4BB9309024424E64144C14F437B2DF24F087CA681C909307F96141ECC33C28D4FB40DC6B72FE");


            byte[] data;
            string plain;

            using (var aes = Aes.Create())
            {
                aes.Mode = CipherMode.CBC;
                aes.Key = key;
                aes.IV = iv;

                using (var dec = aes.CreateDecryptor())
                {
                    data = dec.TransformFinalBlock(cipher, 0, cipher.Length);
                }

                plain = Encoding.GetEncoding("UTF-16").GetString(data);
            }

            Console.WriteLine(plain);


            byte[] odpovedData = Encoding.GetEncoding("UTF-16").GetBytes("Otto Wichterle");
            byte[] odpovedCipher;

            using (var aes = Aes.Create()) 
            {
                aes.Mode = CipherMode.CBC;
                aes.Key = key;
                aes.IV = iv;

                using (var enc = aes.CreateEncryptor())
                {
                    odpovedCipher = enc.TransformFinalBlock(odpovedData, 0, odpovedData.Length);
                }
            }
            
            Console.WriteLine(BitConverter.ToString(odpovedCipher).Replace('-', '\0'));
        }
    }
}
