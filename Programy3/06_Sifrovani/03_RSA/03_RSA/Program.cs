﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace _03_RSA
{
    class Program
    {

        static void Main(string[] args)
        {
            // Setup RSA
            string publicKey, privateKey;
            GenerateKeys(2048, out publicKey, out privateKey);


            //Antonín:

            string msg = "Hello world!";

            Console.WriteLine("Msg: {0}", msg);

            // Generate AES Key
            RandomNumberGenerator rnd = RandomNumberGenerator.Create();
            byte[] key = new byte[256 / 8];
            rnd.GetBytes(key);

            byte[] iv = new byte[16];
            byte[] encryptedMsg = AESEncrypt(msg, key, ref iv);

            Console.WriteLine("key: {0}\n", BitConverter.ToString(key));
            Console.WriteLine("IV: {0}\n", BitConverter.ToString(iv));
            Console.WriteLine("enc msg: {0}\n", BitConverter.ToString(encryptedMsg));


            var encryptedKey = RSAEncrypt(key, publicKey);
            Console.WriteLine("enc key: {0}\n", BitConverter.ToString(encryptedKey));

            var encryptedIV = RSAEncrypt(iv, publicKey);
            Console.WriteLine("enc IV: {0}\n", BitConverter.ToString(encryptedIV));



            // transfer encryptedKey, encryptedIV, encryptedMsg


            // Běta:

            var decryptedKey = RSADecrypt(encryptedKey, privateKey);
            Console.WriteLine("dec key: {0}\n", BitConverter.ToString(decryptedKey));

            var decryptedIV = RSADecrypt(encryptedIV, privateKey);
            Console.WriteLine("dec IV: {0}\n", BitConverter.ToString(decryptedIV));


            string decryptedMsg = AESDecrypt(encryptedMsg, decryptedKey, decryptedIV);

            Console.WriteLine(decryptedMsg);
        }


        static void GenerateKeys(int keyLength, out string publicKey, out string privateKey)
        {
            using (var rsa = RSA.Create())
            {
                rsa.KeySize = keyLength;
                privateKey = rsa.ToXmlString(true);
                publicKey = rsa.ToXmlString(false);
            }
        }


        static byte[] RSAEncrypt(byte[] data, string publicKey)
        {
            using (var rsa = RSA.Create())
            {
                rsa.FromXmlString(publicKey);
                var result = rsa.Encrypt(data, RSAEncryptionPadding.Pkcs1);
                return result;
            }
        }



        static byte[] RSADecrypt(byte[] data, string privateKey)
        {
            using (var rsa = RSA.Create())
            {
                rsa.FromXmlString(privateKey);
                var result = rsa.Decrypt(data, RSAEncryptionPadding.Pkcs1);
                return result;
            }
        }

        public static byte[] AESEncrypt(string plain, byte[] key, ref byte[] iv)
        {
            byte[] data = Encoding.GetEncoding("UTF-16").GetBytes(plain);
            byte[] encrypted;
            using (var aes = Aes.Create())
            {
                aes.Mode = CipherMode.CBC;
                aes.Key = key;
                aes.GenerateIV();
                iv = aes.IV;

                using (var enc = aes.CreateEncryptor())
                {
                    encrypted = enc.TransformFinalBlock(data, 0, data.Length);
                }
            }
            return encrypted;
        }

        public static string AESDecrypt(byte[] data, byte[] key, byte[] iv)
        {
            byte[] decrypted;
            using (var aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;
                aes.Mode = CipherMode.CBC;

                using (var dec = aes.CreateDecryptor())
                {
                    decrypted = dec.TransformFinalBlock(data, 0, data.Length);
                }
            }

            return Encoding.GetEncoding("UTF-16").GetString(decrypted);
        }
    }
}
