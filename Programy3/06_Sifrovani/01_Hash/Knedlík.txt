KNEDLÍK
hudba Vl. Veit, 2007

Knedlík letí nad městem
Neví kde přistát
Ještě včera byl těstem
Nemohl to vystát

Kuchař těsto uválel
Do vody ho hodil
A vařil obracel
A pak vodu scedil

Teď na knedlík vytáh nůž
Chtěl ho rozřezat
Ztratil nervy knedlík už
Řek si teď musím létat

Do výšky si vyskočil
Kolem sebe pohleděl
Ve vzduchu se otočil
Oknem k nebi vyletěl

Na nebi se rozhlédl
Jak je svět krásný
Potom ale zahlédl
Obraz příšerný děsný

Hejno ptáků s pokřikem
Za knedlíkem letí
Strašně klapou zobákem
Že knedlík teď snědí

Prudce k zemi zatočil
Přistál na trávník
A v kočku se proměnil
Co tu leží jak knedlík

Kočička teď na slunci
Kožíšek si hřeje
Kuchař stojí nad hrnci
Neví co se to děje