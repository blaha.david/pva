﻿using System;
using System.Text;
using System.IO;
using System.Security.Cryptography;


// README:
// Hadi rokle.txt je ten druhy (F443D7D6...)
// Knedlik.txt je ten prvni (1B804D1C...)

// Output:
// Hadi rokle.txt:
// Hex digest:
// F4-43-D7-D6-F4-DC-54-BC-20-34-3B-48-99-DB-4C-C1-F5-F1-47-3E-3D-61-C3-D0-2A-A3-D4-D0-9D-18-CA-9B
//
// Knedlik.txt:
// Hex digest:
// 1B-80-4D-1C-78-32-E9-34-36-F0-63-D0-D9-9C-0F-51-EA-70-6F-D4-0E-3D-00-88-29-8F-D0-D6-9D-9C-E3-71


namespace _01_Hash
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var sr = new StreamReader("Hadí rokle.txt"))
            {
                string text = sr.ReadToEnd();
                byte[] zpravaBytes = Encoding.GetEncoding("UTF-8").GetBytes(text);
                SHA256 sHA256 = new SHA256Managed();
                byte[] result = sHA256.ComputeHash(zpravaBytes);
                Console.WriteLine("Hadi rokle.txt:");
                //Console.WriteLine(text);
                Console.WriteLine("Hex digest:");
                Console.WriteLine(BitConverter.ToString(result));
            }

            Console.WriteLine("\n\n");

            using (var sr = new StreamReader("Knedlík.txt"))
            {
                string text = sr.ReadToEnd();
                byte[] zpravaBytes = Encoding.GetEncoding("UTF-8").GetBytes(text);
                SHA256 sHA256 = new SHA256Managed();
                byte[] result = sHA256.ComputeHash(zpravaBytes);
                Console.WriteLine("Knedlik.txt:");
                //Console.WriteLine(text);
                Console.WriteLine("Hex digest:");
                Console.WriteLine(BitConverter.ToString(result));
            }
        }
    }
}
