﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_OOP {

    /*
        Třída, konstruktor, dědičnost, metoda, seznam instancí tříd
        Definujte třídu geometrický objekt, který obsahuje strukturu souřadnice se souřadnicemi X a Y a unikátní identifikátor s parametrickým a bezparametrickým konstruktorem.
        Definujte třídu kružnice, která dědí od geometrického objektu a má vlastnost poloměr s parametrickým a bezparametrickým konstruktorem.
        Kružnice má mimo to metodu pro výpočet plochy, obvodu a metodu, která vrací, zda se protíná s jinou kružnicí, přijatou jako argument metody.
        Vytvořte statickou metodu pro načtení parametrů kružnice z konzole, která vrací instanci kružnice.
        V hlavním programu vytvořte seznam kružnic a naplňte jej částečně konstatními objekty vytvořenými programově a částečně kružnicemi zadanými z konzole.
        Vypište všechny kružnice, vzdálenosti jejich středů od počátku souřadnic, jejich plochy a obvody.
        Vypište všechny kružnice, které se protínají s alespoň jednou další kružnicí.
     */





    class Program {
        static void Main (string[] args) {

            List<Kruznice> seznamKruznic = new List<Kruznice>(10);
            seznamKruznic.Add(new Kruznice());
            seznamKruznic.Add(new Kruznice(3, new Souradnice(1, 1)));
            seznamKruznic.Add(Kruznice.NactiZKonzole());

            foreach (var k in seznamKruznic) {
                Console.WriteLine(k);
                Console.WriteLine("    Vzdalenost k pocatku: {0}", k.VzdalenostK(new Souradnice(0, 0)));
                Console.WriteLine("    Plocha: {0}", k.Plocha());
                Console.WriteLine("    Obvod: {0}", k.Obvod());
            }

            Console.WriteLine();

            foreach (var k in seznamKruznic) {
                bool protina = false;
                foreach (var druha in seznamKruznic) {
                    if (k != druha) {
                        if (k.Protina(druha)) {
                            protina = true;
                            break;
                        }
                    }
                }
                if (protina) {
                    Console.WriteLine(k);
                }
            }

            Console.ReadKey();
        }
    }

    public class GeometrickyObjekt {
        public Souradnice Souradnice { get; protected set; }
        protected int id;

        public GeometrickyObjekt (Souradnice souradnice) {
            this.Souradnice = souradnice;
            this.id = UID.Next();
        }

        public GeometrickyObjekt () : this(new Souradnice(0, 0)) { }
    }

    public class Kruznice : GeometrickyObjekt {
        public double Polomer { get; private set; }

        public Kruznice (double polomer, Souradnice souradnice) : base(souradnice) {
            this.Polomer = polomer;
        }

        public Kruznice (double polomer) : base() {
            this.Polomer = polomer;
        }

        public Kruznice () : this(1) { }

        public double Plocha () {
            return Math.PI * Polomer * Polomer;
        }

        public double Obvod () {
            return 2 * Math.PI * Polomer;
        }

        public bool Protina (Kruznice druha) {
            return VzdalenostK(druha.Souradnice) < this.Polomer + druha.Polomer;
        }

        public double VzdalenostK (Souradnice bod) {
            double dX = this.Souradnice.x - bod.x;
            double dY = this.Souradnice.y - bod.y;
            return Math.Sqrt(dX * dX + dY * dY);
        }

        public static Kruznice NactiZKonzole () {
            string rawX, rawY, rawR;
            double x, y, r;
            do {
                Console.Write("x: ");
                rawX = Console.ReadLine();
            } while (!double.TryParse(rawX, out x));
            do {
                Console.Write("y: ");
                rawY = Console.ReadLine();
            } while (!double.TryParse(rawY, out y));
            do {
                Console.Write("r: ");
                rawR = Console.ReadLine();
            } while (!double.TryParse(rawR, out r));

            return new Kruznice(r, new Souradnice(x, y));
        }

        public override string ToString () {
            return string.Format("Kruznice #{3}: x={0}, y={1}, r={2}", Souradnice.x, Souradnice.y, Polomer, id);
        }
    }

    public struct Souradnice {
        public double x;
        public double y;

        public Souradnice (double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    public class UID {
        private static int nextID = 0;

        public static int Next () {
            return nextID++;
        }
    }
}
