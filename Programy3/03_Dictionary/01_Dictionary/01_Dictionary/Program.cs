﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Dictionary {
    class Program {
        static void Main (string[] args) {
            /*
              1. Napište program, který z klávesnice načte libovolnou větu.
              2. Větu rozdělte na jednotlivá slova a uložte je do seznamu.
              3. Seznam vypište.
              4. Použijte dictionary pro výpočet četnosti jednotlivých hlásek ve větě.
            */

            //1.
            string veta = Console.ReadLine();

            //2. 
            List<string> slova = new List<string>(veta.Split(new char[] {' ', ',', '.', ':'}, StringSplitOptions.RemoveEmptyEntries));

            //3.
            foreach (var slovo in slova) {
                Console.WriteLine(slovo);
            }

            //4.
            SortedDictionary<char, int> hlasky = new SortedDictionary<char, int>();
            foreach (var znak in veta) {
                char hlaska = char.ToLower(znak);
                if (hlaska >= 97 && hlaska <= 122) {
                    if (!hlasky.ContainsKey(hlaska)) {
                        hlasky.Add(hlaska, 1);
                    } else {
                        hlasky[hlaska]++;
                    }
                }
            }

            foreach (var hlaska in hlasky) {
                Console.WriteLine("{0}: {1}", hlaska.Key, hlaska.Value);
            }


            Console.ReadKey();
        }
    }
}
