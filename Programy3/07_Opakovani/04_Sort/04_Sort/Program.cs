﻿using System;
using System.Collections.Generic;

namespace _04_Sort
{
    class Program
    {
        static void Main(string[] args)
        {

            Bod pocatek = new Bod(0, 0);

            List<Bod> body = new List<Bod>(){
                new Bod(4, 0),
                new Bod(0, 1),
                new Bod(0, 2),
                new Bod(10, 1),
                new Bod(5, 5),
                new Bod(1, 1),
            };


            Random rnd = new Random();

            for (int i = 0; i < 20; i++)
            {
                body.Add(new Bod(rnd.Next(0, 100), rnd.Next(0, 100)));
            }

            Porovnavac porovnavac = new Porovnavac(pocatek);

            body.Sort(porovnavac);

            foreach (var bod in body)
            {
                Console.WriteLine("[{0,2}, {1,2}]   d= {2,6:0.00}", bod.x, bod.y, bod.Dist(pocatek));
            }
        }
    }

    class Bod
    {
        public double x;
        public double y;

        public Bod(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public double Dist(Bod b)
        {
            double dx = Math.Abs(x - b.x);
            double dy = Math.Abs(y - b.y);
            return Math.Sqrt(dx * dx + dy * dy);
        }

        public override string ToString()
        {
            return string.Format("[{0}, {1}]", x, y);
        }
    }



    class Porovnavac : IComparer<Bod>
    {
        private Bod bod;

        public Porovnavac(Bod bod)
        {
            this.bod = bod;
        }


        public int Compare(Bod a, Bod b)
        {
            double da = bod.Dist(a);
            double db = bod.Dist(b);

            if (da < db)
            {
                return -1;
            }
            else if (da > db)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
