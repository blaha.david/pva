﻿using System;
using System.IO;

namespace _02_Vektory {
    class Program {
        const double rad2dec = 57.2957795130823;
        static int Main()
        {
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length < 2)
            {
                Console.WriteLine("Usage: {0} <file.csv>", args[0]);
                return 1;
            }
            if (!File.Exists(args[1]))
            {
                Console.WriteLine("File {0} not found", args[1]);
                return 2;
            }
            using (var sr = new StreamReader(args[1]))
            {
                int lineNumber = 1;
                while (!sr.EndOfStream)
                {
                    string[] values = sr.ReadLine().Split(',');
                    Vector a = null, b = null;
                    bool fail = false;
                    try
                    {
                        a = new Vector(double.Parse(values[0]), double.Parse(values[1]));
                        b = new Vector(double.Parse(values[2]), double.Parse(values[3]));
                    }
                    catch (FormatException _)
                    {
                        fail = true;
                        Console.WriteLine("Invalid value at line {0}", lineNumber);
                    }

                    if (!fail)
                    {
                        Console.WriteLine("{0} . {1} = {2}°", a, b, Vector.Dot(a, b) * rad2dec);
                    }
                    lineNumber++;
                }
            }

            //Console.ReadKey();
            return 0;
        }
    }
    class Vector {
        public double x;
        public double y;

        public double Len { get { return Math.Sqrt(x * x + y * y); } }

        public Vector(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public static double Dot(Vector a, Vector b)
        {
            return Math.Acos((a.x * b.x + a.y * b.y) / (a.Len * b.Len));
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", x, y);
        }
    }
}
