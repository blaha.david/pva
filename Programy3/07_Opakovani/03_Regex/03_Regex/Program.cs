﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace _03_Regex
{
    class Program
    {
        static void Main(string[] args)
        {

            //Regex regex = new Regex(@"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*)@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])");
            Regex regex = new Regex(@"\S+@\w+\.\w+");

            Dictionary<string, int> emails = new Dictionary<string, int>();

            int lineNum = 1;
            using (StreamReader sr = new StreamReader("emails.txt"))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine().ToLower();
                    MatchCollection matches = regex.Matches(line);
                    foreach (Match match in matches)
                    {
                        if (!emails.ContainsKey(match.ToString()))
                        {
                            emails.Add(match.ToString(), lineNum);
                        }
                        
                    }

                    lineNum++;
                }
            }

            foreach (string key in emails.Keys)
            {
                Console.WriteLine("{0} {1}", key.PadRight(30), emails[key]);
            }
        }
    }
}
