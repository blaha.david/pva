﻿using System;
using System.Collections.Generic;

namespace _01_Bludiste
{

    //Pohyb robota po bludišti
    //Termín: 31. květen 2021 23:59
    //Pokyny:
    //Naprogramujte instrukce pro robota, který má najít cestu ze startu do cíle pravoúhlé operační plochy.
    //Robot se může pohybovat pouze vodorovně nebo svisle, ne po úhlopříčce.
    //Operační plochu rozdělte do čtverců a některé z nich vyplňte bariérou.
    //Program napište tak, aby fungoval při jakémkoliv rozdělení operační plochy.
    //Instrukce kódujte jako posloupnost čtverců, kterými má robot projít.
    //Pokud není cesta ze startu do cíle proveditelná, vypište o tom informaci na konzoli.

    public class Program
    {
        public const bool DEBUG = false;
        static void Main(string[] args)
        {

            int cols = 20;
            int rows = 20;
            float wallChance = 0.3f;
            int startX = 0;
            int startY = 0;
            int endX = cols - 1;
            int endY = rows - 1;

            Cell[][] grid = new Cell[rows][];

            List<Cell> openSet = new List<Cell>();
            List<Cell> closedSet = new List<Cell>();
            List<Cell> path = new List<Cell>();

            Random r = new Random();

            // setup:

            for (int y = 0; y < rows; y++)
            {
                grid[y] = new Cell[cols];
                for (int x = 0; x < cols; x++)
                {
                    if (r.NextDouble() < wallChance && !((x == startX && y == startY) || (x == endX && y == endY)))
                    {
                        grid[y][x] = new Cell(x, y, true);
                    }
                    else
                    {
                        grid[y][x] = new Cell(x, y, false);
                    }
                }
            }
            for (int y = 0; y < rows; y++)
            {
                for (int x = 0; x < cols; x++)
                {
                    grid[y][x].findNeighbours(grid);
                }
            }

            Cell start = grid[startY][startX];
            Cell end = grid[endY][endX];

            openSet.Add(start);
            start.IsInOpenSet = true;
            start.IsInPath = true;





            // pathfinding:

            while (openSet.Count > 0)
            {
                int bestIndex = 0;
                for (int i = 0; i < openSet.Count; i++)
                {
                    if (openSet[i].F < openSet[bestIndex].F)
                    {
                        bestIndex = i;
                    }
                }

                Cell bestCell = openSet[bestIndex];

                //Console.WriteLine("BestCell: [{0}, {1}]", bestCell.X, bestCell.Y);


                if (bestCell == end)
                {
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    Console.WriteLine("DONE!");
                    Cell s = bestCell;
                    path = new List<Cell>();
                    path.Add(s);
                    while (s.Previous != null)
                    {
                        path.Add(s.Previous);
                        s.IsInPath = true;
                        s = s.Previous;
                    }
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.WriteLine("Solution:");
                    PrintGrid(grid);
                    return;
                }

                if (!bestCell.IsInClosedSet)
                {
                    closedSet.Add(bestCell);
                    bestCell.IsInClosedSet = true;
                }
                openSet.Remove(bestCell);
                bestCell.IsInOpenSet = false;


                foreach (Cell neighbour in bestCell.Neighbours)
                {
                    if (!neighbour.IsInClosedSet && !neighbour.IsWall)
                    {
                        bool haveNewPath = false;
                        if (neighbour.IsInOpenSet)
                        {
                            if (bestCell.G + 1 < neighbour.G)
                            {
                                neighbour.G = bestCell.G + 1;
                                haveNewPath = true;
                            }
                        }
                        else
                        {
                            neighbour.G = bestCell.G + 1;
                            haveNewPath = true;
                            openSet.Add(neighbour);
                            neighbour.IsInOpenSet = true;
                        }

                        if (haveNewPath)
                        {
                            neighbour.H = Dist(neighbour, end);
                            neighbour.F = neighbour.G + neighbour.H;
                            neighbour.Previous = bestCell;
                        }
                    }
                }
                //PrintGrid(grid);
                //Console.Write("Open: ");
                //PrintSet(openSet);
                //Console.Write("\nClosed: ");
                //PrintSet(closedSet);

            }
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("NO SOLUTION");
            PrintGrid(grid);
        }

        public static float Dist(Cell a, Cell b)
        {
            return MathF.Sqrt(MathF.Abs(a.X - b.X) * MathF.Abs(a.X - b.X) + MathF.Abs(a.Y - b.Y) * MathF.Abs(a.Y - b.Y));
        }

        public static void PrintSet(List<Cell> set)
        {
            Console.Write("[");
            foreach (Cell cell in set)
            {
                Console.Write("({0},{1}), ", cell.X, cell.Y);
            }
            Console.WriteLine("]");
        }

        public static void PrintGrid(Cell[][] grid)
        {
            for (int y = 0; y < grid.Length; y++)
            {
                for (int x = 0; x < grid[y].Length; x++)
                {
                    if (grid[y][x].IsWall)
                    {
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                    else
                    {
                        if (DEBUG)
                        {
                            if (grid[y][x].IsInPath)
                            {
                                Console.BackgroundColor = ConsoleColor.Blue;
                            }
                            else if (grid[y][x].IsInOpenSet)
                            {
                                Console.BackgroundColor = ConsoleColor.Green;
                            }
                            else if (grid[y][x].IsInClosedSet)
                            {
                                Console.BackgroundColor = ConsoleColor.Red;
                            }
                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Gray;
                            }
                        }
                        else
                        {
                            if (grid[y][x].IsInPath)
                            {
                                Console.BackgroundColor = ConsoleColor.Blue;
                            }
                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Gray;
                            }
                        }
                    }
                    Console.Write(" ");
                }
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine();
            }
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();
            Console.WriteLine();
        }
    }











    public class Cell
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public bool IsWall { get; private set; }

        public float F { get; set; }
        public float G { get; set; }
        public float H { get; set; }

        public List<Cell> Neighbours { get; private set; }

        public Cell Previous { get; set; }

        public bool IsInOpenSet { get; set; }
        public bool IsInClosedSet { get; set; }
        public bool IsInPath { get; set; }

        public Cell(int x, int y, bool isWall)
        {
            this.X = x;
            this.Y = y;
            IsWall = isWall;
            Neighbours = new List<Cell>(4);
        }


        public void findNeighbours(Cell[][] grid)
        {
            int rows = grid.Length;
            int cols = grid[0].Length;

            if (X < cols - 1)
            {
                Neighbours.Add(grid[Y][X + 1]);
            }
            if (X > 0)
            {
                Neighbours.Add(grid[Y][X - 1]);
            }
            if (Y < rows - 1)
            {
                Neighbours.Add(grid[Y + 1][X]);
            }
            if (Y > 0)
            {
                Neighbours.Add(grid[Y - 1][X]);
            }
            //if (i > 0 && j > 0)
            //{
            //neighbours.add(grid[i - 1][j - 1]);
            //}
            //if (i < cols - 1 && j > 0)
            //{
            //neighbours.add(grid[i + 1][j - 1]);
            //}
            //if (i > 0 && j < rows - 1)
            //{
            //neighbours.add(grid[i - 1][j + 1]);
            //}
            //if (i < cols - 1 && j < rows - 1)
            //{
            //neighbours.add(grid[i + 1][j + 1]);
            //}
        }
    }
}
