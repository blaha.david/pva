﻿using System;
using System.IO;
using System.Linq;

namespace VigenereCipher {
    class Program {

        static void Main (string[] args) {

            char[] msg = "secretsecret".ToCharArray();
            char[] key = "flag".ToCharArray();

            Console.WriteLine(Cipher.VigenereEncrypt(msg, key));
            Console.WriteLine(Cipher.VigenereDecrypt(Cipher.VigenereEncrypt(msg, key), key));

            //Cipher.EncryptStream(new StreamReader(File.Open("text.txt", FileMode.Open)), new StreamWriter(Console.OpenStandardOutput()), key);
            Cipher.EncryptStream(new StreamReader(Console.OpenStandardInput()), new StreamWriter(Console.OpenStandardOutput()), key);

            Console.ReadKey();

        }

    }

    public class Cipher {

        public static string alphabet = "abcdefghijklmnopqrstuvwxyz";

        public static char ShiftChar (char ch, int shift) {
            return (char)(Mod(ch - 97 + shift, 26) + 97);
        }

        public static char[] VigenereEncrypt (char[] input, char[] key) {
            char[] output = new char[input.Length];

            for (int i = 0; i < input.Length; i++) {
                char ch = char.ToLower(input[i]);
                if (!alphabet.Contains(ch)) {
                    //output[i] = input[i];
                } else {
                    output[i] = ShiftChar(ch, key[i % key.Length] - 97);
                }
            }
            return output;
        }

        public static char[] VigenereDecrypt (char[] input, char[] key) {
            char[] output = new char[input.Length];

            for (int i = 0; i < input.Length; i++) {
                char ch = char.ToLower(input[i]);
                if (!alphabet.Contains(ch)) {
                    //output[i] = input[i];
                } else {
                    output[i] = ShiftChar(ch, -(key[i % key.Length] - 97));
                }
            }
            return output;
        }

        public static void EncryptStream (StreamReader sr, StreamWriter sw, char[] key) {
            for (int i = 0; sr.Peek() != -1; i = ((i + 1) % key.Length)) {
                char ch = char.ToLower((char)sr.Read());
                if (!alphabet.Contains(ch)) {
                    //output[i] = input[i];
                } else {
                    sw.Write(ShiftChar(ch, key[i] - 97));
                }
            }
            sw.Flush();
        }

        public static void DecryptStream (StreamReader sr, StreamWriter sw, char[] key) {
            for (int i = 0; sr.Peek() != -1; i = ((i + 1) % key.Length)) {
                char ch = char.ToLower((char)sr.Read());
                if (!alphabet.Contains(ch)) {
                    //output[i] = input[i];
                } else {
                    sw.Write(ShiftChar(ch, -(key[i] - 97)));
                }
            }
            sw.Flush();
        }

        public static int Mod (int x, int m) {
            return (x % m + m) % m;
        }
    }
}
