﻿using System;

namespace _01_NumerickaIntegrace {

    public delegate double Funkce (double x);

    class Program {
        static void Main (string[] args) {

            Funkce f = Xna3;
            double a = 0;
            double b = 2;

            Console.WriteLine(Integrace(f, a, b, 100));
            Console.WriteLine(Integrace(f, a, b, 1000));
            Console.WriteLine(Integrace(f, a, b, 10000));

            Console.WriteLine(MonteCarlo(f, a, b, 100000));



            Console.ReadKey();
        }

        public static double Integrace (Funkce f, double a, double b, double pocetDilku) {

            double stepSize = (b - a) / pocetDilku;

            double obsah = 0;

            for (double i = a; i < b; i += stepSize) {
                double stred = i + (stepSize / 2);

                double y = f(stred);

                obsah += y * stepSize;
            }

            return obsah;
        }


        public static double MonteCarlo (Funkce f, double a, double b, double celkovyPocet) {
            double max = f(b); // vyska plochy

            double S = (b - a) * max; // obsah cele plochy

            int pocetPod = 0; // pocet bodu pod primkou

            Random r = new Random();

            for (int i = 0; i < celkovyPocet; i++) {
                double x = r.NextDouble() * (b - a) + a;
                double y = r.NextDouble() * max;

                double fy = f(x); // fy=hodnota funkce v bode x

                if (y < fy) { // pod primkou
                    pocetPod++;
                }
            }

            return pocetPod / celkovyPocet * S;
        }


        public static double Linearni (double x) {
            return x;
        }

        public static double Xna3 (double x) {
            return Math.Pow(x, 3);
        }
    }
}
