﻿using System;
using System.Text.RegularExpressions;

namespace _07_TelRegex {
    class Program {
        static void Main (string[] args) {
            string tel = "+420 000 111 222\n" +
                         "(420) 000 111 222\n" +
                         "000 111 222\n" +
                         "000111222\n" +
                         "+420000111222\n" +
                         "000111\n" +
                         "+(420) 000 111 222\"\n";

            Regex r = new Regex(@"^(\+?\(?(\d{3})\)?)? ?((\d{3}) ?){3}$", RegexOptions.Multiline);

            foreach (Match match in r.Matches(tel)) {
                GroupCollection groups = match.Groups;
                CaptureCollection telNum = groups[4].Captures;
                Console.WriteLine(string.Format("Shoda: {0,-18}    Predvolba: {1,3}    Tel: {2} {3} {4}", groups[0], groups[2], telNum[0], telNum[1], telNum[2]));
            }

            Console.ReadKey();

        }
    }
}
