﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace _04_Rejstrik {
    class Program {
        public static void Main () {

            Console.OutputEncoding = Encoding.UTF8;

            string[] spojky = { "a", "i", "ani", "jednak", "hned", "jak", "tak", "ba", "dokonce", "nejen", "nýbrž", "ale", "avšak", "však", "sice", "nebo", "anebo", "buď", "vždyť", "neboť", "totiž", "protože", "že", "poněvadž", "jelikož", "aby", "až", "takže", "jestliže", "kdyby", "když", "ač", "ačkoliv", "třeba", "třebaže", "přestože", "jakmile", "zatímco", "sotva", "jako", "tím" };
            string[] predlozky = { "od", "k", "s", "do", "bez", "kromě", "u", "vedle", "za", "pro", "za", "před", "mimo", "na", "pod", "nad", "mezi", "skrz", "o", "po", "v", "z"};

            Dictionary<string, List<int>> rejstrik = new Dictionary<string, List<int>>();

            string dokument = File.ReadAllText("Soubor.txt");

            string[] slova = dokument.Split(new char[] { ' ', ',', '.', ':', ';', '-', '\'', '"', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var raw_slovo in slova) {
                string slovo = raw_slovo.ToLower();

                if (Array.IndexOf(spojky, slovo) != -1 || Array.IndexOf(predlozky, slovo) != -1) {
                    continue;
                }

                if (!rejstrik.ContainsKey(slovo)) {
                    //Console.WriteLine("Found new word: " + slovo);

                    rejstrik[slovo] = new List<int>();

                    int index = dokument.IndexOf(slovo, StringComparison.OrdinalIgnoreCase);
                    do {
                        //Console.WriteLine("Found {0} at {1}", slovo, index);
                        rejstrik[slovo].Add(index);
                        index = dokument.IndexOf(slovo, index + 1, StringComparison.OrdinalIgnoreCase);
                    } while (index != -1);
                }
            }


            foreach (var slovo in rejstrik.Keys) {
                Console.WriteLine("{0}{1}", slovo.PadRight(20), string.Join(", ", rejstrik[slovo].ToArray()));
            }

            Console.ReadKey();
        }
    }
}
