﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace _05_regex {
    class Program {
        static void Main (string[] args) {
            string text = @"<HR>\n" +
                        "<CENTER><SMALL>\n" +
                        "<A HREF=\"#podrobnosti\">Podrobnosti</A>\n" +
                        "<A HREF=\"#software\">Dostupný software</A>\n" +
                        "<A HREF=\"#hardware\">Požadovaný hardware</A>\n" +
                        "<A HREF=\"#odkazy\">Odkazy na další informace</A>\n" +
                        "</SMALL></CENTER>\n" +
                        "<HR>\n";

            Regex r = new Regex("<a .*?href=\"(.*?)\".*?>(.*?)</a>", RegexOptions.IgnoreCase);

            MatchCollection matches = r.Matches(text);

            Console.WriteLine("Odkazy:");
            foreach (Match match in matches) {
                Console.WriteLine(match.Groups[1]);
            }

            Console.WriteLine("\nTitulky:");
            foreach (Match match in matches) {
                Console.WriteLine(match.Groups[2]);
            }

            Console.ReadKey();
        }
    }
}
