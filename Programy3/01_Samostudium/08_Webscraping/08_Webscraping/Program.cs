﻿using System;
using System.Net;
using System.Text.RegularExpressions;

namespace _08_Webscraping {
    class Program {
        static void Main (string[] args) {
            string source = new WebClient().DownloadString("http://www.tutorialspoint.com/tutorialslibrary.htm");

            Regex r = new Regex("^<li><a.*?href=\"(.*?)\".*?title=\"(.*?)\".*?> ?(.*?) ?</a></li>$", RegexOptions.Multiline);

            MatchCollection mathes = r.Matches(source);
            foreach (Match match in mathes) {
                Console.WriteLine(match.Groups[3]);
            }

            Console.WriteLine("\nCelkový počet: {0}", mathes.Count);

            Console.ReadKey();
        }
    }
}
