﻿using System;
using System.Globalization;
using System.Text;

/*
+-----------------------------------------------------+
|                                                     |
|                        Nadpis                       |
|                                   12. prosince 2001 |
+-----------------------------------------------------+
*/

namespace _02_FormatovaniStringu {
    class Program {
        static void Main (string[] args) {
            Console.OutputEncoding = Encoding.UTF8;

            char[] znaky = { '-', '|', '+' };

            Hlavicka(40, 5, "NADPIS", znaky);

            Console.ReadKey();
        }


        public static void Hlavicka (int _sirka, int vyska, string nadpis, char[] znaky) {
            char roh = znaky[2];
            char vodorovna = znaky[0];
            char svisla = znaky[1];

            string datum = DateTime.Now.ToString("dd. MMMM yyyy", CultureInfo.CreateSpecificCulture("cs-CZ"));

            int sirka = Math.Max(Math.Max(_sirka, nadpis.Length), datum.Length);

            string horizontalniRamecek = roh + new string(vodorovna, sirka) + roh;

            int indexNadpisu = (vyska + 1) / 2 - 1;

            Console.WriteLine(horizontalniRamecek);

            for (int i = 0; i < vyska; i++) {
                if (i == indexNadpisu) {
                    int mezera = (sirka - nadpis.Length) / 2;
                    Console.WriteLine(svisla + new string(' ', mezera) + nadpis + new string(' ', mezera + ((sirka - nadpis.Length) % 2 == 1 ? 1 : 0)) + svisla);
                } else if (i == vyska - 1) {
                    Console.WriteLine(svisla + new string(' ', sirka - datum.Length) + datum + svisla);
                } else {
                    Console.WriteLine(svisla + new string(' ', sirka) + svisla);
                }

            }

            Console.WriteLine(horizontalniRamecek);
        }
    }
}
