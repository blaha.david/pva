﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* 
    Napište program, který v zadaném textu, který obsahuje zakázaná slova "C#, CLR, Microsoft" hvězdičkami nahradí tato slova.

    Například:
    Microsoft announced its next generation C# compiler today.
    It uses advanced parser and special optimizer for the
    Microsoft CLR.
    přepíše jako
    ********* announced its next generation ** compiler today.
    It uses advanced parser and special optimizer for the
    ********* ***.
    
    Napište a otestujte metodu, která přijme větu a vrátí informaci, kolik je ve větě slov včetně číslovek, předložek a spojek.
*/



namespace _03_StringReplaceASplit {
    class Program {

        public static void Main (string[] args) {
            string[] zakazanaSlova = { "C#", "CLR", "Microsoft" };
            string text = "Microsoft announced its next generation C# compiler today.\nIt uses advanced parser and special optimizer for the\nMicrosoft CLR";
            string newText = OdstranZakazanaSlova(text, zakazanaSlova);
            Console.WriteLine(newText);

            Console.WriteLine(PocetSlov("Napište a otestujte metodu, která přijme větu a vrátí informaci, kolik je ve větě slov včetně číslovek, předložek a spojek."));

            Console.ReadKey();
        }

        public static string OdstranZakazanaSlova (string text, string[] zakazanaSlova) {
            for (int i = 0; i < zakazanaSlova.Length; i++) {
                text = text.Replace(zakazanaSlova[i], "".PadRight(zakazanaSlova[i].Length, '*'));
            }
            return text;
        }

        public static int PocetSlov (string veta) {
            return veta.Split(new char[] { ' ', ',', '.', ':', ';' }, StringSplitOptions.RemoveEmptyEntries).Length;
        }
    }
}
