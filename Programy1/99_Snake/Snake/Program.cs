﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Timers;
using System.Threading;

namespace Snake {
    class Program {

        static int snakeLength = 2;// length of snake
        const int gridSize = 10;
        static byte[,] grid = new byte[gridSize, gridSize];
        static bool running = true;

        static Random random = new Random();

        static byte direction = 0;//0=up,1=left,2=down,3=right

        static void Main(string[] args) {
            
            Timer stateTimer = new Timer(Update, new AutoResetEvent(true), 500, 1000);

            //0 = nothing
            //1 = food
            //2 = head
            //3... = body

            for (int j = 0; j < grid.GetLength(0); j++) {
                for (int i = 0; i < grid.GetLength(1); i++) {
                    grid[i, j] = 0;
                }
            }

            grid[gridSize / 2, gridSize / 2] = 2;//snake starts in the middle
            generateFood();

            printGrid();

            while (running) {
                
                if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.W) direction = 0;
                if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.A) direction = 1;
                if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.S) direction = 2;
                if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.D) direction = 3;
                
                /*
                switch (Console.ReadKey().KeyChar) {
                    case 'w': {
                            moveUp();
                            break;
                        }
                    case 'a': {
                            moveLeft();
                            break;
                        }
                    case 's': {
                            moveDown();
                            break;
                        }
                    case 'd': {
                            moveRight();
                            break;
                        }
                    case 'q': {
                            endGame();
                            break;
                        }
                }*/
                

                
                //System.Threading.Thread.Sleep(10);
            }

            
        }

        static void Update (Object stateInfo) {

            switch (direction) {
                case 0: {
                        moveUp();
                        break;
                    }
                case 1: {
                        moveLeft();
                        break;
                    }
                case 2: {
                        moveDown();
                        break;
                    }
                case 3: {
                        moveRight();
                        break;
                    }
            }

            printGrid();
        }

        static void moveUp() {
            int headI = -1;
            int headJ = -1;
            for (int j = 0; j < grid.GetLength(0); j++) {
                for (int i = 0; i < grid.GetLength(1); i++) {
                    if (grid[i, j] == 2) {
                        headI = i;
                        headJ = j;
                    }
                    if (grid[i, j] >= 2) {
                        if (grid[i, j] - 1 >= snakeLength) {
                            grid[i, j] = 0;
                        } else {
                            grid[i, j] += 1;
                        }
                    }
                }
            }
            if (headJ > 0) {
                if (grid[headI, headJ - 1] == 1) {
                    snakeLength++;
                    generateFood();
                }
                grid[headI, headJ - 1] = 2;
            } else {
                endGame();
            }
        }

        static void moveLeft() {
            int headI = -1;
            int headJ = -1;
            for (int j = 0; j < grid.GetLength(0); j++) {
                for (int i = 0; i < grid.GetLength(1); i++) {
                    if (grid[i, j] == 2) {
                        headI = i;
                        headJ = j;
                    }
                    if (grid[i, j] >= 2) {
                        if (grid[i, j] - 1 >= snakeLength) {
                            grid[i, j] = 0;
                        } else {
                            grid[i, j] += 1;
                        }
                    }
                }
            }
            if (headI > 0) {
                if (grid[headI - 1, headJ] == 1) {
                    snakeLength++;
                    generateFood();
                }
                grid[headI - 1, headJ] = 2;
            } else {
                endGame();
            }
        }

        static void moveDown() {
            int headI = -1;
            int headJ = -1;
            for (int j = 0; j < grid.GetLength(0); j++) {
                for (int i = 0; i < grid.GetLength(1); i++) {
                    if (grid[i, j] == 2) {
                        headI = i;
                        headJ = j;
                    }
                    if (grid[i, j] >= 2) {
                        if (grid[i, j] - 1 >= snakeLength) {
                            grid[i, j] = 0;
                        } else {
                            grid[i, j] += 1;
                        }
                    }
                }
            }
            if (headJ < grid.GetLength(0) - 1) {
                if (grid[headI, headJ + 1] == 1) {
                    snakeLength++;
                    generateFood();
                }
                grid[headI, headJ + 1] = 2;
            } else {
                endGame();
            }
        }

        static void moveRight() {
            int headI = -1;
            int headJ = -1;
            for (int j = 0; j < grid.GetLength(0); j++) {
                for (int i = 0; i < grid.GetLength(1); i++) {
                    if (grid[i, j] == 2) {
                        headI = i;
                        headJ = j;
                    }
                    if (grid[i, j] >= 2) {
                        if (grid[i, j] - 1 >= snakeLength) {
                            grid[i, j] = 0;
                        } else {
                            grid[i, j] += 1;
                        }
                    }
                }
            }
            if (headI < grid.GetLength(1) - 1) {
                if (grid[headI + 1, headJ] == 1) {
                    snakeLength++;
                    generateFood();
                }
                grid[headI + 1, headJ] = 2;
            } else {
                endGame();
            }
        }

        static void generateFood () {
            int count = 0;
            while (true) {
                int i = random.Next(gridSize);
                int j = random.Next(gridSize);
                if (grid[i, j] == 0) {
                    grid[i, j] = 1;
                    break;
                }
                count++;
                if (count > 10000) {
                    break;
                }
            }
        }

        static void endGame () {
            Console.WriteLine("GAME OVER!");
            running = false;
        }

        static void printGrid () {
            char side = ' ';
            string top = "";
            for (int i = 0;i < gridSize+2;i++) {
                top += ' ';
            }
            Console.Clear();
            Console.BackgroundColor = ConsoleColor.DarkGray;
            //Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.WriteLine(top);
            for (int j = 0; j < grid.GetLength(0); j++) {
                Console.BackgroundColor = ConsoleColor.DarkGray;
                //Console.ForegroundColor = ConsoleColor.White;
                Console.Write(side);
                for (int i = 0; i < grid.GetLength(1); i++) {
                    switch (grid[i,j]) {
                        case 0: {//nothing
                                Console.BackgroundColor = ConsoleColor.Black;
                                //Console.ForegroundColor = ConsoleColor.Black;
                                Console.Write(" ");
                                break;
                            }
                        case 1: {//food
                                Console.BackgroundColor = ConsoleColor.Red;
                                //Console.ForegroundColor = ConsoleColor.Black;
                                Console.Write(" ");
                                break;
                            }
                        case 2: {//head
                                Console.BackgroundColor = ConsoleColor.Green;
                                //Console.ForegroundColor = ConsoleColor.White;
                                Console.Write(" ");
                                break;
                            }
                        default: {//body
                                Console.BackgroundColor = ConsoleColor.DarkGreen;
                                //Console.ForegroundColor = ConsoleColor.White;
                                Console.Write(" ");
                                break;
                            }

                    }
                }
                Console.BackgroundColor = ConsoleColor.DarkGray;
                //Console.ForegroundColor = ConsoleColor.White;
                Console.Write(side);
                Console.WriteLine();
            }
            Console.BackgroundColor = ConsoleColor.DarkGray;
            //Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(top);//bottom
            Console.BackgroundColor = ConsoleColor.Black;
        }

    }
}
