﻿using System;

namespace Priklad3 {
    class Program {
        static void Main (string[] args) {
            Pary(new string[] { "Lucka", "Honza", "Viktor", "Adam", "Pavel", "Adela", "Marketa", "Eva", "Tomas" });
            Console.ReadKey();
        }

        static void Pary (string[] jmena) {
            int unused = jmena.Length;
            bool[] used = new bool[jmena.Length];
            Random random = new Random();

            while (unused >= 2) {
                int a = random.Next(0, jmena.Length);
                int b = random.Next(0, jmena.Length);
                if (a != b && !used[a] && !used[b]) {
                    Console.WriteLine("{0} a {1}", jmena[a], jmena[b]);
                    used[a] = true;
                    used[b] = true;
                    unused -= 2;
                }
            }
            if (unused == 1) {
                int i = 0;
                for (; i < used.Length; i++) {
                    if (!used[i]) {
                        break;
                    }
                }
                Console.WriteLine("Zbyl/a: {0}", jmena[i]);
            }

        }

    }
}
