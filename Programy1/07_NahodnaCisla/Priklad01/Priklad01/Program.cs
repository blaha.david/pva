﻿using System;

namespace Priklad01 {
    class Program {
        static Random r;

        static void Main (string[] args) {
            r = new Random();
            long pocet = 24000000;
            long[] cisla = new long[6];
            for (int i = 0; i < pocet; i++) {
                cisla[nahodneCislo() - 1]++;
            }
            long suma = 0;
            for (int i = 0; i < cisla.Length; i++) {
                suma += cisla[i];
            }
            double prumer = 100 / 6.0;

            for (int i = 0; i < cisla.Length; i++) {
                Console.WriteLine("{0}: pocet: {1}, pravdepodobnost: {2:0.000}%, odchylka od prumeru: {3:0.000}%", i + 1, cisla[i], (double)cisla[i] / pocet * 100, (double)cisla[i] / pocet * 100 - prumer);
            }
            Console.ReadKey();
        }

        static int nahodneCislo () {
            return r.Next(1, 7);
        }
    }
}
