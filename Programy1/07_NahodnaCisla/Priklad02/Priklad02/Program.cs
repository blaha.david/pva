﻿using System;

namespace Priklad02 {
    class Program {
        static void Main (string[] args) {
            double[] nahodnaCisla = NahodnaCisla(50000);
            for (int i = 0; i < nahodnaCisla.Length; i++) {
                //Console.WriteLine(nahodnaCisla[i]);
                for (int j = i + 1; j < nahodnaCisla.Length; j++) {
                    if (nahodnaCisla[i] == nahodnaCisla[j]) {
                        Console.WriteLine("STEJNE CISLO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    }
                }
                if (i % 10000 == 0) {
                    Console.WriteLine(i);
                }
            }
            Console.WriteLine("done");
            Console.ReadKey();
        }

        static double[] NahodnaCisla (int N) {
            double[] nahodnaCisla = new double[N];
            Random r = new Random();

            for (int i = 0; i < nahodnaCisla.Length;) {

                double nahodneCislo = r.NextDouble();

                //bool stejneCislo = false;
                //for (int j = 0; j < i; j++) {
                //    if (nahodneCislo == nahodnaCisla[j]) {
                //        stejneCislo = true;
                //        break;
                //    }
                //}
                //if (stejneCislo) {
                //    continue;
                //}

                nahodnaCisla[i] = nahodneCislo;
                i++;
            }

            return nahodnaCisla;
        }
    }
}
