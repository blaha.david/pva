﻿using System;
namespace DalsiPrikazy_1 {
    class Program {
        static void Main (string[] args) {
            Start:
            char input;
            Console.Write("Zadejte pismeno: ");
            if (char.TryParse(Console.ReadLine(), out input)) {
                switch (input) {
                    case 'M':
                        Console.WriteLine(1000);
                        break;
                    case 'D':
                        Console.WriteLine(500);
                        break;
                    case 'C':
                        Console.WriteLine(100);
                        break;
                    case 'L':
                        Console.WriteLine(50);
                        break;
                    case 'X':
                        Console.WriteLine(10);
                        break;
                    //case 'V':
                    //    Console.WriteLine(5);
                    //    break;
                    case 'I':
                        Console.WriteLine(1);
                        break;
                    case 'Q':
                        return;
                    default:
                        Console.WriteLine("Spatne pismeno.");
                        break;
                }
            }
            goto Start;
        }
    }
}
