﻿using System;
namespace Priklad5 {
    class Program {
        static void Main(string[] args) {
            double sum = 1.0, iterace = 2.0;
            for (bool scitani = true; iterace < 200000; scitani = !scitani) {
                sum += scitani ? (1.0 / iterace++) : (-1.0 / iterace++);
                Console.WriteLine("{0}: {1}", iterace, sum);
            }
            Console.ReadKey();
        }
    }
}