﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operatory {
    class Program {
        static void Main(string[] args) {
            /*
            int i = 1;
            Console.WriteLine(i);  //1
            Console.WriteLine(++i);//2
            Console.WriteLine(i);  //2
            Console.WriteLine("--------");
            i = 1;
            Console.WriteLine(i);  //1
            Console.WriteLine(i++);//1
            Console.WriteLine(i);  //2
            Console.WriteLine("--------");
            i = 1;
            Console.WriteLine();
            */

            //-------------------------------  STRING  -------------------------

            string dvojka = "2";
            string triAPul = "3.5";
            // Console.WriteLine(dvojka * triAPul);
            // skončí chybou!
            // string nezná operátor *
            int i = int.Parse(dvojka);
            double x = double.Parse(triAPul);
            Console.WriteLine(i * x); // OK


            //-----------------------
            Console.WriteLine();
            Console.ReadKey();

        }

        static int factorial (int i) {
            if (i == 1) return 1;
            return factorial(i-1) * i;
        }
    }
}
