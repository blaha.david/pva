﻿using System;

namespace Priklad9_DU {
    class Program {
        static void Main (string[] args) {
            int N = int.Parse(Console.ReadLine());
            int nLength = ("" + (ulong)(Math.Pow(2, N))).Length;
            ulong divider = (ulong)(Math.Pow(2, N)) / 64;
            if (divider == 0) {
                divider = 1;
            }
            for (int i = 0; i <= N; i++) {
                int iLength = ("" + (ulong)(Math.Pow(2, i))).Length;
                int lengthDifference = nLength - iLength;
                Console.Write("({0}): ", Math.Pow(2, i));
                Console.Write(new string(' ', lengthDifference));
                Console.WriteLine(new string('*', (int)(Math.Pow(2, i) / divider)));
            }
            Console.ReadKey();
        }
    }
}
