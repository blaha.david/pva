﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prikazy {
    class Program {
        static void Main(string[] args) {


            { // začátek bloku
                //Console.WriteLine("Hello World!");
                int index = 2;
                index++;
                //Console.WriteLine(index);
            } // konec bloku



            //char[] abc = { 'a', 'b', 'c', 'd', 'e', 'f' };
            string abc = "abcdef";

            foreach (char ch in abc) {
                //Console.WriteLine(ch);
            }

            //Console.WriteLine();



            int i = 1;
            do {
                //Console.WriteLine(i);
            } while (i < 1);




            int a = 1, b = 1;
            while (a <= 10) Console.WriteLine(a++);
            do Console.WriteLine(b++); while (b <= 10);
            for (int c = 1; c <= 10; c++) Console.WriteLine(c);





            Console.ReadKey();
        }
    }
}
