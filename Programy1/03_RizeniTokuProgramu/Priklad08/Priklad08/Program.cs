﻿using System;

namespace Priklad08 {
    class Program {
        static string[][] otazky = new string[][] {
            new string[] {
                "Jakým způsobem se správně deklaruje celočíselná proměnná v C#?",
                "a) int 1x = 10;",
                "b) int x = 10;",
                "c) float x = 10.0f;",
                "d) string x = \"10\";"
            },
            //new string[] { "Ahoj!",
            //    "a) Ahoj!",
            //    "b) Nazdar.",
            //    "c) Windows jsou hloupy.",
            //},
            new string[] { "Jak hodnotite windows?",
                "a) Skvely.",
                "b) Nejlepsi OS jaky existuje.",
                "c) Kam se hrabe Mac.",
                "d) Linux je lepsi."
            }
    };

        static char[] spravneOdpovedi = new char[] { 'b', 'c', 'd' };


        static void Main (string[] args) {
            int otazka = 0;

            while (otazka < otazky.Length) {

                for (int i = 0; i < otazky[otazka].Length; i++) {
                    Console.WriteLine(otazky[otazka][i]);
                }

                Console.Write("Vase odpoved? ");
                char odpoved;
                if (char.TryParse(Console.ReadLine(), out odpoved)) {
                    if (odpoved == spravneOdpovedi[otazka]) {
                        Console.WriteLine("\nJeeej. :)\n\n");
                        otazka++;
                    } else {
                        Console.WriteLine("\nSpatne!\n\n");
                    }
                } else {
                    Console.WriteLine("\nSpatny vstup!\n\n");
                }
            }
            Console.ReadKey();

        }
    }
}
