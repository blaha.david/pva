﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//V C# MA CHAR 16 BITU



namespace DatyveTypy {

    enum Ovoce {
        Jablko,
        Hruska,
        Banan,
        Jahoda,
        Jine
    }


    class Program {
        static int Main(string[] args) {
            /*
            string text = "";
            
            text = "132";
            char ch = 'a';
            ch = (char) 0x41;//cast z cisla na char
            

            text = "a\ta\nb\tb\n\\\"neco\"\\\nabc\t123\n";
            Console.WriteLine(text);

            text = "\n" + ch + " abc ";
            Console.WriteLine(text);

            text = "01234";
            Console.WriteLine("\n\ntext[0]={0}, text[2]={1}", text[0], text[2]);

            Console.WriteLine(true);
            */


            //----------------------------------  KONVERZE  ------------------------------------

            /*
            int i = 129847693;
            long l = i;//implicitni konverze (pokud nemuze dojit ke ztrate dat = z mensiho do vetsiho)

            //l *= 1000; // nelze prevest zpet na int32

            i = Convert.ToInt32(l);//komplicitni konverze

            double d = 1.2;

            int i2 = Convert.ToInt32(d);//Convert
            i2 = (int) d;//cast

            //byte b = Convert.ToByte(l); //nelze
            */


            //---------------------------------------------------------------------------------
            /*
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(Ovoce.Hruska == Ovoce.Banan ? "Yeah" : "Nope");
            */
            //-------------------------------  LITERARY  ---------------------------------------
            //Literary
            /*
            int a = 0;//<- literar = inicializace
            int b = 0x19;//hexadecimal
            long c = 89l;//long
            double d = 9.1d;//double
            decimal e = 1.2m;//decimal
            string f = "asd";//string
            char g = 'A';//char
            */

            //-------------------------------------  POLE  ------------------------

            //string[] s = { "jedna", "dva", "tri" };

            string[] mesice = {"Leden", "Unor", "Brezen", "Duben", "Kveten", "Cerven", "Cervenec", "Srpen", "Zari", "Rijen", "Listopad", "Prosinec"};
            int[] dny = {31,28,31,30,31,30,31,31,30,31,30,31};

            while (true) {
                int choise = Convert.ToInt32(Console.ReadLine()) - 1;
                Console.WriteLine("{0} ma {1} dnu.", mesice[choise], dny[choise]);
            }
            //-----------------------------------------------------------------
            string path = "C:\\Windows\\Notepad.exe";
            string takJakJe = @"C:\Windows\Notepad.exe";//--------------------------  TAK JAK JE
            //-----------------------------------------------------------------
            Console.ReadKey();
            return 0;
        }
    }
}
