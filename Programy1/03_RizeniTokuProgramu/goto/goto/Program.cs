﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @goto {
    class Program {
        static void Main(string[] args) {


            int i = 0;
            loop:
            if (i % 2 == 0) goto even;
            goto odd;
            even:
            Console.WriteLine("Cislo " + i + " je sude.");
            goto ifend;
            odd:
            Console.WriteLine("Cislo " + i + " je liche.");
            ifend:
            i++;
            if (i < 100) goto loop;
            Console.ReadKey();



            for (int x = 0; x < 100; x++) {
                if (x % 2 == 0) {
                    Console.WriteLine("Cislo {0} je sude.", x);
                } else {
                    Console.WriteLine("Cislo {0} je liche.", x);
                }
            }
            Console.ReadKey();
        }
    }
}
