﻿using System;

namespace Priklad08 {
    class Program {
        static void Main (string[] args) {
            Console.WriteLine(Deduplicate(new char[] { 'a', 'b', 'a', 'c', 'b', 'd', 'd', 'e', 'a', 'c', 'd', 'e' }));
            Console.ReadKey();
        }

        static char[] Deduplicate (char[] poleZnaku) {
            string deduplicated = "";
            for (int i = 0; i < poleZnaku.Length; i++) {
                if (!deduplicated.Contains("" + poleZnaku[i])) {
                    deduplicated += poleZnaku[i];
                }
            }
            return deduplicated.ToCharArray();
        }
    }
}
