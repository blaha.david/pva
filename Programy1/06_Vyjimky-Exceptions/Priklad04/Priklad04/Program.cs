﻿using System;

namespace Priklad04 {
    class Program {
        static void Main (string[] args) {

            Console.WriteLine(ObvodTrojuhelnika(-1, 5, 9));

            Console.ReadKey();
        }

        static double ObvodTrojuhelnika (double a, double b, double c) {
            if (a <= 0 || b <= 0 || c <= 0) {
                throw new ArgumentException("Všechny strany musí být větší než nula.");
            } else if (a + b <= c || a + c <= b || b + c <= a) {
                throw new ArgumentException("Součet dvou stran musí být větší než délka třetí strany.");
            } else {
                return a + b + c;
            }
        }
    }
}
