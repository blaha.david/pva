﻿using System;

namespace Priklad02 {
    class Program {
        static void Main (string[] args) {
            Console.WriteLine(IsEven(1));
            Console.WriteLine(IsEven(4));
            Console.WriteLine("-------------");

            Console.WriteLine(Convert("12"));
            Console.WriteLine(Convert("9873985738576846703475673463475673467346"));
            Console.WriteLine(Convert("1A"));
            Console.WriteLine("-------------");

            string s = "s";
            string t = "t";
            Swap(ref s, ref t);
            Console.WriteLine(s);
            Console.WriteLine(t);
            Console.WriteLine("-------------");

            string[] array = { "ahoj", "nazdar", "cus" };
            int[] lengthArray = Lengths(array);
            for (int i = 0; i < array.Length; i++) {
                Console.WriteLine(lengthArray[i]);
            }

            Console.ReadKey();
        }

        static bool IsEven (int number) {
            return number % 2 == 0;
        }

        static int Convert (string text) {
            if (int.TryParse(text, out int number)) {
                return number;
            }
            return int.MinValue;
        }

        static void Swap (ref string s, ref string t) {
            string temp = "";
            for (int i = 0; i < s.Length; i++) {
                temp += s[i];
            }
            s = t;
            t = temp;
        }

        static int[] Lengths (string[] array) {
            int[] lengths = new int[array.Length];
            for (int i = 0; i < lengths.Length; i++) {
                lengths[i] = array[i].Length;
            }
            return lengths;
        }
    }
}
