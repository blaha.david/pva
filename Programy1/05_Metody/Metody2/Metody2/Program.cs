﻿using System;

namespace Metody2 {

    public struct Bod {
        public double x, y;
        public Bod (double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    class Program {
        static void Main (string[] args) {
            Bod bod1 = new Bod(-3, 125);
            Bod bod2 = new Bod(-2, 126);
            Console.WriteLine(Vzdalenost(bod1, bod2));

            Console.ReadKey();
        }

        public static double Vzdalenost (Bod a, Bod b) {
            double dx = Math.Abs(a.x - b.x);
            double dy = Math.Abs(a.y - b.y);
            return Math.Sqrt(dx * dx + dy * dy);
        }
    }
}
