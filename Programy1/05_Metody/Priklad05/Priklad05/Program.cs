﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priklad05 {
    class Program {
        static void Main (string[] args) {
            Console.WriteLine(Prumer(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }));
            Console.ReadKey();
            

        }

        static double Prumer (int[] poleCisel) {
            int sum = 0;
            for (int i = 0; i < poleCisel.Length; i++) {
                sum += poleCisel[i];
            }
            return (double)sum / poleCisel.Length;
        }
    }
}
