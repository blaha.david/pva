﻿using System;
namespace Metody03 {
    class Program {
        static void Main (string[] args) {
            //double r = NactiDouble("Zadejte polomer: ");

            //Console.WriteLine("Obvod: " + Obvod(r));
            //Console.WriteLine("Plocha: " + Plocha(r));

            Console.WriteLine(JeSamohlaska('a'));
            Console.WriteLine(JeSamohlaska('r'));
            Console.WriteLine(JeSamohlaska('E'));

            int[] prvocinitele = Prvocinitele(25);

            Console.ReadKey();
        }

        //static double Obvod (double r) {
        //    return 2 * Math.PI * r;
        //}
        //static double Plocha (double r) {
        //    return Math.PI * r * r;
        //}

        //static double NactiDouble (string zprava = "") {
        //    Console.Write(zprava);
        //    return double.Parse(Console.ReadLine());
        //}

        static bool JeSamohlaska (char znak) {
            return "aeiouy".Contains(znak.ToString().ToLower());
        }

        static bool JeSamohlaska (string text) {
            for (int i = 0;i < text.Length;i++) {
                if (JeSamohlaska(text[i])) {
                    return true;
                }
            }
            return false;
        }

        static bool JeSamohlaska (string[] texty) {
            for (int i = 0;i < texty.Length;i++) {
                if (JeSamohlaska(texty[i])) {
                    return true;
                }
            }
            return false;
        }

    }
}
