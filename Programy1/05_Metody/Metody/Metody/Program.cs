﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metody {
    class Program {
        static void Main(string[] args) {
            int i = 1;
            Console.WriteLine(i);
            dontAdd(i);
            Console.WriteLine(i);
            add(ref i);
            Console.WriteLine(i);

            Console.ReadKey();
        }

        static void dontAdd(int i) {//predana promenna se nemeni (preda se hodnota)
            i++;
        }

        static void add(ref int i) {//zmeni predanou promenou (preda se odkaz na promennou)
            i++;
        }
    }
}
