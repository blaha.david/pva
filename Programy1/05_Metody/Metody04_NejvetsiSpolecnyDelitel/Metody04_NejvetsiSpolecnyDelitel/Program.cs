﻿using System;

namespace Metody04_NejvetsiSpolecnyDelitel {
    class Program {
        static void Main (string[] args) {
            Console.WriteLine(NejvetsiSpolecnyDelitel(new int[] { 12, 6, 24, 18, 36 }));
            Console.ReadKey();
        }

        public static int NejvetsiSpolecnyDelitel (int[] cisla) {
            int nejmensiCislo = Min(cisla);
            int x;
            for (x = nejmensiCislo; x >= 1; x--) {
                bool deliVsechny = true;
                for (int i = 0; i < cisla.Length; i++) {
                    if (cisla[i] % x != 0) {
                        deliVsechny = false;
                        break;
                    }
                }
                if (deliVsechny) {
                    break;
                }
            }
            return x;
        }

        public static int Min (int[] cisla) {
            int nejmensiIndex = 0;
            for (int i = 1; i < cisla.Length; i++) {
                if (cisla[i] < cisla[nejmensiIndex]) {
                    nejmensiIndex = i;
                }
            }
            return cisla[nejmensiIndex];
        }
    }
}
