﻿using System;

namespace Priklad12 {
    class Program {
        static void Main (string[] args) {

            //input
            int[] pole = { 1, 4, 4, 3, 2, 3, 4, 6, 5, 4, 1, 6, 2, 3, 5, 6 };


            for (int i = 0; i < pole.Length - 1; i++) {//loop through all elements
                if (pole[i] == 0) {//if you reach zero you are done (there can only be zeros after the first zero)
                    break;//exit loop
                }
                for (int j = i + 1; j < pole.Length; j++) {//loop from index i to end of the array
                    if (pole[i] == pole[j] && pole[j] != 0) {//if there is a duplicate
                        for (int x = j; x < pole.Length - 1; x++) {//shift the array and then set the last element to zero
                            pole[x] = pole[x + 1];//replace values
                        }
                        pole[pole.Length - 1] = 0;//set the last element to zero
                        j--;//the array was shifted so we need to decrease the index
                    }
                }
            }

            //print all values
            for (int i = 0; i < pole.Length; i++) {
                Console.Write(pole[i] + ", ");
            }
            Console.ReadKey();
        }
    }
}
