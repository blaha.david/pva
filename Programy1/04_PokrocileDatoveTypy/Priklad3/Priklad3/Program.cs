﻿using System;

namespace Priklad3 {
    class Program {
        static void Main (string[] args) {
            //char[] znaky = new char[] { 'a', 'b', 'a', 'a', 'c', 'b', 'b', 'b', 'c' };
            //int maxPocet = 0;
            //char nejdelsiZnak = ' ';
            //int pocet = 0;
            //char znak = znaky[0];
            //for (int i = 1; i < znaky.Length; i++) {
            //    if (znaky[i] == znak) {
            //        pocet++;
            //    } else {
            //        if (pocet > maxPocet) {
            //            maxPocet = pocet;
            //            nejdelsiZnak = znak;
            //        }
            //        znak = znaky[i];
            //        pocet = 1;
            //    }
            //}
            //Console.WriteLine(nejdelsiZnak);
            //Console.WriteLine(maxPocet);
            //Console.ReadKey();


            //char[] pole = { };
            //char[] pole = new char[] { 'a', 'b', 'a', 'a', 'c', 'b', 'b', 'b', 'c' };
            char[] pole = new char[] { 'e', 'a', 'b', 'e', 'e', 'e', 'e', 'e', 'a', 'a', 'c', 'c', 'a', 'c', 'c', 'e', 'b', 'b', 'b', 'c', 'd', 'c', 'c' };
            char[] nejdelsiSekvence = { };
            for (int len = 1; len < pole.Length; len++) {//zkusit vsechny delky sekvenci
                for (int i = 0; i < pole.Length - (len - 1); i++) {//projit cele pole 
                    char[] sekvence = new char[len];
                    bool stejneZnaky = true;
                    char znak = pole[i];
                    for (int x = 0; x < sekvence.Length; x++) {//vytvorit sekvenci a zaroven kontrolovat jestli jsou vsechny znaky stejne
                        sekvence[x] = pole[i + x];
                        if (sekvence[x] != znak) {
                            stejneZnaky = false;
                            break;
                        }
                    }
                    if (stejneZnaky) {
                        nejdelsiSekvence = sekvence;
                    }
                }
            }
            //output
            Console.Write('{');
            for (int i = 0; i < nejdelsiSekvence.Length; i++) {
                Console.Write("'" + nejdelsiSekvence[i]);
                if (i != nejdelsiSekvence.Length - 1) {
                    Console.Write("', ");
                }
            }
            Console.WriteLine("}");
            Console.ReadKey();

        }
    }
}
