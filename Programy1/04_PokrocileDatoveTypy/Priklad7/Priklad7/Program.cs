﻿using System;

namespace Priklad7 {
    public struct Bod {
        public Bod (double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public double x;
        public double y;
        public double z;

        override public string ToString () {
            return String.Format("{0};{1};{2}", x, y, z);
        }
    }
    public struct Usecka {
        public Usecka (Bod zacatek, Bod konec) {
            this.zacatek = zacatek;
            this.konec = konec;
        }
        public Bod zacatek;
        public Bod konec;

        override public string ToString () {
            return String.Format("{0} - {1}; {2}", zacatek.ToString(), konec.ToString(), Delka());
        }

        public double Delka () {
            return Math.Sqrt(Math.Abs(zacatek.x - konec.x) * Math.Abs(zacatek.x - konec.x) + Math.Abs(zacatek.y - konec.y) * Math.Abs(zacatek.y - konec.y) + Math.Abs(zacatek.z - konec.z) * Math.Abs(zacatek.z - konec.z));
        }
    }

    class Program {
        static void Main (string[] args) {

            Bod[] body = new Bod[] { new Bod(0, 0, 0), new Bod(10, 10, 10), new Bod(-20, -80, -0.1), new Bod(-45, -40.12, -57) };

            Usecka nejdelsi = new Usecka(body[0], body[0]);
            for (int i = 0; i < body.Length; i++) {
                for (int j = i + 1; j < body.Length; j++) {
                    Usecka usecka = new Usecka(body[i], body[j]);
                    if (usecka.Delka() > nejdelsi.Delka()) {
                        nejdelsi = usecka;
                    }
                }
            }

            Console.WriteLine(nejdelsi.ToString());

            Console.ReadKey();
        }
    }
}
