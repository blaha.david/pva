﻿using System;

namespace Priklad11 {
    class Program {
        static void Main (string[] args) {
            Hvezda slunce = new Hvezda(1000000, "Slunce", 1000, 200);
            Planeta[] planety = new Planeta[] { new Planeta(100, "Merkur", 10, slunce), new Planeta(900, "Venuse", 90, slunce), new Planeta(1000, "Zeme", 100, slunce), new Planeta(500, "Mars", 50, slunce), new Planeta(10000, "Jupiter", 5000, slunce), new Planeta(500, "Saturn", 50, slunce) };

            PlanetarniSoustava soustava = new PlanetarniSoustava(slunce, planety);

            Array.Sort(planety, delegate (Planeta planeta1, Planeta planeta2) {
                if (planeta1.hmotnost < planeta2.hmotnost) {
                    return -1;
                } else if (planeta1.hmotnost > planeta2.hmotnost) {
                    return 1;
                } else {
                    return 0;
                }
            });

            foreach (Planeta p in planety) {
                Console.WriteLine("Hmotnost: {0}, jmeno: {1}, polomer: {2}", p.hmotnost, p.jmeno, p.polomer);
            }

            Console.ReadKey();
        }
    }

    class AstronomickyObject {
        public double hmotnost;
        public AstronomickyObject (double hmotnost) {
            this.hmotnost = hmotnost;
        }
    }

    class Hvezda : AstronomickyObject {
        public string jmeno;
        public double polomer;
        public double svitivost;

        public Hvezda (double hmotnost, string jmeno, double polomer, double svitivost) : base(hmotnost) {
            this.jmeno = jmeno;
            this.polomer = polomer;
            this.svitivost = svitivost;
        }
    }

    class Planeta : AstronomickyObject {
        public string jmeno;
        public double polomer;
        public Hvezda hvezda;

        public Planeta (double hmotnost, string jmeno, double polomer, Hvezda hvezda) : base(hmotnost) {
            this.jmeno = jmeno;
            this.polomer = polomer;
            this.hvezda = hvezda;
        }
    }

    class PlanetarniSoustava {
        public Hvezda hvezda;
        public Planeta[] planety;

        public PlanetarniSoustava (Hvezda hvezda, Planeta[] planety) {
            this.hvezda = hvezda;
            this.planety = planety;
        }
    }
}
