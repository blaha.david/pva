﻿using System;

namespace DatoveTypy_Struct {
    public struct Bod {
        public Bod (double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public double x;
        public double y;
        public double z;
        public string Print () {
            return String.Format("{0};{1};{2}", x, y, z);
        }
    }
    public struct Usecka {
        public Usecka (Bod zacatek, Bod konec, float tloustka) {
            this.zacatek = zacatek;
            this.konec = konec;
            this.tloustka = tloustka;
        }
        public Bod zacatek;
        public Bod konec;
        public float tloustka;
        public string Print () {
            return String.Format("{0} : {1}; {2}", zacatek.Print(), konec.Print(), tloustka);
        }
    }

    class Program {
        static void Main (string[] args) {
            Bod bod1 = new Bod(0, 0, 0);
            Bod bod2 = new Bod(10, 10, 10);
            Bod bod3 = new Bod(-20, 80, 0.1);
            Bod bod4 = new Bod(45, 40.12, -57);
            Usecka[] usecky = { new Usecka(bod1, bod2, 1.5f),
                new Usecka(bod3, bod4, 2.5f)
            };

            foreach (Usecka usecka in usecky) {
                Console.WriteLine(usecka.Print());
            }

            Console.ReadKey();
        }
    }
}
