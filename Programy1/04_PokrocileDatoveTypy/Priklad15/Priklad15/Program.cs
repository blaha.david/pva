﻿using System;

namespace Priklad15 {
    class Program {
        static void Main (string[] args) {

            //input
            //string text = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Nullam rhoncus aliquam metus. In enim a arcu imperdiet malesuada. Mauris tincidunt sem sed arcu. Nunc dapibus tortor vel mi dapibus sollicitudin. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Duis pulvinar. Phasellus faucibus molestie nisl. Etiam commodo dui eget wisi. Nullam rhoncus aliquam metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum fermentum tortor id mi. Fusce tellus. Aenean placerat. Aliquam erat volutpat.";//input
            string text = System.IO.File.ReadAllText(@"E:\SKOLA\PVA\Programy\04_PokrocileDatoveTypy\Priklad15\Priklad15\TextFile1.txt");
            int[] count = new int[256];//letter count

            //find all counts
            for (char ch = (char)0; ch < 256; ch++) {//loop through all letters
                for (int i = 0; i < text.Length; i++) {//loop through the whole string
                    if (text[i] == ch) {
                        count[ch]++;
                    }
                }
            }

            //if we change count array order we need to keep track of which letter it is
            int[] indexes = new int[256];
            for (int i = 0; i < indexes.Length; i++) {
                indexes[i] = i;
            }

            //sort using bubble sort
            int[] array = count;
            while (true) {
                bool done = true;
                for (int i = 0; i < array.Length - 1; i++) {
                    if (array[i] < array[i + 1]) {
                        //swap value
                        int x = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = x;

                        //swap index
                        int y = indexes[i];
                        indexes[i] = indexes[i + 1];
                        indexes[i + 1] = y;

                        done = false;
                    }
                }
                if (done) {
                    break;
                }
            }
            count = array;

            for (int i = 0; i < indexes.Length; i++) {
                if (indexes[i] >= 32 && indexes[i] <= 256) {//filter for only printable characters
                    Console.WriteLine("{0}: {1}, {2}%", (char)indexes[i], count[i], (double)(count[i]) / text.Length * 100);
                }
            }
            Console.ReadKey();
        }
    }
}
