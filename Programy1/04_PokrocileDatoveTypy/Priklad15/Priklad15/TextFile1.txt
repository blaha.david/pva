﻿Příklad 1: Vytvořte program, který vytvoří celočíselné pole o 20 prvcích a inicializuje každý prvek na hodnotu rovnou pětinásobku indexu prvku. Vytiskněte pole na konzoli.
Příklad 2: Vytvořte program, který načte z konzole dvě pole a zkontroluje, zda jsou pole stejná. Pole jsou stejná, pokud se rovnají hodnoty prvků se stejným indexem.
Příklad 3: Vytvořte program, který najde v poli znaků nejdelší sekvenci prvků, které jsou si rovny. Například, pokud je dáno pole {'a', 'b', 'a', 'a', 'c', 'b', 'b', 'b', 'c'}, program vypíše  {'b', 'b', 'b'}.
Příklad 4: Vytvořte program, který načte z konzole pole stringů a zkontroluje, zda je zadané pole symetrické.
Například, pokud je zadané pole {"Merkur", "Venuše", "Země", "Mars", "Země", "Venuše", "Merkur"} vypíše všechny prvky pole, vypíše "Pole je symetrické." a skončí.
Příklad 5: Napište program, který z daného pole vypíše index druhého záporného prvku a jeho hodnotu. Například, pokud je dáno pole {1, 2, -3, 5, -6, 0, 2}, vypíše:
index = 4
hodnota = -6
Příklad 6: Vytvořte datovou strukturu struct pro popis souřadnice bodu v rovině, která bude mít záznamy x a y typu double.
Vytvořte datovou strukturu struct pro popis úsečky v rovině, která obsahuje souřadnice počátečního a koncového bodu.
Inicializujte 4 body v rovině a vytvořte pole dvou úseček. Vypište pole úseček.
Příklad 7: Vypočtěte, která z úseček v poli úseček z příkladu 6 je delší a vypište souřadnice jejího počátku, konce a její délku. Jakou nejdelší úsečku můžeme z daných 4 bodů vytvořit?
Příklad 8: Vytvořte třídu, která bude uchovávat informace o třídách SSPŠ: jméno třídy (např. 1D), počet žáků (34) a průměrný prospěch z PVA (1.2). Vytvořte pole čtyř objektů tříd a inicalizujte jej rozumnými hodnotami. Vytvořte metodu třídy, která zajistí výpis všech informací o třídě. Vypište třídy, které mají průměrný prospěch z PVA lepší než 2.5.
Příklad 9: Vytvořte program, který setřídí od nejmenšího po největší pole celých čísel bez použití knihovní metody Sort. Nejdřív vytvořte vývojový diagram, potom úlohu naprogramujte.
Příklad 10: Je dáno pole P bodů určených souřadnicemi x a y. Napište program, který rozhodne, které body z pole bodů P leží na kružnici se středem v počátku a s poloměrem r.
Příklad 11: Vytvořte třídu Astronomický objekt s atributem hmotnost.
Namodelujte hierarchii tříd:
Hvězda, která má atributy hmotnost, jméno, poloměr a svítivost(W).
Planeta, která má atributy hmotnost, jméno, poloměr, a Hvězda, kolem které obíhá.
Planetární soustava, která má Hvězdu, kolem které obíhají Planety.
Vytvořte objekty typu Hvězda a Planety a Planetární soustavu. Planety realizujte jako pole planet.
Setřiďte planety podle hmotnosti a vypište jejich vlastnosti.
Příklad 12: Vytvořte program, který vymaže z daného pole celých čísel všechny duplicitní prvky.  Např., pokud je dáno pole P = { 1, 4, 3, 2, 3, 4, 4, 1 }, bude výstupem programu pole
{ 1, 4, 3, 2, 0, 0, 0, 0 }.  Program vytvořte tak, aby nepotřeboval další pomocné pole. Na uspořádání výsledného pole nezáleží.
Příklad 13: Vytvořte program, který metodou bubblesort setřídí vzestupně pole celých čísel.
Příklad 14: Vytvořte program, který pomocí bubblesortu setřídí planety v příkladu 11 vzestupně podle:
a) hmotnosti
b) poloměru
V programu nabídněte obě možnosti a nechte uživatele, aby si vybral, v jakém pořadí program vypíše planety na terminál.
Příklad 15: Vytvořte program, který z načteného stringu vypíše tabulku absolutních a relativních četností písmen, které tvoří string. Tabulku vypište sestupně řazenou podle absolutní četnosti.
DÚ: Vytvořte program, který naplní 20 členné pole celými čísly. Čísla se budou generovat od čísla 1 tak, že každé následující číslo bude součtem dvou předchozích čísel, takže první čísla budou:
1,
1(=1+0),
2(=1+1),
3(=1+2),
5(=2+3),
8(=3+5), 13, 21, ... atd.