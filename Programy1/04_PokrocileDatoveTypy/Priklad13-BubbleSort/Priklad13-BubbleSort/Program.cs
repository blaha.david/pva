﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priklad13_BubbleSort {
    class Program {
        static void Main (string[] args) {
            string[] sArray = Console.ReadLine().Split(',');
            int[] array = new int[sArray.Length];
            for (int i = 0; i < array.Length; i++) {
                array[i] = Int32.Parse(sArray[i]);
            }
            while (true) {
                bool done = true;
                for (int i = 0; i < array.Length - 1; i++) {
                    if (array[i] > array[i + 1]) {
                        int x = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = x;
                        done = false;
                    }
                }
                if (done) {
                    break;
                }
            }
            for (int i = 0; i < array.Length; i++) {
                Console.Write(array[i]);
                if (i != array.Length - 1) {
                    Console.Write(',');
                }
            }
            Console.ReadKey();
        }
    }
}
