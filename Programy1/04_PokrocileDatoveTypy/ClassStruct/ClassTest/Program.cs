﻿using System;

namespace ClassStruct {
    public class Program {
        static void Main (string[] args) {
            //Struct je v zasobniku - prenaseji se promenne (vytvori se novy struct)
            {
                Book b;
                b.name = "abc";
                Book b2 = b;
                b.name = "123";

                Console.WriteLine(b.name);
                Console.WriteLine(b2.name);
            }

            //Trida je v horde - prenasi se odkaz na objekt (oba jsou totez)
            {
                BookClass b = new BookClass();
                b.name = "abc";
                BookClass b2 = b;
                b2.name = "123";

                Console.WriteLine(b.name);
                Console.WriteLine(b2.name);
            }
            Console.ReadKey();
        }
    }

    public struct Book {
        public string name;
    }

    public class BookClass {
        public string name;
    }
}
