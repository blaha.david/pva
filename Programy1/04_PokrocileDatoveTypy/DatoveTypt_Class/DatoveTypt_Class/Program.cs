﻿using System;

namespace DatoveTypt_Class {
    class Program {
        static void Main (string[] args) {
            Dvere dvere = new Dvere(Material.drevo, 8.25, 100, 50);
            Okno okno = new Okno(Material.sklo, 10.81, 50.5, 50.5);

            dvere.NapisMe();
            okno.NapisMe();

            Console.ReadKey();
        }
    }

    class StavebniOtvor {
        protected double sirka;
        protected double delka;

        public StavebniOtvor (double sirka, double delka) {
            this.sirka = sirka;
            this.delka = delka;
        }

        public void NapisMe () {
            Console.WriteLine("Otvor - sirka: {0}, delka: {1}.", sirka, delka);
        }
    }

    class Dvere : StavebniOtvor {
        private Material material;
        private double tepelnyOdpor;

        public Dvere (Material material, double tepelnyOdpor, double sirkaOtvoru, double delkaOtvoru) : base(sirkaOtvoru, delkaOtvoru) {
            this.material = material;
            this.tepelnyOdpor = tepelnyOdpor;
        }

        new public void NapisMe () {
            Console.WriteLine("Dvere - material: {0}, tepelny odpor: {1}, sirka: {2}, delka: {3}.", material, tepelnyOdpor, sirka, delka);
        }
    }

    class Okno : StavebniOtvor {
        private Material material;
        private double tepelnyOdpor;

        public Okno (Material material, double tepelnyOdpor, double sirkaOtvoru, double delkaOtvoru) : base(sirkaOtvoru, delkaOtvoru) {
            this.material = material;
            this.tepelnyOdpor = tepelnyOdpor;
        }

        new public void NapisMe () {
            Console.WriteLine("Okno - material: {0}, tepelny odpor: {1}, sirka: {2}, delka: {3}.", material, tepelnyOdpor, sirka, delka);
        }
    }

    enum Material {
        drevo,
        sklo,
        zelezo
    }
}
