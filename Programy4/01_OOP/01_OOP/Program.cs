﻿using System;
using System.Text.RegularExpressions;

namespace _01_OOP
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Clovek clovek1 = new Clovek();
            Console.WriteLine(clovek1);

            Clovek clovek2 = new Clovek("Jan", "Novak");
            Console.WriteLine(clovek2);

            Obcan obcan1 = new Obcan();
            Console.WriteLine(obcan1);

            Obcan obcan2 = new Obcan(clovek2, "123456/7890");
            Console.WriteLine(obcan2);


            //Obcan obcan3 = new Obcan(clovek2, "lmao");


            IZamestnanec zamestnanec = new ZamestnanyObcan(clovek2, "098765/4321");
            zamestnanec.NazevZamestnavatele = "Nekdo";
            zamestnanec.Plat = 20000m;
            
        }
    }

    public class Clovek
    {
        public string Jmeno { get; private set; }
        public string Prijmeni { get; private set; }



        public Clovek(string jmeno, string prijmeni)
        {
            Jmeno = jmeno;
            Prijmeni = prijmeni;
        }

        public Clovek()
        {
            Jmeno = "Nezname";
            Prijmeni = "Nezname";
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"Clovek: Jmeno={Jmeno}, Prijmeni={Prijmeni}";
        }
    }


    public class Obcan : Clovek
    {

        public static Regex rodneCisloRegex = new Regex(@"^\d{6}/\d{3,4}$");

        public string RodneCislo { get; private set; }

        public Obcan() : base()
        {
            RodneCislo = "000000/0000";
        }

        public Obcan(Clovek clovek, string rodneCislo) : base(clovek.Jmeno, clovek.Prijmeni)
        {
            if (!rodneCisloRegex.IsMatch(rodneCislo))
            {
                throw new FormatException($"Rodne cislo ({rodneCislo}) neni ve formatu 'nnnnnn/nnnn'");
            }
            RodneCislo = rodneCislo;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"Obcan: Jmeno={Jmeno}, Prijmeni={Prijmeni}, RodneCislo={RodneCislo}";
        }
    }


    // 2. hodina


    public interface IZamestnanec
    {
        public string NazevZamestnavatele { get; set; }
        public decimal Plat { get; set; }
    }
    public interface IPojistenec
    {
        public string NazevPojistovny { get; set; }
        public decimal MesicniPojistne { get; set; }
    }


    public class ZamestnanyObcan : Obcan, IZamestnanec, IPojistenec
    {
        private string _nazevZamestnavatele;
        string IZamestnanec.NazevZamestnavatele { get { return _nazevZamestnavatele; } set { _nazevZamestnavatele = value; } }

        private decimal _plat;
        decimal IZamestnanec.Plat { get { return _plat; } set { _plat = value; } }


        private string _nazevPojistovny;
        string IPojistenec.NazevPojistovny { get { return _nazevPojistovny; } set { _nazevPojistovny = value; } }

        private decimal _mesicniPojistne;
        decimal IPojistenec.MesicniPojistne { get { return _mesicniPojistne; } set { _mesicniPojistne = value; } }

        public ZamestnanyObcan(Clovek clovek, string rodneCislo) : base(clovek, rodneCislo)
        {
        }
    }
}
