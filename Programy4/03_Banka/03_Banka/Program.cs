﻿using System;

namespace _03_Banka
{
    public class Program
    {
        static void Main(string[] args)
        {
            var depozitni1 = new DepozitniUcet();
            var depozitni2 = new DepozitniUcet();
            var depozitni3 = new DepozitniUcet();
            var depozitni4 = new DepozitniUcet();
            var depozitni5 = new DepozitniUcet();
            var uverovy1 = new UverovyUcet(1000);
            var uverovy2 = new UverovyUcet(2000);
            var uverovy3 = new UverovyUcet(5000);
            var uverovy4 = new UverovyUcet(10000);
            var uverovy5 = new UverovyUcet(50000);
            var studentsky1 = new StudentskyUcet(1000);
            var studentsky2 = new StudentskyUcet(500);

            Ucet[] ucty = new Ucet[]{depozitni1, depozitni2, depozitni3, depozitni4, depozitni5, uverovy1, uverovy2, uverovy3, uverovy4, uverovy5, studentsky1, studentsky2};

            depozitni1.Vklad(2345);
            depozitni2.Vklad(123);
            depozitni3.Vklad(10000);
            depozitni4.Vklad(10000);
            depozitni5.Vklad(100);
            uverovy1.Vklad(2345);
            uverovy2.Vklad(123);
            uverovy3.Vklad(10000);
            uverovy4.Vklad(10000);
            uverovy5.Vklad(100);
            studentsky1.Vklad(2345);
            studentsky2.Vklad(123);

            depozitni3.Vyber(980);
            uverovy1.Vyber(3000);
            uverovy2.Vyber(500);

            foreach (var ucet in ucty)
            {
                Console.WriteLine(ucet);
            }


            depozitni3.Vklad(-465);
        }
    }



    public class Ucet
    {
        public static Random random = new Random();
        public int CisloUctu { get; private set; }
        public decimal Zustatek { get; protected set; }

        public Ucet()
        {
            CisloUctu = random.Next(10000000, 99999999);
            Zustatek = 0m;
        }

        public virtual void Vklad(decimal mnozstvi)
        {
            if (mnozstvi < 0m) 
                throw new ArgumentOutOfRangeException("Nelze vlozit zaporne finance");

            Zustatek += mnozstvi;
        }

        public virtual void Vyber(decimal mnozstvi)
        {
            if (mnozstvi < 0m) 
                throw new ArgumentOutOfRangeException("Nelze vybrat zaporne finance");

            Zustatek -= mnozstvi;
        }

        public override string ToString()
        {
            return $"{GetType()} #{CisloUctu}: {Zustatek}Kč";
        }
    }


    public class UverovyUcet : Ucet
    {
        public decimal UverovyRamec { get; private set; }
        public DateTime DatumZadluzeni { get; private set; }

        public UverovyUcet(decimal uverovyRamec)
        {
            UverovyRamec = uverovyRamec;
            DatumZadluzeni = new DateTime();
        }

        public override void Vyber(decimal mnozstvi)
        {
            if (Zustatek-mnozstvi >= -UverovyRamec)
                base.Vyber(mnozstvi);
            else
                throw new ArgumentOutOfRangeException("Dosazen uverovy ramec");
            if (Zustatek < 0m)
            {
                DatumZadluzeni = DateTime.Now;
            }
        }
    }

    public class DepozitniUcet : Ucet
    {
        public override void Vyber(decimal mnozstvi)
        {
            if (Zustatek-mnozstvi >= 0m)
                base.Vyber(mnozstvi);
            else
                throw new ArgumentOutOfRangeException("Nedostatek financi na uctu");
        }
    }

    public class StudentskyUcet : DepozitniUcet
    {
        public decimal Limit { get; private set; }

        public StudentskyUcet(decimal limit)
        {
            Limit = limit;
        }

        public override void Vyber(decimal mnozstvi)
        {
            if (mnozstvi <= Limit)
                base.Vyber(mnozstvi);
            else
                throw new ArgumentOutOfRangeException("Velikost vyberu presahuje limit");
        }
    }
}
