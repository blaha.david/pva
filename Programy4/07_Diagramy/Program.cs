﻿using System;

namespace _07_Diagramy
{
    class Program
    {
        static void Main(string[] args)
        {
            int cena = int.Parse(Console.ReadLine());
            int platba = int.Parse(Console.ReadLine());

            int[] hodnoty = {50,20,10,5,2,1};

            int vratit = platba - cena;

            foreach(int hodnota in hodnoty)
            {
                int pocet = vratit / hodnota;
                if (pocet > 0)
                {
                    Console.WriteLine($"{pocet} x {hodnota}Kč mincí");
                }
                vratit %= hodnota;
            }
        }
    }
}
