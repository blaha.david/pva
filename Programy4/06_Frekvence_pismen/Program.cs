﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace _06_Frekvence_pismen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            if (args.Length != 1)
            {
                Console.WriteLine("Requires 1 argument");
                return;
            }

            Dictionary<string, double> cetnosti = new Dictionary<string, double>(){
                {"a", 6.2193},
                {"á", 2.2355},
                {"b", 1.5582},
                {"c", 1.6067},
                {"č", 0.9490},
                {"d", 3.6019},
                {"ď", 0.0222},
                {"e", 7.6952},
                {"é", 1.3346},
                {"ě", 1.6453},
                {"f", 0.2732},
                {"g", 0.2729},
                {"h", 1.2712},
                {"ch", 1.1709},
                {"i", 4.3528},
                {"í", 3.2699},
                {"j", 2.1194},
                {"k", 3.7367},
                {"l", 3.8424},
                {"m", 3.2267},
                {"n", 6.5353},
                {"ň", 0.0814},
                {"o", 8.6664},
                {"ó", 0.0313},
                {"p", 3.4127},
                {"q", 0.0013},
                {"r", 3.6970},
                {"ř", 1.2166},
                {"s", 4.5160},
                {"š", 0.8052},
                {"t", 5.7268},
                {"ť", 0.0426},
                {"u", 3.1443},
                {"ú", 0.1031},
                {"ů", 0.6948},
                {"v", 4.6616},
                {"w", 0.0088},
                {"x", 0.0755},
                {"y", 1.9093},
                {"ý", 1.0721},
                {"z", 2.1987},
                {"ž", 0.9952}
            };

            Dictionary<string, int> frekvencePismen = new Dictionary<string, int>();
            Dictionary<string, int> frekvenceSlov = new Dictionary<string, int>();

            char[] splitChars = { ' ', ',', '-', '.', ':', ';' };

            double total = 0;
            double totalWords = 0;
            using (var sr = new StreamReader(args[0]))
            {
                while (sr.Peek() != -1)
                {
                    string line = sr.ReadLine().Trim();
                    IEnumerator<char> lineEnumerator = line.GetEnumerator();
                    lineEnumerator.Reset();
                    if (!lineEnumerator.MoveNext()) continue;
                    while (true)
                    {
                        char _ch = lineEnumerator.Current;
                        string ch;
                        bool cChar = false;
                        if (_ch == 'c')
                        {
                            cChar = true;
                            if (!lineEnumerator.MoveNext()) break;
                            if (lineEnumerator.Current == 'h')
                            {
                                ch = "ch";
                                if (!lineEnumerator.MoveNext()) break;
                            }
                            else
                            {
                                ch = "c";
                            }
                        }
                        else
                        {
                            ch = _ch.ToString().ToLower();
                        }
                        if (cetnosti.ContainsKey(ch))
                        {
                            total++;
                            if (frekvencePismen.ContainsKey(ch))
                            {
                                frekvencePismen[ch] += 1;
                            }
                            else
                            {
                                frekvencePismen.Add(ch, 1);
                            }
                        }
                        if (!cChar)
                        {
                            if (!lineEnumerator.MoveNext()) break;
                        }
                    }

                    string[] words = line.Split(splitChars, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
                    //string[] words = line.Split(' ');
                    foreach (string _word in words)
                    {
                        totalWords++;
                        string word = _word.ToLower();
                        if (frekvenceSlov.ContainsKey(word))
                        {
                            frekvenceSlov[word] += 1;
                        }
                        else
                        {
                            frekvenceSlov.Add(word, 1);
                        }
                    }
                }
            }

            Console.WriteLine("Frekvence pismen:");
            foreach (var frekvencePismena in frekvencePismen.OrderByDescending(x => Math.Abs((x.Value / total * 100) - cetnosti[x.Key])))
            {
                double myF = frekvencePismena.Value / total * 100;
                double targetF = cetnosti[frekvencePismena.Key];
                Console.WriteLine($"{frekvencePismena.Key,2}: {myF,5:F2}%  - {targetF,5:F2}%  =  {myF - targetF,5:F2}");
            }
            Console.WriteLine("\nFrekvence slov:");
            foreach (var frekvenceSlova in frekvenceSlov.OrderByDescending(x => x.Value))
            {
                Console.WriteLine($"{frekvenceSlova.Key,-15}: {frekvenceSlova.Value / totalWords * 100:F2}% ({frekvenceSlova.Value})");
            }
        }
    }
}
