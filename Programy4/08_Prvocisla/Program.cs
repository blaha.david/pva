﻿using System;

namespace _08_Prvocisla
{
    class Program
    {
        //Domácí úkol č. 2
        //1. Vytvořte vývojový diagram
        //Vytvořte konzolový program, který na základě dvou zadaných přirozených čísel n1 a n2, kde n1 < n2,  vypíše všechna prvočísla v intervalu (n1, n2).
        //2. Podle vývojového diagramu vytvořte program
        //3. Program otestujte.
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int n1 = int.Parse(Console.ReadLine());
            int n2 = int.Parse(Console.ReadLine());

            int[] prvocisla = new int[n2 - 2];
            for (int i = 2; i < n2; i++)
            {
                prvocisla[i - 2] = i;
            }


            for (int i = 2; i < Math.Sqrt(n2); i++)
            {
                for (int j = 0; j < prvocisla.Length; j++)
                {
                    if (prvocisla[j] % i == 0 && prvocisla[j] != i)
                    {
                        prvocisla[j] = -1;
                    }
                }
            }


            for (int i = n1; i < prvocisla.Length; i++)
            {
                if (prvocisla[i] != -1)
                {
                    Console.WriteLine(prvocisla[i]);
                }
            }
        }
    }
}
