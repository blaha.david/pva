﻿var s=1.0;
double F(double n)=>n==0?1:n*F(n-1);
for(double x=5,n=20,i=1;i<=n;i++)s+=Math.Pow(x,i)/F(i)*(i%2==0?-1:1);
Console.Write(s);
