﻿using System;

/*
Příklad:
Definujte třídu zlomek, která implementuje metody pro práci se zlomky(sčítání, odčítání, násobení, dělená a krácení). Vytvořte metodu Parse(), která načte zlomek ze stringu. Naprogramujte odpovídající konstruktory a metody a otestujte je v programu.
*/

namespace _04_Zlomek
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Zadejte zlomek: ");
            Zlomek zlomek1 = Zlomek.Parse(Console.ReadLine());
            Console.Write("Zadejte druhy zlomek: ");
            Zlomek zlomek2 = Zlomek.Parse(Console.ReadLine());
            Console.WriteLine($"{zlomek1} + {zlomek2} = {Zlomek.Zkratit(zlomek1 + zlomek2)}");
            Console.WriteLine($"{zlomek1} - {zlomek2} = {Zlomek.Zkratit(zlomek1 - zlomek2)}");
            Console.WriteLine($"{zlomek1} * {zlomek2} = {Zlomek.Zkratit(zlomek1 * zlomek2)}");
            Console.WriteLine($"{zlomek1} / {zlomek2} = {Zlomek.Zkratit(zlomek1 / zlomek2)}");
        }
    }

    public class Zlomek
    {
        public int Citatel {get; private set;}
        public int Jmenovatel {get; private set;}

        public Zlomek(int citatel, int jmenovatel)
        {
            this.Citatel = citatel;
            this.Jmenovatel = jmenovatel;
        }
        public Zlomek(int celeCislo)
        {
            this.Citatel = celeCislo;
            this.Jmenovatel = 1;
        }


        public static Zlomek operator +(Zlomek a, Zlomek b)
        {
            if (a.Jmenovatel == b.Jmenovatel)
            {
                return new Zlomek(a.Citatel + b.Citatel, a.Jmenovatel);
            }
            return a.Rozsirit(b.Jmenovatel) + b.Rozsirit(a.Jmenovatel);
        }
        public static Zlomek operator -(Zlomek a, Zlomek b)
        {
            if (a.Jmenovatel == b.Jmenovatel)
            {
                return new Zlomek(a.Citatel - b.Citatel, a.Jmenovatel);
            }
            return a.Rozsirit(b.Jmenovatel) - b.Rozsirit(a.Jmenovatel);
        }

        public static Zlomek operator *(Zlomek a, Zlomek b)
        {
            return new Zlomek(a.Citatel * b.Citatel, a.Jmenovatel * b.Jmenovatel);
        }
        public static Zlomek operator *(Zlomek a, int b)
        {
            return new Zlomek(a.Citatel * b, a.Jmenovatel);
        }


        public static Zlomek operator /(Zlomek a, Zlomek b)
        {
            return a * b.Obrazeny();
        }
        public static Zlomek operator /(Zlomek a, int b)
        {
            return new Zlomek(a.Citatel, a.Jmenovatel * b);
        }

        public Zlomek Obrazeny ()
        {
            return new Zlomek(Jmenovatel, Citatel);
        }

        public Zlomek Rozsirit (int i)
        {
            return new Zlomek(Citatel * i, Jmenovatel * i);
        }

        public static Zlomek Zkratit (Zlomek a)
        {
            Zlomek z = new Zlomek(a.Citatel, a.Jmenovatel);
            z.Zkratit();
            return z;
        }

        public bool Zkratit()
        {
            int c = Math.Abs(Citatel);
            int j = Math.Abs(Jmenovatel);
            int max = c < j ? c : j; // Math.min
            for (int i = 2; i <= max; i++)
            {
                if (Citatel % i == 0 && Jmenovatel % i == 0)// jsou oba delitelny beze zbytku
                { 
                    Citatel /= i;
                    Jmenovatel /= i;
                    if (!Zkratit())
                    {
                        return false;
                    }
                }
            }
            return false;
        }


        public override string ToString()
        {
            return $"{Citatel}/{Jmenovatel}";
        }

        public static Zlomek Parse(string input)
        {
            string[] split = input.Split('/');

            int citatel = int.Parse(split[0].Trim());
            int jmenovatel = int.Parse(split[1].Trim());

            return new Zlomek(citatel, jmenovatel);
        }
    }
}
